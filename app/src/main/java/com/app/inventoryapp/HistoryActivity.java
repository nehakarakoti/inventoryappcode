package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.HistoryListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.HistoryDataItem;
import com.app.inventoryapp.model.HistoryModel;
import com.app.inventoryapp.model.HistoryResponse;
import com.app.inventoryapp.my_interface.PaymentRejectListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HistoryActivity extends BaseActivity implements PaymentRejectListener {
    ImageView imageView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.LL_no_history_layout)
    LinearLayout LL_no_history_layout;

    @BindView(R.id.view)
    View view;

    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.btn_review_request)
    Button btn_review_request;
    @BindView(R.id.llReviewHistoryLL)
    LinearLayout llReviewHistoryLL;

    AlertDialog.Builder alertDialogBuilder;
    AlertDialog.Builder alertDialogBuilder2;
    ArrayList<HistoryModel> historyModelArrayList = new ArrayList<>();
    HistoryListAdapter historyListAdapter;
    List<HistoryDataItem> historyDataItemArrayListSorted = new ArrayList<>();

    HistoryDataItem historyDataItem;

    InventoryAppSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        session = new InventoryAppSession(this);
        if (session.getKeyUserType().equals("Physical") || session.getKeyUserType().equals("Supervisor")) {
            btn_review_request.setVisibility(View.VISIBLE);
        }

        settingStatus();


        setData();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        intit();

        btn_review_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReviewRequestDialog();
            }
        });


        llReviewHistoryLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(HistoryActivity.this, ReviewHistoryActivity.class);
                startActivity(mIntent);
            }
        });

    }

    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {

            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                    loggingOut(HistoryActivity.this);
                } else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);

                    httpGetHistory();
                }
            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);

                httpGetHistory();
            }


        }


    }


    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(HistoryActivity.this);
        alertDialogBuilder2 = new AlertDialog.Builder(HistoryActivity.this);
    }

    private void settingStatus() {
        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }

    private void httpGetHistory() {
        try {
            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            String url = Content.baseURL + "docps/history?i_id=" + InventoryAppSingleton.getInstance().getItem_id() + "&sort=-id";
            System.out.println("OUTPUT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(HistoryActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.OPTIONS, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            Log.d("HOSTORY RESPONSE==", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();
                                    HistoryResponse historyResponse = gson.fromJson(response, HistoryResponse.class);
                                    System.out.println("HISTORY DATA======" + historyResponse.getData().size());
                                    if (historyResponse.getData().size() == 0) {
                                        LL_no_history_layout.setVisibility(View.VISIBLE);
                                    } else {
                                        setList(historyResponse.getData());
                                    }
                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);
                                } else {

                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HistoryActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setList(List<HistoryDataItem> historyDataItemArrayList) {
        historyDataItemArrayListSorted = getSortedHistoryList(historyDataItemArrayList);
        historyListAdapter = new HistoryListAdapter(HistoryActivity.this, historyDataItemArrayListSorted, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyListAdapter);

    }

    private void setListR(List<HistoryDataItem> historyDataItemArrayList) {
        historyDataItemArrayListSorted.clear();
        historyListAdapter.notifyDataSetChanged();
        historyDataItemArrayListSorted = getSortedHistoryList(historyDataItemArrayList);
        historyListAdapter = new HistoryListAdapter(HistoryActivity.this, historyDataItemArrayListSorted, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(historyListAdapter);
    }


    private List<HistoryDataItem> getSortedHistoryList(List<HistoryDataItem> historyDataItemArrayList) {
        List<HistoryDataItem> historyDataItemArrayListSorted = new ArrayList<>();
        for (int i = historyDataItemArrayList.size() - 1; i >= 0; i--) {
            historyDataItemArrayListSorted.add(historyDataItemArrayList.get(i));
        }
        return historyDataItemArrayListSorted;
    }

    private void setData() {
        for (int i = 0; i < 10; i++) {
            HistoryModel historyModel = new HistoryModel();
            historyModelArrayList.add(historyModel);
        }
    }


    @Override
    public void rejectPayment(HistoryDataItem historyDataItem) {
        this.historyDataItem = historyDataItem;
        showConfirmationDialog();
    }

    @Override
    public void approvePayment(HistoryDataItem historyDataItem) {
        this.historyDataItem = historyDataItem;
        showConfirmationDialogPayment();

    }

    @Override
    public void reviewRequest(HistoryDataItem historyDataItem) {
        this.historyDataItem = historyDataItem;
        showReviewRequestDialog();
    }

    public void showReviewRequestDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_review_request_layout, null);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

        //final LinearLayout LL_review_text_container=(LinearLayout)dialogView.findViewById(R.id.LL_review_text_container);
        final EditText inputReview = (EditText) dialogView.findViewById(R.id.inputReview);
        // final TextView reviewText=(TextView) dialogView.findViewById(R.id.reviewText);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("DATA======" + inputReview.getText());
                if (inputReview.getText().toString().equals("")) {
                    Toast.makeText(HistoryActivity.this, "Please enter review text", Toast.LENGTH_SHORT).show();

                } else {
                    alertDialog.dismiss();
                    reviewRequestApiCall(inputReview.getText().toString().trim());
                }

            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });


    }


    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to reject this payment?");

        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                rejectApiCall();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });
    }

    private void showConfirmationDialogPayment() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to approve this quantity?");

        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                approveApiCall();
                //rejectApiCall();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });
    }


    public void reviewRequestApiCall(final String message) {
        try {
            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            String url = "http://dharmani.com/railway_api/web/site/custompush";
            System.out.println("OUTPUT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(HistoryActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                Log.d("RESPONSE=====", response);
                                Toast.makeText(HistoryActivity.this, "Review message has been sent successfully", Toast.LENGTH_SHORT).show();
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("project_id", InventoryAppSingleton.getInstance().getProject_id());
                    params.put("message", message);
                    params.put("doc_id", "" + "0");
                    params.put("item", "" + InventoryAppSingleton.getInstance().getItem_id());
                    params.put("login_user", "" + session.getKeyUserId());


                    return params;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HistoryActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    public void approveApiCall() {
        try {
            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            String url = Content.baseURL + "docs/" + historyDataItem.getId();
            System.out.println("OUTPUT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(HistoryActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();
                                    HistoryResponse historyResponse = gson.fromJson(response, HistoryResponse.class);
                                    System.out.println("HISTORY DATA======" + historyResponse.getData().size());

                                    if (historyResponse.getData().size() == 0) {
                                        LL_no_history_layout.setVisibility(View.VISIBLE);
                                    } else {
                                        setListR(historyResponse.getData());
                                    }
                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);
                                } else {
                                }
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", session.getKeyUserId());
                    params.put("qty", historyDataItem.getQtyDone());
                    params.put("c_status", "3");


                    return params;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HistoryActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    public void rejectApiCall() {
        try {
            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            String url = Content.baseURL + "docs/" + historyDataItem.getId();
            System.out.println("OUTPUT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(HistoryActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();

                                    HistoryResponse historyResponse = gson.fromJson(response, HistoryResponse.class);

                                    System.out.println("HISTORY DATA======" + historyResponse.getData().size());


                                    if (historyResponse.getData().size() == 0) {
                                        LL_no_history_layout.setVisibility(View.VISIBLE);
                                    } else {
                                        setListR(historyResponse.getData());
                                    }
                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);
                                } else {

                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", session.getKeyUserId());
                    params.put("qty", historyDataItem.getQtyDone());
                    params.put("c_status", "2");


                    return params;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HistoryActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }
}
