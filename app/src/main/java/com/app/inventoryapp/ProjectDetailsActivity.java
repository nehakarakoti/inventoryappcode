package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ProjectDetailsListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.DataIndividual;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.model.ProjectItemDetails;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.model.ResponseProjectIndividual;
import com.app.inventoryapp.my_interface.ItemDeleteListener;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProjectDetailsActivity extends BaseActivity implements ItemDeleteListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.piechart)
    PieChart pieChart;
    @BindView(R.id.piechart2)
    PieChart pieChart2;
    public BroadcastReceiver comingBack = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            apiCall();

        }
    };
    @BindView(R.id.view)
    View view;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.imageView_users)
    ImageView imageView_users;
    AlertDialog.Builder alertDialogBuilder;
    @BindView(R.id.imageView_notification)
    ImageView imageView_notification;
    @BindView(R.id.txtBillClearkPTV)
    TextView txtBillClearkPTV;
    @BindView(R.id.txtSSEProgressPTV)
    TextView txtSSEProgressPTV;


    ProjectDataModel projectDataModel = new ProjectDataModel();
    private ArrayList<ProjectItemModel> projectItemModelArrayList = new ArrayList<>();
    private ArrayList<ProjectDataModel> projectDataModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details2);
        ButterKnife.bind(this);
        registerReceiver(comingBack, new IntentFilter("comingBack"));


        intit();

        System.out.println("PROJECT ID==========" + InventoryAppSingleton.getInstance().getProject_id());
        System.out.println("PROJECT LEVEL FROM DETAIL PROGRESS FINANCIAL======" + projectDataModel.getP_total_payment_done());
        System.out.println("PROJECT LEVEL FROM DETAIL PROGRESS PHYSICAL======" + projectDataModel.getP_total_qty_done());

        String financialProgress = InventoryAppSingleton.getInstance().getFinancialProgress();
        String physicalProgress = InventoryAppSingleton.getInstance().getPhysicalProgress();

        System.out.println("PROJECT LEVEL PROGRESS FINANCIAL======" + financialProgress);
        System.out.println("PROJECT LEVEL PROGRESS PHYSICAL======" + physicalProgress);

        /*Setting color of Screen as per its coresponding progress*/
        apiCall();


        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        imageView_users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProjectDetailsActivity.this, ProjectUsersActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });


        imageView_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProjectDetailsActivity.this, ProjectNotifications.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ProjectDetailsActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);

                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);

            }


        }

    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(ProjectDetailsActivity.this);
    }

    private void apiCall() {
        try {
            String url = Content.baseURL + "projectds/" + InventoryAppSingleton.getInstance().getProject_id() + "?expand=tblIteams";
            System.out.println("CURRENT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(ProjectDetailsActivity.this, "Loading....");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("RESPONSE=======" + response);

                            try {
                                Log.d("RESPONSE==", response);
                                Gson gson = new Gson();
                                ResponseProjectIndividual responseProjectIndividual = gson.fromJson(response, ResponseProjectIndividual.class);

                                DataIndividual dataIndividual = responseProjectIndividual.getData();

                                setFinacialPieChart(dataIndividual.getP_total_payment_done());
                                txtBillClearkPTV.setText(dataIndividual.getP_total_payment_done());
                                setPhysicalPieChart(dataIndividual.getP_total_qty_done());
                                txtSSEProgressPTV.setText(dataIndividual.getP_total_qty_done());

                                setList(dataIndividual.getTblIteams());
                                /* Gson gson=new Gson();

                                ProjectResponseIndividual projectResponseIndividual=gson.fromJson(response, ProjectResponseIndividual.class);
                                ProjectDataModelIndividual projectDataModeIndividual=projectResponseIndividual.getData();
*/

                                //setList(projectDataModeIndividual.getTblIteams());
                                /*setFinacialPieChart(projectDataModel.getP_total_payment_done());
                                setPhysicalPieChart(projectDataModel.getP_total_qty_done());*/

                                // System.out.println("RESPONSE======="+projectDataModeIndividual.getTblIteams().size());


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectDetailsActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }

    }

    private void setPhysicalPieChart(String quantityDone) {

        float quntity_done = 0.0f;
        float quantity_not_done = 0.0f;


        try {
            quntity_done = Float.parseFloat(quantityDone);

            if (quntity_done == 0.0) {
                setPhysicalChartSingleQuantityNoDoneOnly(100.0f);
            } else if (quntity_done == 100.0) {
                setPhysicalChartSingleQuantityDoneOnly(quntity_done);
            } else {
                setPhysicalChartQuantityDoneBoth(quntity_done, 100.0f - quntity_done);
            }

        } catch (Exception ex) {
            System.out.println("ERROR=======" + ex);
        }


    }

    private void setPhysicalChartQuantityDoneBoth(float quntity_done, float v) {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();
        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(quntity_done, 0));
        entries.add(new BarEntry(v, 1));

        PieEntryLabels.add("");
        PieEntryLabels.add("");
        /*PieEntryLabels.add("English");
        PieEntryLabels.add("MIL");
        PieEntryLabels.add("MATHEMATICS");*/

        final int[] piecolors = new int[]{
                Color.rgb(0, 128, 0),
                Color.rgb(0, 0, 255)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart2.setData(pieData);


        pieChart2.setDescription("");


        // pieChart.setDescriptionPosition(20,20);

        pieChart2.animateXY(3000, 3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);

    }

    private void setPhysicalChartSingleQuantityDoneOnly(float quntity_done) {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();

        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(quntity_done, 0));


        PieEntryLabels.add("");


        final int[] piecolors = new int[]{
                Color.rgb(0, 128, 0)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart2.setData(pieData);
        pieChart2.setDescription("");

        pieChart2.animateXY(3000, 3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);
    }

    private void setPhysicalChartSingleQuantityNoDoneOnly(float v) {

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();

        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(v, 0));


        PieEntryLabels.add("");


        final int[] piecolors = new int[]{
                Color.rgb(0, 0, 255)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart2.setData(pieData);
        pieChart2.setDescription("");

        pieChart2.animateXY(3000, 3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);


    }

    private void setFinacialPieChart(String paymerntDone) {
        float payment_done = 0.0f;
        float payment_not_done = 0.0f;

        try {
            payment_done = Float.parseFloat(paymerntDone);

            if (payment_done == 0.0) {
                setFinancialChartSingleQuantityNoDoneOnly(100.f);
            } else if (payment_done == 100.0) {
                setFinancialChartSingleQuantityDoneOnly(payment_done);
            } else {
                setFinancialChartQuantityDoneBoth(payment_done, 100.f - payment_done);
            }

        } catch (Exception ex) {
            System.out.println("ERROR=======" + ex);
        }


    }

    private void setFinancialChartQuantityDoneBoth(float payment_done, float v) {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();
        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(payment_done, 0));
        entries.add(new BarEntry(v, 1));

        PieEntryLabels.add("");
        PieEntryLabels.add("");
        /*PieEntryLabels.add("English");
        PieEntryLabels.add("MIL");
        PieEntryLabels.add("MATHEMATICS");*/

        final int[] piecolors = new int[]{
                Color.rgb(0, 128, 0),
                Color.rgb(0, 0, 255)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart.setData(pieData);


        pieChart.setDescription("");


        // pieChart.setDescriptionPosition(20,20);

        pieChart.animateXY(3000, 3000);
        pieChart.getLegend().setEnabled(false);
        pieChart.setTouchEnabled(false);

    }

    private void setFinancialChartSingleQuantityDoneOnly(float payment_done) {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();
        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(payment_done, 0));


        PieEntryLabels.add("");

        /*PieEntryLabels.add("English");
        PieEntryLabels.add("MIL");
        PieEntryLabels.add("MATHEMATICS");*/

        final int[] piecolors = new int[]{
                Color.rgb(0, 128, 0)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart.setData(pieData);

        pieChart.setDescription("");
        pieChart.animateXY(3000, 3000);
        pieChart.getLegend().setEnabled(false);
        pieChart.setTouchEnabled(false);
    }

    private void setFinancialChartSingleQuantityNoDoneOnly(float v) {
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();
        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(v, 0));


        PieEntryLabels.add("");

        /*PieEntryLabels.add("English");
        PieEntryLabels.add("MIL");
        PieEntryLabels.add("MATHEMATICS");*/

        final int[] piecolors = new int[]{
                Color.rgb(0, 0, 255)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart.setData(pieData);


        pieChart.setDescription("");


        // pieChart.setDescriptionPosition(20,20);

        pieChart.animateXY(3000, 3000);
        pieChart.getLegend().setEnabled(false);
        pieChart.setTouchEnabled(false);
    }

    private void setPieChart() {

        /*PIE CHART1 SETTING*/

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> PieEntryLabels = new ArrayList<>();
        PieDataSet pieDataSet;
        PieData pieData;
        entries.add(new BarEntry(30, 0));
        entries.add(new BarEntry(70, 1));

        PieEntryLabels.add("");
        PieEntryLabels.add("");
        /*PieEntryLabels.add("English");
        PieEntryLabels.add("MIL");
        PieEntryLabels.add("MATHEMATICS");*/

        final int[] piecolors = new int[]{
                Color.rgb(46, 134, 193),
                Color.rgb(31, 97, 141)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart.setData(pieData);

        pieChart.setDescription("");

        // pieChart.setDescriptionPosition(20,20);

        pieChart.animateXY(3000, 3000);
        pieChart.getLegend().setEnabled(false);

        ArrayList<Entry> entries2 = new ArrayList<>();
        ArrayList<String> PieEntryLabels2 = new ArrayList<>();

        PieDataSet pieDataSet2;
        PieData pieData2;
        entries2.add(new BarEntry(60, 0));
        entries2.add(new BarEntry(40, 1));

        PieEntryLabels2.add("");
        PieEntryLabels2.add("");

        final int[] piecolors2 = new int[]{
                Color.rgb(33, 228, 115),
                Color.rgb(255, 214, 141)};


        pieDataSet2 = new PieDataSet(entries2, "");
        pieData2 = new PieData(PieEntryLabels2, pieDataSet2);
        pieData2.setValueFormatter(new PercentFormatter());
        pieDataSet2.setColors(ColorTemplate.createColors(piecolors2));

        pieChart2.setData(pieData2);
        pieChart2.setDescription("");

        pieChart2.animateXY(3000, 3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);

    }

    private void setList(ArrayList<ProjectItemDetails> itemDetailsArrayList) {
        ProjectDetailsListAdapter projectDetailsListAdapter = new ProjectDetailsListAdapter(ProjectDetailsActivity.this, itemDetailsArrayList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(projectDetailsListAdapter);
    }

    @Override
    public void itemDelete(String item_id) {
        showConfirmationDialog(item_id);
        System.out.println("ITEM ID=====================" + item_id);
        // Toast.makeText(this, ""+item_id, Toast.LENGTH_SHORT).show();


    }

    private void deleteItemApi(String item_id) {
        try {

            String url = Content.baseURL + "iteams/" + item_id;
            CommonLoadingDialog.showLoadingDialog(ProjectDetailsActivity.this, "Deleting....");
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            try {

                                JSONObject jObject = new JSONObject(response);

                                if (jObject.getBoolean("status")) {
                                    Toast.makeText(ProjectDetailsActivity.this, "Item deleted successfully", Toast.LENGTH_SHORT).show();
                                    sendBroadcast(new Intent("comingBack"));
                                } else {
                                    Toast.makeText(ProjectDetailsActivity.this, "Could not deleted due to some internal error", Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectDetailsActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }


    }

    private void showConfirmationDialog(final String item_id) {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to delete?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItemApi(item_id);
                alertDialog.dismiss();

                startActivity(new Intent(ProjectDetailsActivity.this, ProjectDetailsActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();


                //  Toast.makeText(ProjectDetailsActivity.this, "You are redy to delet"+item_id, Toast.LENGTH_SHORT).show();

            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog.dismiss();

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(comingBack);
    }
}
