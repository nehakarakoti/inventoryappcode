package com.app.inventoryapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.NotificationAdapter;
import com.app.inventoryapp.adapter.ReviewHistoryAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.NotificationData;
import com.app.inventoryapp.model.ReviewHistoryModel;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReviewHistoryActivity extends BaseActivity {
    Activity mActivity = ReviewHistoryActivity.this;
    String TAG = ReviewHistoryActivity.this.getClass().getSimpleName();
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.LL_no_history_layout)
    LinearLayout LL_no_history_layout;

    ReviewHistoryAdapter mReviewHistoryAdapter;

    ArrayList<ReviewHistoryModel> modelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_history);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.back_btn)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    protected void onResume() {
        super.onResume();
//        httpGetHistory();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ReviewHistoryActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);

                    httpGetHistory();
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);

                httpGetHistory();
            }


        }

    }

    private void httpGetHistory() {
        String strURL = "http://dharmani.com/railway_api/web/activities?sort=-id&item_id=" + InventoryAppSingleton.getInstance().getItem_id() + "&activity_name=push_custom&page=1";
        try {
            //String url = Content.baseURL + "docps/history?i_id=" + InventoryAppSingleton.getInstance().getItem_id() + "&sort=-id";
            System.out.println("OUTPUT URL=====" + strURL);
            CommonLoadingDialog.showLoadingDialog(mActivity, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, strURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            Log.d("HOSTORY RESPONSE==", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status") == true){
                                    JSONArray mJsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < mJsonArray.length(); i++){
                                        JSONObject mDataObj = mJsonArray.getJSONObject(i);
                                        ReviewHistoryModel mModel = new ReviewHistoryModel();
                                        if (!mDataObj.isNull("id"))
                                            mModel.setId(mDataObj.getString("id"));

                                        if (!mDataObj.isNull("activity_name"))
                                            mModel.setActivity_name(mDataObj.getString("activity_name"));

                                        if (!mDataObj.isNull("massage"))
                                            mModel.setMassage(mDataObj.getString("massage"));

                                        if (!mDataObj.isNull("activity_by"))
                                            mModel.setActivity_by(mDataObj.getString("activity_by"));

                                        if (!mDataObj.isNull("activity_for"))
                                            mModel.setActivity_for(mDataObj.getString("activity_for"));

                                        if (!mDataObj.isNull("user_role"))
                                            mModel.setUser_role(mDataObj.getString("user_role"));

                                        if (!mDataObj.isNull("project_id"))
                                            mModel.setProject_id(mDataObj.getString("project_id"));

                                        if (!mDataObj.isNull("item_id"))
                                            mModel.setItem_id(mDataObj.getString("item_id"));

                                        if (!mDataObj.isNull("doc_id"))
                                            mModel.setDoc_id(mDataObj.getString("doc_id"));

                                        if (!mDataObj.isNull("status"))
                                            mModel.setStatus(mDataObj.getString("status"));

                                        if (!mDataObj.isNull("created_on"))
                                            mModel.setCreated_on(mDataObj.getString("created_on"));

                                        modelArrayList.add(mModel);

                                    }


                                    if (modelArrayList.size() == 0){
                                        LL_no_history_layout.setVisibility(View.VISIBLE);
                                    }else{
                                        setNotificationList(modelArrayList);
                                    }


                                }else {
                                    CommonValidationDialog.showLoadingDialog(mActivity,"Server Error!");
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setNotificationList(ArrayList<ReviewHistoryModel> notificationDataArrayList) {
        mReviewHistoryAdapter = new ReviewHistoryAdapter(mActivity, notificationDataArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mReviewHistoryAdapter);
    }


}
