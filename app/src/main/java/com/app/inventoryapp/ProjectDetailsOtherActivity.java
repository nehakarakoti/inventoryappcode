package com.app.inventoryapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.DataIndividualOther;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.model.ResponseProjectIndividualOther;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProjectDetailsOtherActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.imageView_notification)
    ImageView imageView_notification;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private ArrayList<ProjectItemModel> projectItemModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details_other);

        ButterKnife.bind(this);

        setStatus();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imageView_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProjectDetailsOtherActivity.this, ProjectNotifications.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                apiCall();
            }
        });
    }


    public void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ProjectDetailsOtherActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                    apiCall();
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);

                apiCall();
            }


        }


    }


    private void apiCall() {
        try {
            String url = Content.baseURL + "projectds/" + InventoryAppSingleton.getInstance().getProject_id() + "?expand=tblIteams";
            System.out.println("CURRENT URL=====" + url);

            refreshLayout.setRefreshing(true);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            refreshLayout.setRefreshing(false);
                            System.out.println("RESPONSE=======" + response);

                            try {
                                Log.d("RESPONSE==", response);
                                Gson gson = new Gson();
                                ResponseProjectIndividualOther responseProjectIndividualOther = gson.fromJson(response, ResponseProjectIndividualOther.class);
                                DataIndividualOther dataIndividual = responseProjectIndividualOther.getData();
                                setList(dataIndividual.getTblIteams());
                            } catch (Exception ex) {
                                refreshLayout.setRefreshing(false);

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            refreshLayout.setRefreshing(false);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectDetailsOtherActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }

    }

    private void setStatus() {


        String financialProgress = InventoryAppSingleton.getInstance().getFinancialProgress();
        String physicalProgress = InventoryAppSingleton.getInstance().getPhysicalProgress();

        System.out.println("PROJECT LEVEL PROGRESS FINANCIAL======" + financialProgress);
        System.out.println("PROJECT LEVEL PROGRESS PHYSICAL======" + physicalProgress);

        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }


    private void setList(ArrayList<ProjectItemModel> projectItemModelArrayList) {
        ProjectDetailsListAdapterOther projectDetailsListAdapterother = new ProjectDetailsListAdapterOther(ProjectDetailsOtherActivity.this, projectItemModelArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(projectDetailsListAdapterother);
    }


}
