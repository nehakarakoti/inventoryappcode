package com.app.inventoryapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.FinancialUserListAdapter;
import com.app.inventoryapp.adapter.UserListAdapter;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.GetFinacialUserInterface;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UserActivity extends BaseActivity {
    public static ArrayList<UsersResponse> mSelectedUserArrayList = new ArrayList<>();
    String TAG = UserActivity.this.getClass().getSimpleName();
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.btnPhysicalUser)
    Button btnPhysicalUser;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.input_search_user)
    EditText input_search_user;
    @BindView(R.id.imageView_clear_text)
    ImageView imageView_clear_text;

    @BindView(R.id.financialRV)
    RecyclerView financialRV;
    @BindView(R.id.supervisorRV)
    RecyclerView supervisorRV;

    UserListAdapter mSupervisorAdapter;
    FinancialUserListAdapter mFinancialUserListAdapter;

    ArrayList<UsersResponse> mFinancialUserAL = new ArrayList<>();
    ArrayList<UsersResponse> mSupervisorUserAL = new ArrayList<>();


    ArrayList<UsersResponse> mSelectedSupervisorAL = new ArrayList<>();

    UsersResponse mSingleFiananceUser;
    SelectedUserInterface mSelectedUserInterface = new SelectedUserInterface() {
        @Override
        public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList) {
            System.out.println("SIZE=======INTEFACE====" + mArrayList.size());
            mSelectedSupervisorAL = mArrayList;
            //mSelectedUserArrayList.addAll(mArrayList);
        }

        @Override
        public void changeBadgeICon(boolean t) {

        }
    };


    GetFinacialUserInterface mGetFinacialUserInterface = new GetFinacialUserInterface() {
        @Override
        public void getFinancialUser(UsersResponse mUsersResponse) {
            //Single Fianancial User
            mSingleFiananceUser = mUsersResponse;
            mSelectedUserArrayList.clear();
            mSelectedUserArrayList.add(mSingleFiananceUser);
            Log.e(TAG,"===FinancialUser==="+mSelectedUserArrayList.size());
        }
    };

    boolean contains(ArrayList<UsersResponse> list, String mUserID) {
        for (UsersResponse item : list) {
            if (item.getId().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }
    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {
            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                    loggingOut(UserActivity.this);
                }

            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }
        }
//        if (PhysicalUserActivity.mSelectedPhysicalUserAL.size() > 0)
//            mSelectedUserArrayList.addAll(PhysicalUserActivity.mSelectedPhysicalUserAL);
//        if (mSingleFiananceUser != null){
//            if (contains(mSelectedUserArrayList,mSingleFiananceUser.getId()) == false){
//                mSelectedUserArrayList.add(mSingleFiananceUser);
//            }
//        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        setClickListner();
        httpGetUsersCall();
    }


    private void setClickListner() {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserActivity.this, AddUserActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        imageView_clear_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                input_search_user.setText("");
                imageView_clear_text.setVisibility(View.GONE);
            }
        });

        input_search_user.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                userListAdapter.getFilter().filter(s);
//                if (s.toString().length() > 0) {
//                    imageView_clear_text.setVisibility(View.VISIBLE);
//                } else {
//                    imageView_clear_text.setVisibility(View.GONE);
//                }
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnPhysicalUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedUserArrayList.size() > 0 &&  mSelectedSupervisorAL.size() > 0) {
                    Intent mIntent = new Intent(UserActivity.this, PhysicalUserActivity.class);
                    mIntent.putExtra("LIST", mSelectedSupervisorAL);
                    startActivity(mIntent);
                    finish();
                } else {
                    CommonValidationDialog.showLoadingDialog(UserActivity.this, "Please select atleast one  Bill Clerk and one Officer In charge");
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void httpGetUsersCall() {
        try {
            final String url = Content.baseURL + "users?sort=-id";
            CommonLoadingDialog.showLoadingDialog(UserActivity.this, "Loading...");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                System.out.println("SIZE====" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                    UsersResponse usersResponse = new UsersResponse();
                                    if (!jsonObject1.isNull("id"))
                                        usersResponse.setId(jsonObject1.getString("id"));
                                    if (!jsonObject1.isNull("username"))
                                        usersResponse.setUsername(jsonObject1.getString("username"));
                                    if (!jsonObject1.isNull("usertype"))
                                        usersResponse.setUsertype(jsonObject1.getString("usertype"));
                                    if (!jsonObject1.isNull("name"))
                                        usersResponse.setName(jsonObject1.getString("name"));

                                    if (!jsonObject1.isNull("supervisor"))
                                        usersResponse.setSupervisor(jsonObject1.getString("supervisor"));

                                    if (!jsonObject1.isNull("physical"))
                                        usersResponse.setPhysical(jsonObject1.getString("physical"));
                                    //trial adding
                                    if (!jsonObject1.isNull("password"))
                                        usersResponse.setPassword(jsonObject1.getString("password"));

                                    if (jsonObject1.getString("usertype").equals("Financial")) {
                                        mFinancialUserAL.add(usersResponse);
                                    } else if (jsonObject1.getString("usertype").equals("Supervisor")) {
                                        mSupervisorUserAL.add(usersResponse);
                                    }

                                }

                                System.out.println("Financial SIZE====" + mFinancialUserAL.size());
                                System.out.println("Supervisor SIZE====" + mSupervisorUserAL.size());


                                /*Set Both user Adapters*/
                                setFinancialUserAdapter();
                                setSupervisorUserAdapter();

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UserActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setFinancialUserAdapter() {
        mFinancialUserListAdapter = new FinancialUserListAdapter(UserActivity.this, mFinancialUserAL, mGetFinacialUserInterface);
        financialRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        financialRV.setLayoutManager(layoutManager);
        financialRV.setItemAnimator(new DefaultItemAnimator());
        financialRV.setAdapter(mFinancialUserListAdapter);
    }

    private void setSupervisorUserAdapter() {
        mSupervisorAdapter = new UserListAdapter(UserActivity.this, mSupervisorUserAL, mSelectedUserInterface);
        supervisorRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        supervisorRV.setLayoutManager(layoutManager);
        supervisorRV.setItemAnimator(new DefaultItemAnimator());
        supervisorRV.setAdapter(mSupervisorAdapter);
    }



}
