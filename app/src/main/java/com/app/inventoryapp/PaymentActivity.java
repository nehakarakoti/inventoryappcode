package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.font.CustomFontArialGeoBoldTextView;
import com.app.inventoryapp.font.CustomTextView;
import com.app.inventoryapp.model.DataItem;
import com.app.inventoryapp.model.UnpaidResponseModel;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.CompressImage;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.DocPath;
import com.app.inventoryapp.utils.MultipartUtility;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PaymentActivity extends BaseActivity {

    @BindView(R.id.view)
    View view;

InventoryAppSession inventoryAppSession;
    DataItem dataItem = new DataItem();
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.textView_date)
    CustomFontArialGeoBoldTextView textViewDate;
    @BindView(R.id.textView_itemQuantity)
    CustomFontArialGeoBoldTextView textViewItemQuantity;
    @BindView(R.id.textView_itemAmount)
    CustomFontArialGeoBoldTextView textViewItemAmount;

    @BindView(R.id.imageView_add_doc)
    ImageView imageView_add_doc;

    @BindView(R.id.fileChooseInfo)
    TextView fileChooseInfo;

    Double amount;
    @BindView(R.id.docImage)
    ImageView docImage;

    File fileToUpload;

    String filePath, fileName;
    String fileExtension;

    private static final int CHOOSE_FILE_REQUESTCODE = 102;
    boolean isFileSelected = false;

    InventoryAppSession session;
    AlertDialog.Builder alertDialogBuilder;

    @BindView(R.id.saveBtn)
    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        dataItem = (DataItem) getIntent().getExtras().getSerializable("dataItem");
        session = new InventoryAppSession(getApplicationContext());
        alertDialogBuilder = new AlertDialog.Builder(PaymentActivity.this);
        System.out.println("DATA============" + dataItem.getDocName());

        setStatus();

        setData();

        imageView_add_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fileChooseInfo.setVisibility(View.GONE);
                String[] mimeTypes =
                        {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                "text/plain",
                                "application/pdf",
                                "application/zip"};

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                    if (mimeTypes.length > 0) {
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    }
                } else {
                    String mimeTypesStr = "";
                    for (String mimeType : mimeTypes) {
                        mimeTypesStr += mimeType + "|";
                    }
                    intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
                }
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent, CHOOSE_FILE_REQUESTCODE);

            }

        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                fileExtension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(filePath);
                if(!filePath.equals(""))
                {
                    if(fileExtension.equals("pdf") || fileExtension.equals("jpg") || fileExtension.equals("png") || fileExtension.equals("jpeg"))
                    {



                            showConfirmationDialog();

                    }
                    else
                    {
                        CommonValidationDialog.showLoadingDialog(PaymentActivity.this, "Document must be in pdf, jpg, jpeg or png format");
                    }

                }
                else
                {
                    CommonValidationDialog.showLoadingDialog(PaymentActivity.this, "Please select document of jpg, jpeg, pdf or png format");


                }

            }
        });


    }


    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to payment?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
              new paymentUpload().execute();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog.dismiss();

            }
        });
    }

    private class paymentUpload extends AsyncTask<String, String, String> {


        StringBuilder response = new StringBuilder();
        String stringData = "";


        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            String charset = "UTF-8";
            String requestURL = Content.baseURL + "docps?check=2";

            System.out.println("URL=========================" + requestURL);


            try {

                MultipartUtility multipart = new MultipartUtility(requestURL, charset);

                multipart.addFormField("project_id", InventoryAppSingleton.getInstance().getProject_id());
                multipart.addFormField("user_id", "" + dataItem.getUserId());
                multipart.addFormField("iteam_id", InventoryAppSingleton.getInstance().getItem_id());
                multipart.addFormField("paid_user", "" + session.getKeyUserId());
                multipart.addFormField("qty_done", "" + dataItem.getQtyDone());
                multipart.addFormField("sub_item", "" + dataItem.getId());
                multipart.addFormField("amount_paid", "" + amount);
                multipart.addFilePart("pay_img", fileToUpload);

                response = multipart.finish();


                System.out.println("RESPONSE MULTIPART===" + response);

            } catch (Exception ex) {
                CommonLoadingDialog.closeLoadingDialog();
                System.out.println(" " + ex);
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonLoadingDialog.showLoadingDialog(PaymentActivity.this, "Uploading....");

        }


        @Override
        protected void onPostExecute(String jsonStr) {
            System.out.println("POST EXECUTION");
            try {
                CommonLoadingDialog.closeLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());

                    System.out.println("PAYMENT RESPONSE========================" + jsonObject.toString());

                    System.out.println("RESPONSE MULTIPART======" + jsonObject);

                    if (jsonObject.getBoolean("status"))
                    {


                        startActivity(new Intent(PaymentActivity.this, UpdateDetailsFinancialActivity.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();

                    }
                    else
                    {

                    }


                } catch (Exception ex) {
                    System.out.println("ERROR=========1" + ex.toString());
                }


                //finish();
            } catch (Exception ex) {
                System.out.println("ERROR=========2" + ex.toString());
            }


        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case CHOOSE_FILE_REQUESTCODE:

                try {


                    if (data != null) {
                        Uri fileUri = data.getData();
                        if (!DocPath.getPath(getApplicationContext(), data.getData()).equals("")) {
                            fileToUpload = new File(DocPath.getPath(getApplicationContext(), data.getData()));
                            filePath = fileToUpload.getAbsolutePath();
                            fileName = fileToUpload.getName();


                            fileExtension = MimeTypeMap.getFileExtensionFromUrl(filePath);

                            System.out.println("OUTPUT=============" + fileExtension);

                            if (fileExtension.equals("jpg") || fileExtension.equals("png") || fileExtension.equals("jpeg")) {

                                System.out.println("File SIZE BEFORE UPLOAD======" + CompressImage.getReadableFileSize(fileToUpload.length()));

                                fileToUpload = new File(new CompressImage(getApplicationContext()).compressImage(fileToUpload.getAbsolutePath()));
                                System.out.println("File SIZE BEFORE UPLOAD======" + CompressImage.getReadableFileSize(fileToUpload.length()));

                                Picasso.with(getApplicationContext())
                                        .load(fileToUpload)
                                        .resize(300, 300)
                                        .placeholder(R.drawable.place_holder)
                                        .into(imageView_add_doc);

                            } else {
                                imageView_add_doc.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.doc_image));
                            }


                            isFileSelected = true;


                            System.out.println("PAYMENT PROOF FILE PATH==========" + fileToUpload.getAbsolutePath());
                            System.out.println("PAYMENT PROOF FILE NAME==========" + fileToUpload.getName());


                        }
                        else
                        {
                            fileChooseInfo.setVisibility(View.VISIBLE);
                        }


                    } else {
                        isFileSelected = false;
                        fileChooseInfo.setVisibility(View.VISIBLE);
                       // Toast.makeText(this, "You have not choose file..", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    System.out.println("ERROR================" + ex);
                }

               /* //System.out.println("OUTPUT OF FILE PATH==="+data);
                //System.out.println("OUTPUT OF FILE PATH==="+fileToUpload.getPath());*/

                break;
        }
    }

    private void setData() {
        Double rate = UpdateDetailsFinancialActivity.rate_per_unit;
        amount = Double.parseDouble(dataItem.getQtyDone()) * rate;

        String date = dataItem.getCreatedOn().split(" ")[0];
        textViewDate.setText("Date:  " + date);
        textViewItemQuantity.setText("Quantity:  " + dataItem.getQtyDone());
        textViewItemAmount.setText("Amount:  " + amount);

        Picasso.with(getApplicationContext())
                .load(Content.baseURL + dataItem.getDocName())
                .error(R.drawable.doc_image)
                .placeholder(R.drawable.place_holder)
                .resize(300, 300)
                .into(docImage);


    }


    private void setStatus() {
        /*Setting color in item level*/

        String itemLevelPhysicalProgress = InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel();
        String itemLevelFinancialProgress = InventoryAppSingleton.getInstance().getFinancialProgressItemlevel();


        System.out.println("ITEM LEVEL PROGRESS RITU========PHYSICAL" + itemLevelPhysicalProgress);
        System.out.println("ITEM LEVEL PROGRESS RITU========FINANCIAL" + itemLevelFinancialProgress);


        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }

    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(PaymentActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);


                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);


            }


        }

    }
}
