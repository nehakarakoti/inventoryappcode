package com.app.inventoryapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.EditFinancialUserListAdapter;
import com.app.inventoryapp.adapter.UserListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.GetFinacialUserInterface;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProjectUserActivity extends AppCompatActivity {
    public static ArrayList<UsersResponse> mSelectedUserArrayListEDIT = new ArrayList<>();
    Activity mActivity = EditProjectUserActivity.this;
    String TAG = EditProjectUserActivity.this.getClass().getSimpleName();
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.input_search_user)
    EditText inputSearchUser;
    @BindView(R.id.imageView_clear_text)
    ImageView imageViewClearText;
    @BindView(R.id.financialRV)
    RecyclerView financialRV;
    @BindView(R.id.supervisorRV)
    RecyclerView supervisorRV;
    @BindView(R.id.LL_data_container)
    LinearLayout LLDataContainer;
    @BindView(R.id.btnPhysicalUser)
    Button btnPhysicalUser;
    @BindView(R.id.btnUpdateOnlyPhyUser)
    Button btnUpdateOnlyPhyUser;
    UserListAdapter mSupervisorAdapter;
    EditFinancialUserListAdapter mEditFinancialUserListAdapter;

    ArrayList<UsersResponse> mFinancialUserAL = new ArrayList<>();
    ArrayList<UsersResponse> mSupervisorUserAL = new ArrayList<>();
    ArrayList<UserDataModel> userDataModelArrayList = new ArrayList<UserDataModel>();


    ArrayList<UsersResponse> mSelectedSupervisorAL = new ArrayList<>();

    String strAlreadyAddedFinancialUser = "";

    String str_F_User = "";
    String str_S_User = "";
    String str_P_User = "";


    UsersResponse mSingleFiananceUser;
    SelectedUserInterface mSelectedUserInterface = new SelectedUserInterface() {
        @Override
        public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList) {
            System.out.println("SIZE=======INTEFACE====" + mArrayList.size());
            mSelectedSupervisorAL = mArrayList;
        }

        @Override
        public void changeBadgeICon(boolean t) {

        }
    };


    GetFinacialUserInterface mGetFinacialUserInterface = new GetFinacialUserInterface() {
        @Override
        public void getFinancialUser(UsersResponse mUsersResponse) {
            //Single Fianancial User
            mSingleFiananceUser = mUsersResponse;

            strAlreadyAddedFinancialUser = mSingleFiananceUser.getId();
            str_F_User = mSingleFiananceUser.getId();

            mSelectedUserArrayListEDIT.clear();
            mSelectedUserArrayListEDIT.add(mSingleFiananceUser);
            Log.e(TAG, "===FinancialUser===" + mSelectedUserArrayListEDIT.size());
        }
    };

    boolean contains(ArrayList<UsersResponse> list, String mUserID) {
        for (UsersResponse item : list) {
            if (item.getId().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_project_user);
        ButterKnife.bind(this);
        userDataModelArrayList = (ArrayList<UserDataModel>) getIntent().getExtras().getSerializable("userDataModelArrayList");

        System.out.println("SIZE OF PROJECT USERS====" + userDataModelArrayList.size());
        setClickListner();
        httpGetUsersCall();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void setClickListner() {
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnPhysicalUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strAlreadyAddedFinancialUser != null && strAlreadyAddedFinancialUser.length() > 0 && mSelectedSupervisorAL.size() > 0) {
                    Intent mIntent = new Intent(mActivity, EditPhysicalUserActivity.class);
                    mIntent.putExtra("LIST", mSelectedSupervisorAL);
                    mIntent.putExtra("P_SEL_LIST", userDataModelArrayList);
                    mIntent.putExtra("F_ID", strAlreadyAddedFinancialUser);
                    startActivity(mIntent);
                    finish();
                } else {
                    CommonValidationDialog.showLoadingDialog(mActivity, "Please select atleast one Bill Clerk  and one Officer In charge");
                }
            }
        });


        btnUpdateOnlyPhyUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (str_F_User != null && str_F_User.length() > 0) {
                    validateUpdateFinancialOnly();
                } else {
                    CommonValidationDialog.showLoadingDialog(mActivity, "Please select atleast one Bill Clerk");
                }
            }
        });
    }

    private void validateUpdateFinancialOnly() {

        for (int i = 0; i < userDataModelArrayList.size(); i++) {
            if (userDataModelArrayList.get(i).getUsertype().equals("Supervisor")) {
                    if (str_S_User.equals("")) {
                        str_S_User = userDataModelArrayList.get(i).getId();
                    } else {
                        str_S_User = str_S_User + "," + userDataModelArrayList.get(i).getId();
                    }
            } else if (userDataModelArrayList.get(i).getUsertype().equals("Physical")) {
                if (str_P_User.equals("")) {
                    str_P_User = userDataModelArrayList.get(i).getId();
                } else {
                    str_P_User = str_P_User + "," + userDataModelArrayList.get(i).getId();
                }
            }
        }


        str_F_User = strAlreadyAddedFinancialUser;


        Log.e(TAG,"=====FINANCIAL===="+str_F_User);
        Log.e(TAG,"====PHYSICAL====="+str_P_User);
        Log.e(TAG,"====SUPERVISOR====="+str_S_User);


        httpAddUSer();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mActivity, ProjectUsersActivity.class));
        finish();
    }

    private void httpGetUsersCall() {
        try {
            final String url = Content.baseURL + "users?sort=-id";
            CommonLoadingDialog.showLoadingDialog(mActivity, "Loading...");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                System.out.println("SIZE====" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                    UsersResponse usersResponse = new UsersResponse();
                                    if (!jsonObject1.isNull("id"))
                                        usersResponse.setId(jsonObject1.getString("id"));
                                    if (!jsonObject1.isNull("username"))
                                        usersResponse.setUsername(jsonObject1.getString("username"));
                                    if (!jsonObject1.isNull("usertype"))
                                        usersResponse.setUsertype(jsonObject1.getString("usertype"));
                                    if (!jsonObject1.isNull("name"))
                                        usersResponse.setName(jsonObject1.getString("name"));

                                    if (!jsonObject1.isNull("supervisor"))
                                        usersResponse.setSupervisor(jsonObject1.getString("supervisor"));

                                    if (!jsonObject1.isNull("physical"))
                                        usersResponse.setPhysical(jsonObject1.getString("physical"));
                                    //trial adding
                                    if (!jsonObject1.isNull("password"))
                                        usersResponse.setPassword(jsonObject1.getString("password"));

                                    if (jsonObject1.getString("usertype").equals("Financial")) {
                                        mFinancialUserAL.add(usersResponse);
                                    } else if (jsonObject1.getString("usertype").equals("Supervisor")) {
                                        mSupervisorUserAL.add(usersResponse);
                                    }

                                }

                                System.out.println("Financial SIZE====" + mFinancialUserAL.size());
                                System.out.println("Supervisor SIZE====" + mSupervisorUserAL.size());


                                /*Set Both user Adapters*/
                                setFinancialUserAdapter();
                                setSupervisorUserAdapter();

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setFinancialUserAdapter() {
        ArrayList<UsersResponse> finalFinancialUserAL = new ArrayList<>();

        for (int i = 0; i < mFinancialUserAL.size(); i++) {
            UsersResponse mResponse = mFinancialUserAL.get(i);
            if (!mResponse.getId().equals(strAlreadyAddedFinancialUser)) {
                finalFinancialUserAL.add(mResponse);
            }
        }

        mEditFinancialUserListAdapter = new EditFinancialUserListAdapter(mActivity, finalFinancialUserAL, mGetFinacialUserInterface, strAlreadyAddedFinancialUser);
        financialRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        financialRV.setLayoutManager(layoutManager);
        financialRV.setItemAnimator(new DefaultItemAnimator());
        financialRV.setAdapter(mEditFinancialUserListAdapter);
    }

    private void setSupervisorUserAdapter() {
        mSupervisorAdapter = new UserListAdapter(mActivity, mSupervisorUserAL, mSelectedUserInterface);
        supervisorRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        supervisorRV.setLayoutManager(layoutManager);
        supervisorRV.setItemAnimator(new DefaultItemAnimator());
        supervisorRV.setAdapter(mSupervisorAdapter);
    }


    private void httpAddUSer() {
        try {
            final String url = Content.baseURL + "projects/" + InventoryAppSingleton.getInstance().getProject_id();

            CommonLoadingDialog.showLoadingDialog(mActivity, "Creating...");


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            System.out.println("RESPONSE==========" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    startActivity(new Intent(mActivity, ProjectUsersActivity.class));
                                    finish();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    String startingDate[] = InventoryAppSingleton.getInstance().getStarting_date().split("-");
                    String endingDate[] = InventoryAppSingleton.getInstance().getEnding_date().split("-");


                    params.put("starting_date", startingDate[2] + "-" + startingDate[1] + "-" + startingDate[0]);
                    params.put("ending_date", endingDate[2] + "-" + endingDate[1] + "-" + endingDate[0]);
                    params.put("s_user", str_S_User);
                    params.put("f_user", str_F_User);
                    params.put("user_id", str_P_User);
                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("ERROR====" + ex);
        }

    }


}
