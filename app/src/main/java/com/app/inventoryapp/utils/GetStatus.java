package com.app.inventoryapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by RITUPARNA on 6/6/2018.
 */

public class GetStatus
{

    public static long dayDiff( String completion_date)
    {
        long diff = 0;

        try {
                String completion_date_string = completion_date;
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                Date date_completation = formatter.parse(completion_date_string);
                Date date_current = new Date();
                String curent_date_string = formatter.format(date_current);

                System.out.println("RITUPARNA=====ENDING DATE TEST=============="+date_completation.getTime());
                System.out.println("RITUPARNA=====CURRENT DATE TEST=============="+date_current.getTime());


                date_current = formatter.parse(curent_date_string);
                diff = date_completation.getTime() - date_current.getTime();

            }

            catch (Exception ex)
            {
                    System.out.println("ERROR====="+ex);
            }
        return diff;
    }

}
