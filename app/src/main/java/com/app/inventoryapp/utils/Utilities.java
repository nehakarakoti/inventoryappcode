package com.app.inventoryapp.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.app.inventoryapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by android-da on 11/22/18.
 */

public class Utilities {

    public static void removeDividerFromELV(Context mContext, ExpandableListView mExpandableListView) {
        mExpandableListView.setGroupIndicator(null);
        mExpandableListView.setChildIndicator(null);
        mExpandableListView.setChildDivider(mContext.getResources().getDrawable(R.color.color_white));
        mExpandableListView.setDivider(mContext.getResources().getDrawable(R.color.color_white));
        mExpandableListView.setDividerHeight(2);
    }

    public static void justifyListViewHeightBasedOnChildren(ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }


    //    ending_date = "29-11-2018"
//    starting_date = "29-11-2018"
    public static String convertOneToAnotherFormat(String strDate) throws ParseException {
        String strActualFormat = "";
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date d = format.parse(strDate);
        format = new SimpleDateFormat("dd-MMM-yyyy");
        strActualFormat = format.format(d);
        return strActualFormat;
    }

    /*Physical User => SSE
Financial user => Bill Clerk
Supervisors => Officer In charge
Contractor Name=> Firm Name
Admin => Bill Passing Authority
Comment:-will do*/

    /*<item>Physical</item>
        <item>Financial</item>
        <item>Supervisor</item>*/

    public static String getUpdatedRoleText(String strRole) {
        String strNewRole = "";
        if (strRole.equals("Physical")) {
            strNewRole = "SSE";
            return strNewRole;
        } else if (strRole.equals("Financial")) {
            strNewRole = "Bill Clerk";
            return strNewRole;
        } else if (strRole.equals("Supervisor")) {
            strNewRole = "Officer In charge";
            return strNewRole;
        } else if (strRole.equals("Contractor Name")) {
            strNewRole = "Firm Name";
            return strNewRole;
        } else if (strRole.equals("Admin")) {
            strNewRole = "Bill Passing Authority";
            return strNewRole;
        } else if (strRole.equals("Select role")) {
            strNewRole = "Select role";
            return strNewRole;
        }else if (strRole.equals("Select Officer In charge")) {
            strNewRole = "Select Officer In charge";
            return strNewRole;
        }
        return "";
    }
    public static String getPreviousRoleText(String strRole) {
        String strNewRole = "";
        if (strRole.equals("SSE")) {
            strNewRole = "Physical";
            return strNewRole;
        } else if (strRole.equals("Bill Clerk")) {
            strNewRole = "Financial";
            return strNewRole;
        } else if (strRole.equals("Officer In charge")) {
            strNewRole = "Supervisor";
            return strNewRole;
        } else if (strRole.equals("Firm Name")) {
            strNewRole = "Contractor Name";
            return strNewRole;
        } else if (strRole.equals("Bill Passing Authority")) {
            strNewRole = "Admin";
            return strNewRole;
        } else if (strRole.equals("Select role")) {
            strNewRole = "Select role";
            return strNewRole;
        }else if (strRole.equals("Select Officer In charge")) {
            strNewRole = "Select Officer In charge";
            return strNewRole;
        }
        return "";
    }


    public static void hideKeyBoardFromView(Activity mActivity) {
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus();
        if (view == null) {
            view = new View(mActivity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(Activity mActivity) {
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


}
