package com.app.inventoryapp.utils;

import com.app.inventoryapp.model.ProjectDataModel;

import java.util.ArrayList;

import volley.PhysicalUser;

public class ProjectData {
    private String status;

    private ArrayList<PhysicalUser> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<PhysicalUser> getData() {
        return data;
    }

    public void setData(ArrayList<PhysicalUser> data) {
        this.data = data;
    }
}
