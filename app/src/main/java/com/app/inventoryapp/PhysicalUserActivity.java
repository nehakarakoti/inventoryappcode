package com.app.inventoryapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ExpandableListAdapter;
import com.app.inventoryapp.adapter.PhysicalUserListAdapter;
import com.app.inventoryapp.adapter.UserListAdapter;
import com.app.inventoryapp.model.SupervisorWithPhysicalUsers;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.PhysicalSelectedUserInterface;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.Utilities;
import com.app.inventoryapp.views.NonScrollExpandableListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhysicalUserActivity extends BaseActivity {
    public static ArrayList<UsersResponse> mSelectedPhysicalUserAL = new ArrayList<>();
    Activity mActivity = PhysicalUserActivity.this;
    String TAG = PhysicalUserActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.imageView_clear_text)
    ImageView imageView_clear_text;
    @BindView(R.id.input_search_user)
    EditText input_search_user;
    @BindView(R.id.physicalRV)
    RecyclerView physicalRV;

    ArrayList<UsersResponse> mSupervisorAL = new ArrayList<>();
    ArrayList<SupervisorWithPhysicalUsers> mSupervisorWithPhysicalAL = new ArrayList<SupervisorWithPhysicalUsers>();
    PhysicalUserListAdapter mPhysicalUserListAdapter;

    String strUserIDs = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physical_user);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            mSupervisorAL = (ArrayList<UsersResponse>) getIntent().getSerializableExtra("LIST");
            for (int i = 0; i < mSupervisorAL.size(); i++) {
                if (i < mSupervisorAL.size() - 1) {
                    strUserIDs = strUserIDs + mSupervisorAL.get(i).getId() + ",";
                } else {
                    strUserIDs = strUserIDs + mSupervisorAL.get(i).getId();
                }
            }
            Log.e(TAG, "========IDS=====" + strUserIDs);

            httpGetUsersCall();
        }
    }



    @OnClick({R.id.imgBackIV, R.id.imageView_clear_text})
    public void onViewClicked(View mView) {
        switch (mView.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imageView_clear_text:
                input_search_user.setText("");
                imageView_clear_text.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        getAllUsersList();

        UserActivity.mSelectedUserArrayList.addAll(physicalUserArrayList);
        UserActivity.mSelectedUserArrayList.addAll(finalSupervisorArrayList);

        finish();
    }

    private void httpGetUsersCall() {
        try {
            final String url = Content.GET_PHYSICAL_USERS_ACCORING_SUPERVISOR + strUserIDs;
            Log.e(TAG, "==URL==" + url);
            CommonLoadingDialog.showLoadingDialog(mActivity, "Loading...");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                System.out.println("SIZE====" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                    SupervisorWithPhysicalUsers mModel = new SupervisorWithPhysicalUsers();
                                    if (!jsonObject1.isNull("id"))
                                        mModel.setId(jsonObject1.getString("id"));
                                    if (!jsonObject1.isNull("username"))
                                        mModel.setUsername(jsonObject1.getString("username"));
                                    if (!jsonObject1.isNull("usertype"))
                                        mModel.setUsertype(jsonObject1.getString("usertype"));
                                    if (!jsonObject1.isNull("name"))
                                        mModel.setName(jsonObject1.getString("name"));
                                    if (!jsonObject1.isNull("password"))
                                        mModel.setPassword(jsonObject1.getString("password"));
                                    if (!jsonObject1.isNull("cellno"))
                                        mModel.setCellno(jsonObject1.getString("cellno"));
                                    if (!jsonObject1.isNull("user_location"))
                                        mModel.setUser_location(jsonObject1.getString("user_location"));
                                    if (!jsonObject1.isNull("role"))
                                        mModel.setRole(jsonObject1.getString("role"));
                                    if (!jsonObject1.isNull("device_token"))
                                        mModel.setDevice_token(jsonObject1.getString("device_token"));
                                    if (!jsonObject1.isNull("supervisor"))
                                        mModel.setSupervisor(jsonObject1.getString("supervisor"));
                                    if (!jsonObject1.isNull("physical"))
                                        mModel.setPhysical(jsonObject1.getString("physical"));
                                    if (!jsonObject1.isNull("status"))
                                        mModel.setStatus(jsonObject1.getString("status"));

                                    if (!jsonObject1.isNull("physicalUser")) {
                                        JSONArray mJsonArray1 = jsonObject1.getJSONArray("physicalUser");
                                        ArrayList<UsersResponse> mSubPhyscialAL = new ArrayList<>();
                                        mSubPhyscialAL.clear();
                                        for (int k = 0; k < mJsonArray1.length(); k++) {
                                            JSONObject mSubObj = mJsonArray1.getJSONObject(k);
                                            UsersResponse mUsersResponse = new UsersResponse();

                                            if (!mSubObj.isNull("id"))
                                                mUsersResponse.setId(mSubObj.getString("id"));
                                            if (!mSubObj.isNull("username"))
                                                mUsersResponse.setUsername(mSubObj.getString("username"));
                                            if (!mSubObj.isNull("usertype"))
                                                mUsersResponse.setUsertype(mSubObj.getString("usertype"));
                                            if (!mSubObj.isNull("name"))
                                                mUsersResponse.setName(mSubObj.getString("name"));
                                            if (!mSubObj.isNull("password"))
                                                mUsersResponse.setPassword(mSubObj.getString("password"));
                                            if (!mSubObj.isNull("cellno"))
                                                mUsersResponse.setCellno(mSubObj.getString("cellno"));
                                            if (!mSubObj.isNull("user_location"))
                                                mUsersResponse.setUser_location(mSubObj.getString("user_location"));
                                            if (!mSubObj.isNull("role"))
                                                mUsersResponse.setRole(mSubObj.getString("role"));
                                            if (!mSubObj.isNull("device_token"))
                                                mUsersResponse.setDevice_token(mSubObj.getString("device_token"));
                                            if (!mSubObj.isNull("supervisor"))
                                                mUsersResponse.setSupervisor(mSubObj.getString("supervisor"));
                                            if (!mSubObj.isNull("physical"))
                                                mUsersResponse.setPhysical(mSubObj.getString("physical"));
                                            if (!mSubObj.isNull("status"))
                                                mUsersResponse.setStatus(mSubObj.getString("status"));

                                            mSubPhyscialAL.add(mUsersResponse);
                                        }

                                        mModel.setmSubPhysicalUserAL(mSubPhyscialAL);
                                    }

                                    mSupervisorWithPhysicalAL.add(mModel);
                                }


                                /*Set Adapter*/
                                setmExpandableListAdapter();

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    private void setmExpandableListAdapter(){
        mPhysicalUserListAdapter = new PhysicalUserListAdapter(mActivity, mSupervisorWithPhysicalAL, mSelectedUserInterface);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        physicalRV.setLayoutManager(layoutManager);
        physicalRV.setItemAnimator(new DefaultItemAnimator());
        physicalRV.setAdapter(mPhysicalUserListAdapter);
    }
    private ArrayList<UsersResponse> physicalUserArrayList = new ArrayList<>();
    private ArrayList<UsersResponse> finalSupervisorArrayList = new ArrayList<>();

    PhysicalSelectedUserInterface mSelectedUserInterface = new PhysicalSelectedUserInterface() {
        @Override
        public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList,boolean isSelectedUnSelect, UsersResponse mResponse) {

            if (isSelectedUnSelect == true){
                for (int i = 0; i < mArrayList.size(); i++){
                    UsersResponse mUsersResponse = mArrayList.get(i);
                    if (contains(physicalUserArrayList,mUsersResponse.getId()) == false){
                        physicalUserArrayList.add(mUsersResponse);
                    }
                }
            }else if (isSelectedUnSelect == false){
                physicalUserArrayList.remove(mResponse);
            }


            Log.e(TAG , "=====SIZE====="+physicalUserArrayList.size());
        }
    };

    boolean contains(ArrayList<UsersResponse> list, String mUserID) {
        for (UsersResponse item : list) {
            if (item.getId().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }




    private void getAllUsersList(){
        for (int i = 0; i < mSupervisorAL.size(); i ++){
            for (int j = 0; j < physicalUserArrayList.size(); j++){
                if (mSupervisorAL.get(i).getId().equals(physicalUserArrayList.get(j).getSupervisor())){

                    UsersResponse mUsersResponse = mSupervisorAL.get(i);
                    if (contains(finalSupervisorArrayList,mUsersResponse.getId()) == false){
                        finalSupervisorArrayList.add(mUsersResponse);
                    }
                }
            }
        }

        Log.e(TAG,"=====Supervisor Size===="+finalSupervisorArrayList.size());

    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }

    }
}
