package com.app.inventoryapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.app.inventoryapp.adapter.SelectedUserAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.app.MyApplication;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.UserRemoveEventListener;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.MultipartUtility;
import com.app.inventoryapp.utils.MyFileExtension;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import volley.VolleyMultipartRequest;

import static android.view.View.GONE;


//import com.android.volley.request.SimpleMultiPartRequest;

public class AddProjectActivity extends BaseActivity implements UserRemoveEventListener {
/*@BindView(R.id.imageView)
    ImageView imageView;*/

    public static final int USER_TAKE_CODE = 103;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int CHOOSE_FILE_REQUESTCODE = 102;
    private static final int TRYRIAl = 111;
    InputMethodManager imm;
    File f;

    @BindView(R.id.input_extended_date)
    EditText input_extended_date;
    @BindView(R.id.Btn_addUser)
    Button Btn_addUser;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.imageView_add_project)
    ImageView imageView_add_project;

    /*@BindView(R.id.imageView_doc)
    ImageView imageView_doc;*/
    @BindView(R.id.user_imageView)
    ImageView user_imageView;

    /* @BindView(R.id.LL_doc_container)
     LinearLayout LLDocContainer;*/
    @BindView(R.id.input_complation_date)
    EditText input_complation_date;
    @BindView(R.id.btn_save)
    Button btn_save;
    @BindView(R.id.inputProjectName)
    EditText inputProjectName;
    @BindView(R.id.textFileName)
    TextView textFileName;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.listView)
    ListView listView;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @BindView(R.id.LL_list_container)
    LinearLayout LL_list_container;

    @BindView(R.id.RL_image_container)
    RelativeLayout RL_image_container;

    @BindView(R.id.imageView_close)
    ImageView imageView_close;

    @BindView(R.id.imageView_add_doc2)
    ImageView imageView_add_doc2;


    @BindView(R.id.LL_add_btn_container)
    LinearLayout LL_add_btn_container;

    @BindView(R.id.LL_doc_contianer)
    LinearLayout LL_doc_contianer;
@BindView(R.id.llExtendedDate)
        LinearLayout llExtendedDate;

    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    File fileToUpload;
    AlertDialog.Builder alertDialogBuilder;
    SelectedUserAdapter selectedUserAdapter;
    String fileExtension;
    byte[] strByteArray = null;
    Boolean added = false;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private String userId;
    private int mYear, mMonth, mDay;
    private String projectFilePath = "";
    private String projectFileName = "";
    private String user_id = ""; // Physical user id
    private String f_user = "";
    private String s_user = "";
    private String mCompletationDate,extendeddate="0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);
        added = getIntent().getBooleanExtra("added", false);
        if (added) {
            InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());
        }
        ButterKnife.bind(this);
        intit();

        /*Flushing Data of bunch of user */


//        InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());


        input_complation_date.setFocusableInTouchMode(false);
        input_complation_date.setFocusable(false);
input_extended_date.setFocusable(false);
input_extended_date.setFocusableInTouchMode(false);
        Btn_addUser = (Button) findViewById(R.id.Btn_addUser);

        setCurrentDate();


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        permissionControl();

        Btn_addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddProjectActivity.this, UserActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

            }
        });


        imageView_add_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    permissionControl();
                } else {
                    proceedAfterPermission();
                }


                //permissionControl();
            }
        });


        /*
         * Thsi block is used ti check the flow of screen with functionality*/
        if (InventoryAppSingleton.getInstance().getFromWhere().equals("HOME")) {
            InventoryAppSingleton.getInstance().setFromWhere("");
            System.out.println("OUT PUT FLOW OF SCREEN=====" + InventoryAppSingleton.getInstance().getFromWhere());


        }
        input_extended_date.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        hideKeyBoard(llExtendedDate);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddProjectActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        //  input_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        //  input_complation_date.setText(year+"-"+monthOfYear+"-"+dayOfMonth);

                        extendeddate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                        input_extended_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        //input_complation_date.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.show();

    }
});
        input_complation_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard(input_complation_date);
//hideKeyBoard(inputProjectName);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddProjectActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                                //  input_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                //  input_complation_date.setText(year+"-"+monthOfYear+"-"+dayOfMonth);

                                mCompletationDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                input_complation_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                //input_complation_date.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.show();
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -1);
//                Calendar now = Calendar.getInstance();
//
//                now.add(Calendar.DATE, -1);
              //  datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            }

        });

        imageView_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LL_add_btn_container.setVisibility(View.VISIBLE);
                LL_doc_contianer.setVisibility(GONE);
                fileExtension = "";
                imageView_add_project.setVisibility(View.VISIBLE);
                imageView_add_project.setImageDrawable(getDrawable(R.drawable.icon_add_doc));
            }
        });

        /*imageView_close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                RL_image_container.setVisibility(GONE);

            }
        });*/
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tempCheck();
                getUserIdToUpload();
// && validateDateDiff()
                if (validateProjectName() && validateCompletationDate() && validateProjectFile() && validateAllUsers()) {

                    System.out.println("You are ready to go");
                    System.out.println("FILE EXTENSION====" + fileExtension);
//                    if (fileExtension.equals("xlsx")) { //showConfirmationDialog();

                    //new UpdateProjectData().execute();
                    showConfirmationDialog();
                }
///*else{
//                    CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Invalid file format you have choosen");
//                        }


//                }


            }

        });


    }


    private void tempCheck() {
        System.out.println("RESULT OF SIZE=====" + InventoryAppSingleton.getInstance().getUsersResponseArrayList().size());
    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(AddProjectActivity.this);
    }

    private void showCustomDialog(String msg) {

        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText(msg);
        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
    }


    private void executeUploadAPI() {

    }


    private void permissionControl() {
        if (ActivityCompat.checkSelfPermission(AddProjectActivity.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(AddProjectActivity.this, permissionsRequired[0])) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProjectActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app need Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(AddProjectActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(permissionsRequired[0], false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProjectActivity.this);
                builder.setTitle("Permissions");
                builder.setMessage("Storage permission required.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(AddProjectActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }


            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            //  proceedAfterPermission();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {
            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                }
                else {


                    user_id = "";
                    s_user = "";
                    f_user = "";
                    System.out.println("Get Current Selected Item=======SIZE======" + InventoryAppSingleton.getInstance().getUsersResponseArrayList().size());

                    if (UserActivity.mSelectedUserArrayList.size() != 0) {
                        setListOfUser();
                        LL_list_container.setVisibility(View.VISIBLE);
                    } else {
                        LL_list_container.setVisibility(GONE);
                    }


                }

            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
                System.out.println("ON RESUME ACTIVATED");
                /*
                 * Flushing userId
                 * */
                user_id = "";
                s_user = "";
                f_user = "";
                System.out.println("Get Current Selected Item=======SIZE======" + InventoryAppSingleton.getInstance().getUsersResponseArrayList().size());

                if (UserActivity.mSelectedUserArrayList.size() != 0) {
                    setListOfUser();
                    LL_list_container.setVisibility(View.VISIBLE);
                } else {
                    LL_list_container.setVisibility(GONE);
                }

            }
        }
//trial

//                System.out.println("ON RESUME ACTIVATED");
//                /*
//                 * Flushing userId
//                 * */
//                user_id = "";
//                s_user = "";
//                f_user = "";
//                System.out.println("Get Current Selected Item=======SIZE======" + InventoryAppSingleton.getInstance().getUsersResponseArrayList().size());
//
//                if (UserActivity.mSelectedUserArrayList.size() != 0) {
//                    setListOfUser();
//                    LL_list_container.setVisibility(View.VISIBLE);
//                } else {
//                    LL_list_container.setVisibility(GONE);
//                }
//
//            }}
    }

    private void setListOfUser() {
        System.out.println("OUT PUT================SIZE===" + UserActivity.mSelectedUserArrayList.size());


        selectedUserAdapter = new SelectedUserAdapter(UserActivity.mSelectedUserArrayList, AddProjectActivity.this, this);
        recyclerview.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setAdapter(selectedUserAdapter);
       /* SlectedUsersAdapter slectedUsersAdapter=new SlectedUsersAdapter(InventoryAppSingleton.getInstance().getUsersResponseArrayList(), AddProjectActivity.this);
        listView.setAdapter(slectedUsersAdapter);*/
    }


    boolean contains(ArrayList<UsersResponse> list, String mUserID) {
        for (UsersResponse item : list) {
            if (item.getId().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(AddProjectActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                // proceedAfterPermission();
                //Toast.makeText(this, "Stoarage permission granted", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                /* proceedAfterPermission();*/
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(AddProjectActivity.this, permissionsRequired[0])
                    ) {
                //  txtPermissions.setText("Permissions Required");
                AlertDialog.Builder builder = new AlertDialog.Builder(AddProjectActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app needs Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(AddProjectActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void proceedAfterPermission() {
  /*Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        //intent.setType("**//*");
       /* Intent i = Intent.createChooser(intent, "View Default File Manager");
        startActivityForResult(i, CHOOSE_FILE_REQUESTCODE);*/
        // Toast.makeText(this, "Your Permission is granted", Toast.LENGTH_SHORT).show();

//        ===============================
        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "application/zip"};
//        String[] mimeTypes =
//                { // .doc & .docx
//                        // .ppt & .pptx
//                        "application/vnd.ms-excel"
//
//                       };
//
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(intent, CHOOSE_FILE_REQUESTCODE);
//        ======================================
//
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        intent.setType("application/msword/application/vnd.openxmlformats-officedocument.wordprocessingml.document/application/vnd.ms-powerpoint/application/vnd.openxmlformats-officedocument.presentationml.presentation/application/vnd.ms-excel/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet/text/plain/application/pdf/application/zip");
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//
//        try {
//            startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), CHOOSE_FILE_REQUESTCODE);
//        } catch (android.content.ActivityNotFoundException ex) {
//            // Potentially direct the user to the Market with a Dialog
//            Toast.makeText(this, "Please install a File Manager.",
//                    Toast.LENGTH_SHORT).show();
//        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {


            case CHOOSE_FILE_REQUESTCODE:


                if (data != null) {
                    Uri fileUri = data.getData();
                    projectFilePath = fileUri.getPath();
                    projectFilePath = projectFilePath.replace(" ", "_");
                    fileToUpload = new File(projectFilePath);
                    InputStream iStream = null;
                    try {
                        iStream = getContentResolver().openInputStream(fileUri);
                        try {
                            strByteArray = getBytes(iStream);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

//                    ==========================================
//                    projectFilePath = DocPath.getPath(AddProjectActivity.this, fileUri);
//                    fileToUpload = new File(projectFilePath);
//                    projectFilePath = fileToUpload.getPath();
//                    projectFileName = fileToUpload.getName();

//====================================
                    if (!projectFilePath.equals("")) {
                        projectFileName = fileUri.getLastPathSegment();
                        textFileName.setText(fileUri.getLastPathSegment());
                        Log.e("TAG", String.valueOf(fileToUpload));
//                        ========================
                        fileExtension = MyFileExtension.getFileExtension(fileToUpload);
//                        System.out.println("FILE EXTENSION====" + fileExtension);
//                        textFileName.setText(projectFileName.substring(0, projectFileName.lastIndexOf(".")));

                        textFileName.setVisibility(View.VISIBLE);
                        imageView_add_project.setImageDrawable(getResources().getDrawable(R.drawable.doc_image));
                        LL_doc_contianer.setVisibility(View.VISIBLE);
                        LL_add_btn_container.setVisibility(GONE);

                    }


                }

               /* //System.out.println("OUTPUT OF FILE PATH==="+data);
                //System.out.println("OUTPUT OF FILE PATH==="+fileToUpload.getPath());*/

                break;
            default:


        }

    }


    private void setCurrentDate() {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
    }




    /*Validation Section */


    private boolean validateProjectName() {

        if (inputProjectName.getText().toString().trim().equals("")) {
            // Toast.makeText(this, "Project name must not be empty", Toast.LENGTH_SHORT).show();

            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Project name must not be empty");
            // showCustomDialog("Project name must not be empty");
            return false;
        }

        return true;
    }

    private boolean validateCompletationDate() {
        if (input_complation_date.getText().toString().trim().equals("")) {
            //Toast.makeText(this, "Completation date must not be empty", Toast.LENGTH_SHORT).show();
            // showCustomDialog("Completation date must not be empty");

            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Completion date must not be empty");

            return false;
        }
        return true;
    }


    private boolean validateProjectFile() {
        if (LL_doc_contianer.getVisibility() == View.GONE) {
            projectFilePath = "";
        }
        if (projectFilePath.equals("")) {
            //Toast.makeText(this, "Please choose project file to upload", Toast.LENGTH_SHORT).show();

            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Please choose project file to upload");

            //  showCustomDialog("Please choose project file to upload");
            return false;
        }
        return true;
    }


    private boolean validateDateDiff() {
        long diff = 0;
        try {

            String completion_date_string = input_complation_date.getText().toString().trim();

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date_completation = formatter.parse(completion_date_string);
            Date date_current = new Date();
            String curent_date_string = formatter.format(date_current);
            date_current = formatter.parse(curent_date_string);

            diff = date_completation.getTime() - date_current.getTime();

            //   System.out.println ("Days: ========" + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

           /* System.out.println("OUT PUT==========Completation date=======" + formatter.format(date_completation));
            System.out.println("OUT PUT==========Current date=======" + formatter.format(date_current));*/

        } catch (Exception ex) {

        }


        if (diff >= 0) {
            return true;
        } else {
            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Completation date must set greater then current date");


            // showCustomDialog("Completation date must set greater then current date");
            return false;

        }
        //return true;
    }

    private boolean validateAllUsers() {

        if (f_user.equals("") || user_id.equals("") || s_user.equals("")) {
            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Please select atleast one  Bill Clerk, one SSE, one Officer In charge");

            // showCustomDialog("Please select atleast one Financial user");
            return false;
        }

        return true;

    }



    private boolean validateFinanceuser() {

        if (f_user.equals("")) {
            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Please select atleast one Financial user");

            // showCustomDialog("Please select atleast one Financial user");
            return false;
        }

        return true;

    }

    private boolean validatePhysicaluser() {
        if (user_id.equals("")) {
            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Please select atleast one Physical user");

            // showCustomDialog("Please select atleast one Physical user");
            return false;
        }

        return true;

    }

    private boolean validateSuperuser() {
        if (s_user.equals("")) {
            CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Please select atleast one Supervisor user");

            //showCustomDialog("Please select atleast one Supervisor user");
            return false;
        }
        return true;
    }


    private void getUserIdToUpload() {
        //ArrayList<UsersResponse> usersResponseArrayList = InventoryAppSingleton.getInstance().getUsersResponseArrayList();
        ArrayList<UsersResponse> usersResponseArrayList = UserActivity.mSelectedUserArrayList;

        s_user = "";
        f_user = "";
        user_id = "";

        if (usersResponseArrayList.size() != 0) {
            for (int i = 0; i < usersResponseArrayList.size(); i++) {
                if (usersResponseArrayList.get(i).getUsertype().equals("Supervisor")) {
                    if (s_user.equals("")) {
                        s_user = usersResponseArrayList.get(i).getId().trim();
                    } else {
                        s_user = s_user + "," + usersResponseArrayList.get(i).getId().trim();
                    }
                }

                if (usersResponseArrayList.get(i).getUsertype().equals("Financial")) {
                    if (f_user.equals("")) {
                        f_user = usersResponseArrayList.get(i).getId().trim();
                    } else {
                        f_user = f_user + "," + usersResponseArrayList.get(i).getId().trim();
                    }
                }

                if (usersResponseArrayList.get(i).getUsertype().equals("Physical")) {
                    if (user_id.equals("")) {
                        user_id = usersResponseArrayList.get(i).getId().trim();
                    } else {
                        user_id = user_id + "," + usersResponseArrayList.get(i).getId().trim();
                    }
                }


            }


        }


        System.out.println("SIZE OF CAREER USER DATA===" + usersResponseArrayList.size());
        System.out.println("SIZE OF CAREER S_USER===" + s_user);
        System.out.println("SIZE OF CAREER F_USER===" + f_user);
        System.out.println("SIZE OF CAREER P_USER===" + user_id);
    }

    @Override
    public void removeUser(UsersResponse usersResponse) {


//        System.out.println("NAME=========" + usersResponse.getName());
//
//
//        ArrayList<UsersResponse> usersResponseArrayList = InventoryAppSingleton.getInstance().getUsersResponseArrayList();
//
//        for (int i = 0; i < usersResponseArrayList.size(); i++) {
//
//            if (usersResponse.getId().equals(usersResponseArrayList.get(i).getId())) {
//                System.out.println("MATCH");
//                usersResponseArrayList.remove(i);
//            }
//        }
//
//
//        InventoryAppSingleton.getInstance().setUsersResponseArrayList(usersResponseArrayList);

//        selectedUserAdapter.updateList(usersResponseArrayList);
    }


    private void uploadExcel() {

    }


    /*
     * */

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure you want to add this project?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // new UpdateProjectData().execute();

                saveProfileAccount();

//                executeUploadAPI();

                alertDialog.dismiss();
                //new UpdateDetailsPhysicalActivity.UpdateProjectData().execute();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }

    public void hideKeyBoard(View v) {
        imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(AddProjectActivity.this.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        //InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());
        UserActivity.mSelectedUserArrayList.clear();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //    InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (CommonLoadingDialog.alertDialog != null)
            CommonLoadingDialog.alertDialog.dismiss();

    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 512;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    /*================================
    Execute Byte Array API
    =============================*/
    /*Upload Document*/
    private void saveProfileAccount() {
        // loading or check internet connection or something...
        // ... then
        CommonLoadingDialog.showLoadingDialog(AddProjectActivity.this, "Uploading....");
        final String[] stringData = {""};
        String url = Content.baseURL + "projects";
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);

                    if (jsonObject.getBoolean("status")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            stringData[0] = stringData[0] + jsonArray.get(i) + "\n";
                        }

                        // Toast.makeText(AddProjectActivity.this, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                        Toast.makeText(AddProjectActivity.this, "Project uploaded successfully", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddProjectActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        UserActivity.mSelectedUserArrayList.clear();
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                        CommonLoadingDialog.closeLoadingDialog();
                    } else {
                        CommonLoadingDialog.closeLoadingDialog();
                        CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Invalid file format you have choosen");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CommonLoadingDialog.closeLoadingDialog();
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    CommonLoadingDialog.closeLoadingDialog();
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Please provide file of appropriate format";
                        Toast.makeText(AddProjectActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                } else {

                    CommonLoadingDialog.closeLoadingDialog();
                    String result = new String(networkResponse.data);
                    CommonLoadingDialog.closeLoadingDialog();
                    CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Invalid file format you have choosen");

                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("project_name", inputProjectName.getText().toString());
                params.put("ending_date", mCompletationDate);
                params.put("ecd", input_extended_date.getText().toString());

                params.put("user_id", user_id.trim());
                params.put("f_user", f_user.trim());
                params.put("s_user", s_user.replace(" ", ""));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
//                params.put("file", new DataPart("file_avatar.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mAvatarImage.getDrawable()), "image/jpeg"));
                params.put("file", new VolleyMultipartRequest.DataPart("" + fileToUpload, strByteArray, "image/jpeg/pdf/txt/rtf"));//xlsx

                return params;
            }
        };

        MyApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    public class UpdateProjectData extends AsyncTask<String, String, String> {

        ProgressDialog pDialog;

        StringBuilder response = new StringBuilder();
        String stringData = "";

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            String charset = "UTF-8";
            String requestURL = Content.baseURL + "projects";

            System.out.println("COMPLETATIONDATE=====" + mCompletationDate);
            try {
                MultipartUtility multipart = new MultipartUtility(requestURL, charset);
                multipart.addFormField("project_name", inputProjectName.getText().toString());
                multipart.addFormField("ending_date", mCompletationDate);
                multipart.addFormField("ecd",input_extended_date.getText().toString().trim());
                multipart.addFormField("user_id", user_id.trim());
                multipart.addFormField("f_user", f_user.trim());
                multipart.addFormField("s_user", s_user.replace(" ", ""));
                if (!projectFilePath.contains("xlsx")) {
                    // multipart.addFilePart("file", new File(projectFilePath+".xlsx"));
                    multipart.addFilePart("file", null);
                }
                response = multipart.finish();
                System.out.println("RESPONSE MULTIPART===" + response);
            } catch (Exception ex) {
                System.out.println("OURPUT ERROR==" + ex);
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonLoadingDialog.showLoadingDialog(AddProjectActivity.this, "Uploading....");

        }


        @Override
        protected void onPostExecute(String jsonStr) {
            System.out.println("POST EXECUTION");
            try {
                JSONObject jsonObject = new JSONObject(response.toString());
                //<< get json string from server

                if (jsonObject.getBoolean("status")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        stringData = stringData + jsonArray.get(i) + "\n";
                    }

                    // Toast.makeText(AddProjectActivity.this, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                    Toast.makeText(AddProjectActivity.this, "Project uploaded successfully", Toast.LENGTH_SHORT).show();


                    startActivity(new Intent(AddProjectActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    finish();
                    CommonLoadingDialog.closeLoadingDialog();
                } else {

                    //   Toast.makeText(AddProjectActivity.this, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                    CommonLoadingDialog.closeLoadingDialog();
                    CommonValidationDialog.showLoadingDialog(AddProjectActivity.this, "Invalid file format you have choosen");

                }


                //* Toast.makeText(getApplicationContext(),stringData,Toast.LENGTH_SHORT);*//*
            } catch (Exception ex) {
                System.out.println("ERROR====" + ex.toString());
                CommonLoadingDialog.closeLoadingDialog();
                Toast.makeText(AddProjectActivity.this, "Project is not uploaded due to weak network", Toast.LENGTH_SHORT).show();

            }

        }
    }


//<html><body><h1>408 Request Time-out</h1>
//Your browser didn't send a complete request in time.
//</body></html>

}





