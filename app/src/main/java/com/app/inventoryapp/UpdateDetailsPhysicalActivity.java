package com.app.inventoryapp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.DocumentListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.ItemDataModel;
import com.app.inventoryapp.model.ItemDocModel;
import com.app.inventoryapp.model.ItemResponse;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.DocPath;
import com.app.inventoryapp.utils.MultipartUtility;
import com.app.inventoryapp.utils.MyFileExtension;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;

public class UpdateDetailsPhysicalActivity extends BaseActivity {
public  static  String Ujf="";
    public static final int USER_TAKE_CODE = 103;
    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int CHOOSE_FILE_REQUESTCODE = 102;
    ProjectItemModel projectItemModel = new ProjectItemModel();
    @BindView(R.id.imageView_add_doc)
    ImageView imageViewAddDoc;
    @BindView(R.id.imageView_add_doc2)
    ImageView imageView_add_doc2;
    @BindView(R.id.textFileName)
    TextView textFileName;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.input_quantity)
    EditText input_quantity;
    @BindView(R.id.textNoCertificate)
    TextView textNoCertificate;
    @BindView(R.id.btn_histry)
    Button btn_histry;
    @BindView(R.id.btn_info)
    Button btn_info;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.textSave)
    TextView textSave;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    @BindView(R.id.input_total_quantity)
    EditText input_total_quantity;
    @BindView(R.id.LL_upload_container)
    LinearLayout LL_upload_container;
    @BindView(R.id.RL_image_container)
    RelativeLayout RL_image_container;
    @BindView(R.id.imageView_close)
    ImageView imageView_close;
    @BindView(R.id.LL_add_button_container)
    LinearLayout LL_add_button_container;
    @BindView(R.id.LL_doc_contianer)
    LinearLayout LL_doc_contianer;
    String fileExtension;
    InventoryAppSession session;
    ItemResponse itemResponse = new ItemResponse();
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    File fileToUpload;
    String filePath = "";
    String fileName = "";
    @BindView(R.id.cbSupply)
    AppCompatCheckBox cbSupply;
    @BindView(R.id.cbInstallation)
    AppCompatCheckBox cbInstallation;
    @BindView(R.id.cbBoth)
    AppCompatCheckBox cbBoth;
    @BindView(R.id.inputReview)
    EditText inputReview;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private Double item_quantity;
    private Double item_quantity_to_update;
    private double remaining_quantity;
    private AlertDialog.Builder alertDialogBuilder;
public Integer cbPramater;
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details_physical);
        ButterKnife.bind(this);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        projectItemModel = (ProjectItemModel) getIntent().getExtras().getSerializable("ProjectItemModel");
//        input_quantity.setHint(projectItemModel.getTotal_qty_done());
cbSupply.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        cbInstallation.setChecked(false);
        cbBoth.setChecked(false);
    }
});
cbInstallation.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        cbSupply.setChecked(false);
        cbBoth.setChecked(false);
    }
});
cbBoth.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        cbSupply.setChecked(false);
        cbInstallation.setChecked(false);
    }
});
        if (projectItemModel != null)
            InventoryAppSingleton.getInstance().setProjectItemModel(projectItemModel);

        session = new InventoryAppSession(UpdateDetailsPhysicalActivity.this);

        intit();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        textSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateQuantity();
            }
        });

        imageViewAddDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permissionControl();
            }
        });

        imageView_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LL_doc_contianer.setVisibility(GONE);

                LL_add_button_container.setVisibility(View.VISIBLE);
                filePath = "";
                fileToUpload = null;

            }
        });
        if (InventoryAppSingleton.getInstance().getFromWhere().equals("UpdateDetailsPhysicalActivity")) {
            System.out.println("You are from UpdateDetailsPhysicalActivity();");
            InventoryAppSingleton.getInstance().setFromWhere("");

            ItemResponse itemResponse = (ItemResponse) getIntent().getExtras().getSerializable("itemResponse");
            ItemDataModel itemDataModel = itemResponse.getData();

            System.out.println("ITEM LEVEL PHYSICAL PREGRESS====================" + itemDataModel.getTotal_qty_done());
            System.out.println("ITEM LEVEL FINANCIAL PREGRESS====================" + itemDataModel.getTotal_payment_done());
            InventoryAppSingleton.getInstance().setPhysicalProgressItemLevel(itemDataModel.getTotal_qty_done());
            InventoryAppSingleton.getInstance().setFinancialProgressItemlevel(itemDataModel.getTotal_payment_done());
            settingStatus();

            initView(projectItemModel.getQty(), projectItemModel.getDone_work());

            ArrayList<ItemDocModel> itemDocModelArrayList = itemDataModel.getTblDoc();
            if (itemDataModel != null) {
                if (itemDocModelArrayList.size() > 0) {
                    ArrayList<ItemDocModel> docTempAL = new ArrayList<>();

                    for (int i = 0; i < itemDocModelArrayList.size(); i++) {
                        ItemDocModel mModel = itemDocModelArrayList.get(i);
                        if (mModel.getDoc_name().toString().length() > 0) {
                            docTempAL.add(mModel);
                        }
                    }

                    setDocList(docTempAL);
                } else {
                    textNoCertificate.setVisibility(View.VISIBLE);
                }
            }
        } else {
            System.out.println("=======You are not from UpdateDetailsPhysicalActivity();");
            settingStatus();
            httpGetItem();
        }


        input_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    item_quantity_to_update = Double.parseDouble(s.toString());


                    if (item_quantity_to_update > remaining_quantity) {
                        item_quantity_to_update = 0.0;
                        input_quantity.setText("");
                        input_quantity.setHint("" + remaining_quantity);
                    }

                } catch (Exception ex) {
                    System.out.println("ERROR=========" + ex);

                   item_quantity_to_update = 0.0;

                }
            }
        });


        btn_histry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(UpdateDetailsPhysicalActivity.this, "HIIII", Toast.LENGTH_SHORT).show();


                startActivity(new Intent(UpdateDetailsPhysicalActivity.this, HistoryActivity.class));

                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


            }
        });

        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle bundle = new Bundle();
                bundle.putSerializable("projectItemModel", projectItemModel);
                startActivity(new Intent(UpdateDetailsPhysicalActivity.this, ProjectInfoOther.class).putExtras(bundle));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

            }
        });

    }

    private void validateQuantity() {

        if (input_quantity.getText().toString().equals("")) {
            CommonValidationDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Please enter quantity");
            return;
        } else if (item_quantity_to_update == 0) {
            CommonValidationDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Quantity should not be 0");
            return;
        } else if (item_quantity_to_update > remaining_quantity) {
            CommonValidationDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Quantity should not be greater then " + remaining_quantity);
            return;
        }
    else if(!cbSupply.isChecked()&&!cbInstallation.isChecked()&&!cbBoth.isChecked()){
            CommonValidationDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Please select one of user updates option");

        }
        else if(inputReview.getText().toString().trim().equals("")){
            CommonValidationDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Please add review for the update to be done");
        }
        else if (!filePath.equals("")) {
            if (fileExtension.equals("pdf") || fileExtension.equals("jpg") || fileExtension.equals("png") || fileExtension.equals("jpeg")) {
                showConfirmationDialog();
            } else {
                CommonValidationDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Document must be in pdf, jpg, jpeg or png format");
                return;
            }
        } else {
            if(cbSupply.isChecked()){
                cbPramater=1;
            }
            if(cbInstallation.isChecked()){
                cbPramater=2;
            }
            if(cbBoth.isChecked()){
                cbPramater=3;
            }
            showConfirmationDialog();
        }
    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(UpdateDetailsPhysicalActivity.this);
        textSave.setVisibility(View.VISIBLE);
    }

    private void settingStatus() {
        /*Setting color in item level*/

        String itemLevelPhysicalProgress = InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel();
        String itemLevelFinancialProgress = InventoryAppSingleton.getInstance().getFinancialProgressItemlevel();

        System.out.println("ITEM LEVEL PROGRESS========PHYSICAL" + itemLevelPhysicalProgress);
        System.out.println("ITEM LEVEL PROGRESS========FINANCIAL" + itemLevelFinancialProgress);

        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            // System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }

    private void initView(String qua, String doneWork) {

        input_total_quantity.setText("" + qua);
        try {
            item_quantity = Double.parseDouble(qua) - Double.parseDouble(doneWork);
            item_quantity_to_update = Double.parseDouble(qua);

            remaining_quantity = item_quantity;

            input_quantity.setHint("" + remaining_quantity);


        } catch (Exception ex) {
            System.out.println("ERROR==============" + ex.toString());
        }
        // input_quantity.setText(""+projectItemModel.getQty());
    }

    private void httpGetItem() {


        try {

            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            String url = Content.baseURL + "iteams/" + InventoryAppSingleton.getInstance().getItem_id() + "?expand=tblDoc";

            System.out.println("OUTPUT URL====" + url);
            CommonLoadingDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Loading....");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();
                                    ItemResponse itemResponse = gson.fromJson(response, ItemResponse.class);
                                    ItemDataModel itemDataModel = itemResponse.getData();
                                    ArrayList<ItemDocModel> itemDocModelArrayList = itemDataModel.getTblDoc();
                                    initView(itemDataModel.getQty(), itemDataModel.getDone_work());
                                    System.out.println("ITEM QUANTITY==================" + itemDataModel.getQty());
                                    if (itemDataModel != null) {
                                        /*Do your stuff*/
                                        if (itemDocModelArrayList.size() > 0) {
                                            ArrayList<ItemDocModel> docTempAL = new ArrayList<>();
                                            for (int i = 0; i < itemDocModelArrayList.size(); i++) {
                                                ItemDocModel mModel = itemDocModelArrayList.get(i);
                                                if (mModel.getDoc_name().toString().length() > 0 && !mModel.getDoc_name().equals("NA")) {
                                                    docTempAL.add(mModel);
                                                }
                                            }
                                            setDocList(docTempAL);
                                        } else {
                                            textNoCertificate.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } else {
                                    Toast.makeText(UpdateDetailsPhysicalActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UpdateDetailsPhysicalActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }

    }

    private void setDocList(ArrayList<ItemDocModel> itemDocModelArrayList) {


        ArrayList<ItemDocModel> local_itemDocModelArrayList = new ArrayList<ItemDocModel>();
        for (int i = 0; i < itemDocModelArrayList.size(); i++) {
            if (itemDocModelArrayList.get(i).getPay_status().equals("1") || itemDocModelArrayList.get(i).getPay_status().equals("0") || itemDocModelArrayList.get(i).getPay_status().equals("3")) {
                local_itemDocModelArrayList.add(itemDocModelArrayList.get(i));
            }
        }


        itemDocModelArrayList = filterData(itemDocModelArrayList);
        //countTotalUpadtedItem(itemDocModelArrayList);
        DocumentListAdapter documentListAdapter = new DocumentListAdapter(UpdateDetailsPhysicalActivity.this, local_itemDocModelArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(documentListAdapter);

    }

    private ArrayList<ItemDocModel> filterData(List<ItemDocModel> itemDocModelArrayList) {

        ArrayList<ItemDocModel> m_itemDocModelArrayList = new ArrayList<>();

        for (int i = 0; i < itemDocModelArrayList.size(); i++) {
            if (!itemDocModelArrayList.get(i).getPay_status().equals("2")) {
                m_itemDocModelArrayList.add(itemDocModelArrayList.get(i));
            }
        }
        return m_itemDocModelArrayList;
    }
//
//    private void countTotalUpadtedItem(ArrayList<ItemDocModel> itemDocModelArrayList) {
//        double total_update_qunatity = 0;
//        for (int i = 0; i < itemDocModelArrayList.size(); i++) {
//            if (!itemDocModelArrayList.get(i).getPay_status().equals("2"))
//                //getQty_done()
//                total_update_qunatity = total_update_qunatity + Double.parseDouble(itemDocModelArrayList.get(i).getQty_done().trim());
//        }
//        remaining_quantity = item_quantity - total_update_qunatity;
//        item_quantity_to_update = remaining_quantity;
//
//        /*System.out.println("REMAINNIONG QUANTITY======================"+remaining_quantity);
//        System.out.println("REMAINNIONG QUANTITY======================"+total_update_qunatity);*/
//
//
//        //  item_quantity=remaining_quantity;
//
//
//        //Log.d("OBJECT=====",)
//
//        if (item_quantity_to_update == 0.0) {
//            input_quantity.setEnabled(false);
//            textSave.setVisibility(GONE);
//            LL_upload_container.setVisibility(GONE);
//        }
//        //input_quantity.setText(""+item_quantity_to_update);
//       input_quantity.setHint("" + item_quantity_to_update);
////trail
//    }

    private void permissionControl() {
        if (ActivityCompat.checkSelfPermission(UpdateDetailsPhysicalActivity.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(UpdateDetailsPhysicalActivity.this, permissionsRequired[0])) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDetailsPhysicalActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app need Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(UpdateDetailsPhysicalActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(permissionsRequired[0], false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDetailsPhysicalActivity.this);
                builder.setTitle("Permissions");
                builder.setMessage("This Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(UpdateDetailsPhysicalActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }


            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            proceedAfterPermission();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(UpdateDetailsPhysicalActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                proceedAfterPermission();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(UpdateDetailsPhysicalActivity.this, permissionsRequired[0])
                    ) {
                //  txtPermissions.setText("Permissions Required");
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDetailsPhysicalActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app needs Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(UpdateDetailsPhysicalActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {


            case CHOOSE_FILE_REQUESTCODE:


                if (data != null) {

                    try {


                        if (!DocPath.getPath(getApplicationContext(), data.getData()).equals("")) {
                            fileToUpload = new File(DocPath.getPath(getApplicationContext(), data.getData()));

                            filePath = fileToUpload.getAbsolutePath();
                            fileName = fileToUpload.getName();
                            LL_doc_contianer.setVisibility(View.VISIBLE);
                            // imageViewAddDoc.setBackground(getResources().getDrawable(R.drawable.doc_image));
                            textFileName.setText(fileName.substring(0, fileName.lastIndexOf(".")));
                            textFileName.setVisibility(View.VISIBLE);
                            LL_add_button_container.setVisibility(GONE);

                            // fileExtension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(filePath);
                            fileExtension = MyFileExtension.getFileExtension(fileToUpload);
                            System.out.println();

                            if (fileExtension.equals("png") || fileExtension.equals("PNG") || fileExtension.equals("jpeg") || fileExtension.equals("jpg")) {

                                String filePathe = compressImage(fileToUpload.getAbsolutePath());
                                fileToUpload = new File(filePathe);


                            }


                            System.out.println("TEST========" + data.getData());

                            // System.out.println("path====="+filePath);
                        }
                    } catch (Exception ex) {
                        System.out.println("ERROR========" + ex);
                    }
                }


                break;
            default:


        }

    }

    private void proceedAfterPermission() {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(intent, CHOOSE_FILE_REQUESTCODE);


    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showConfirmationDialog();
                alertDialog.dismiss();
                new UpdateProjectData().execute();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }


    public String compressImage(String imagePath) {
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        if (bmp != null) {
            bmp.recycle();
        }

        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filepath = getFilename();
        try {
            out = new FileOutputStream(filepath);

            //write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filepath;
    }

    public String getFilename() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files/Compressed");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        String mImageName = "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        String uriString = (mediaStorageDir.getAbsolutePath() + "/" + mImageName);
        ;
        return uriString;

    }

    public String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public class UpdateProjectData extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        StringBuilder response = new StringBuilder();
        String stringData = "";

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            String charset = "UTF-8";
            String requestURL = Content.baseURL + "docs?expand=tblDoc";


            try {

                MultipartUtility multipart = new MultipartUtility(requestURL, charset);

                if (fileToUpload != null)
                    multipart.addFilePart("doc_name", fileToUpload);
                multipart.addFormField("user_id", session.getKeyUserId().trim());
                multipart.addFormField("project_id", InventoryAppSingleton.getInstance().getProject_id().trim());
                multipart.addFormField("iteam_id", InventoryAppSingleton.getInstance().getItem_id().trim());
                multipart.addFormField("qty_done", "" + item_quantity_to_update);
                multipart.addFormField("work_type", String.valueOf(cbPramater));
                multipart.addFormField("work_remark", String.valueOf(inputReview.getText().toString().trim()));
                response = multipart.finish();
                System.out.println("RESPONSE MULTIPART===" + response);

            } catch (Exception ex) {
                CommonLoadingDialog.closeLoadingDialog();
                System.out.println("ERROR=======" + ex);
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            System.out.println("====================");
//            System.out.println("====================DoC path===" + fileToUpload.toString());
//            System.out.println("====================userId===" + session.getKeyUserId().trim());
//            System.out.println("====================project_id==" + InventoryAppSingleton.getInstance().getProject_id().trim());
//            System.out.println("====================iteam_id" + InventoryAppSingleton.getInstance().getItem_id().trim());
//            System.out.println("====================iteam_Quantity" + item_quantity_to_update);

            CommonLoadingDialog.showLoadingDialog(UpdateDetailsPhysicalActivity.this, "Uploading....");

        }


        @Override
        protected void onPostExecute(String jsonStr) {
            System.out.println("POST EXECUTION");
            try {
                CommonLoadingDialog.closeLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());

                    System.out.println("RESPONSE MULTIPART======" + jsonObject);

                    if (jsonObject.getBoolean("status")) {

                        try {

                            Toast.makeText(UpdateDetailsPhysicalActivity.this, "Updated successfully", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(UpdateDetailsPhysicalActivity.this, HistoryActivity.class));

                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();

                        } catch (Exception ex) {
                            System.out.println("ERROR=================" + ex);
                        }


                    }

                } catch (Exception ex) {
                    System.out.println("ERROR====" + ex.toString());
                }


                //finish();
            } catch (Exception ex) {
                System.out.println("ERROR====" + ex.toString());
            }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {
            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                    startActivity(new Intent(UpdateDetailsPhysicalActivity.this, SignInActivity.class));

                }

            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }
        }
    }

}
