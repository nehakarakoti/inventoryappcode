package com.app.inventoryapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ProjectParticularUsersListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.AddUserToProjectInterface;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddUserProjectActivity extends AppCompatActivity implements AddUserToProjectInterface {
    public static List<UsersResponse> finalList = new ArrayList<>();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.buttonSubmit)
    Button buttonSubmit;


    AlertDialog.Builder alertDialogBuilder;
    private ArrayList<UsersResponse> usersResponseArrayList = new ArrayList<>();
    private ArrayList<UsersResponse> usersResponseArrayListFilter = new ArrayList<>();
    private ArrayList<UserDataModel> userDataModelArrayList = new ArrayList<>();
InventoryAppSession inventoryAppSession;

    private String user_id = "";
    private String s_user = "";
    private String f_user = "";

    private ArrayList<UsersResponse> userDataModelArrayList2 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_project);
        ButterKnife.bind(this);
        inventoryAppSession=new InventoryAppSession(AddUserProjectActivity.this);
        userDataModelArrayList = (ArrayList<UserDataModel>) getIntent().getExtras().getSerializable("userDataModelArrayList");
        System.out.println("SIZE OF PROJECT USERS====" + userDataModelArrayList.size());
        intit();
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (userDataModelArrayList2.size() != 0) {
                    user_id = "";
                    s_user = "";
                    f_user = "";
                    for (int i = 0; i < userDataModelArrayList2.size(); i++) {
                        if (userDataModelArrayList2.get(i).getUsertype().equals("Supervisor")) {
                            if (s_user.equals("")) {
                                s_user = userDataModelArrayList2.get(i).getId().trim();
                            } else {
                                s_user = s_user + "," + userDataModelArrayList2.get(i).getId().trim();
                            }
                        }
                        if (userDataModelArrayList2.get(i).getUsertype().equals("Financial")) {
                            if (f_user.equals("")) {
                                f_user = userDataModelArrayList2.get(i).getId().trim();
                            } else {
                                f_user = f_user + "," + userDataModelArrayList2.get(i).getId().trim();
                            }
                        }
                        if (userDataModelArrayList2.get(i).getUsertype().equals("Physical")) {
                            if (user_id.equals("")) {
                                user_id = userDataModelArrayList2.get(i).getId().trim();
                            } else {
                                user_id = user_id + "," + userDataModelArrayList2.get(i).getId().trim();
                            }
                        }
                    }
                    System.out.println("SIZE OF CAREER USER DATA===" + usersResponseArrayList.size());
                    System.out.println("SIZE OF CAREER S_USER===" + s_user);
                    System.out.println("SIZE OF CAREER F_USER===" + f_user);
                    System.out.println("SIZE OF CAREER P_USER===" + user_id);
                    showConfirmationDialog();
                } else {
                    CommonValidationDialog.showLoadingDialog(AddUserProjectActivity.this, "Please select user to add in project");
                    System.out.println("SIZE OF CAREER USER DATA===" + usersResponseArrayList.size());
                    System.out.println("SIZE OF CAREER S_USER===" + s_user);
                    System.out.println("SIZE OF CAREER F_USER===" + f_user);
                    System.out.println("SIZE OF CAREER P_USER===" + user_id);
                }

            }
        });

        httpGetUsersCall();
    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(AddUserProjectActivity.this);
    }


    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to add user?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                httpAddUSer();
                alertDialog.dismiss();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }


    private void httpAddUSer() {
        try {
            final String url = Content.baseURL + "projects/" + InventoryAppSingleton.getInstance().getProject_id();

            CommonLoadingDialog.showLoadingDialog(AddUserProjectActivity.this, "Creating...");


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            System.out.println("RESPONSE==========" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    startActivity(new Intent(AddUserProjectActivity.this, ProjectUsersActivity.class));
                                    finish();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                       /* System.out.println("OUT PUT==START DATE===="+InventoryAppSingleton.getInstance().getStarting_date());
                        System.out.println("OUT PUT ENDING DATE===="+InventoryAppSingleton.getInstance().getEnding_date());
                        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");*/
                    String startingDate[] = InventoryAppSingleton.getInstance().getStarting_date().split("-");
                    String endingDate[] = InventoryAppSingleton.getInstance().getEnding_date().split("-");


                    params.put("starting_date", startingDate[2] + "-" + startingDate[1] + "-" + startingDate[0]);
                    params.put("ending_date", endingDate[2] + "-" + endingDate[1] + "-" + endingDate[0]);

                    if (!s_user.equals("")) {
                        params.put("s_user", InventoryAppSingleton.getInstance().getS_user() + "," + s_user);
                    } else {
                        params.put("s_user", InventoryAppSingleton.getInstance().getS_user());
                    }

                    if (!f_user.equals("")) {
                        // params.put("s_user",InventoryAppSingleton.getInstance().getS_user()+","+s_user);
                        params.put("f_user", InventoryAppSingleton.getInstance().getF_user() + "," + f_user);
                    } else {
                        params.put("f_user", InventoryAppSingleton.getInstance().getF_user());
                    }
                    if (!user_id.equals("")) {
                        params.put("user_id", InventoryAppSingleton.getInstance().getUser_id() + "," + user_id);
                    } else {
                        params.put("user_id", InventoryAppSingleton.getInstance().getUser_id());
                    }
                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AddUserProjectActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("ERROR====" + ex);
        }

    }

    private void httpGetUsersCall() {
        try {
            final String url = Content.baseURL + "users?sort=-id";
            //  final String url = "http://dharmani.com/railway_api/web/users?sort=-id";


            CommonLoadingDialog.showLoadingDialog(AddUserProjectActivity.this, "Loading...");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                System.out.println("SIZE====" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                    UsersResponse usersResponse = new UsersResponse();

                                    usersResponse.setId(jsonObject1.getString("id"));
                                    usersResponse.setUsername(jsonObject1.getString("username"));
                                    usersResponse.setUsertype(jsonObject1.getString("usertype"));
                                    usersResponse.setName(jsonObject1.getString("name"));
                                    usersResponseArrayList.add(usersResponse);

                                }
                                filterUsers();
                                System.out.println("STRING RESPONSE====" + usersResponseArrayList.size());

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AddUserProjectActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }


    }

    private void filterUsers() {
        for (int i = 0; i < usersResponseArrayList.size(); i++) {

            boolean isExist = false;

            for (int j = 0; j < userDataModelArrayList.size(); j++) {
                if (usersResponseArrayList.get(i).getId().equals(userDataModelArrayList.get(j).getId())) {
                    isExist = true;
                }
            }
            if (!isExist) {
                usersResponseArrayListFilter.add(usersResponseArrayList.get(i));
            }
        }

        System.out.println("SIZE AFTER FILTER===" + usersResponseArrayListFilter.size());

        setList();
    }

    private void setList() {
        ProjectParticularUsersListAdapter adapter = new ProjectParticularUsersListAdapter(AddUserProjectActivity.this, usersResponseArrayListFilter, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void selectUserToProject(ArrayList<UsersResponse> userDataModelArrayList) {
        userDataModelArrayList2 = userDataModelArrayList;
        System.out.println("SIZE====" + userDataModelArrayList.size());
    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(AddUserProjectActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);


            }
        }
    }
    public int difftime(String string, String string2) {
        int hours;
        int min = 0;
        int days;
        long difference;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date1 = simpleDateFormat.parse(string2);
            Date date2 = simpleDateFormat.parse(string);

            difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours))
                    / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= Hours", " :: " + hours);
            return min;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return min;
    }
    public void loggingOut(Activity activity1){
        startActivity(new Intent(activity1,SignInActivity.class));
    }
}
