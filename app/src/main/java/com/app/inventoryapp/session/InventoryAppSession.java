package com.app.inventoryapp.session;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by RITUPARNA on 5/3/2018.
 */

public class InventoryAppSession {
    public static final String KEY_CURRENTTIMESTAMP = "time_stamp";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USER_TYPE = "user_type";
    public static final String KEY_USER_NAME = "user_name";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_CELL_NUMBER = "user_cellno";
    public static final String KEY_DEVICE_TOKEN = "device_token";
    public static final boolean IS_LOGIN = false;
    private static final String PREF_NAME = "Inventoty_pref";
    SharedPreferences inventoryPreference;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;


    public InventoryAppSession(Context context) {
        this.context = context;
        inventoryPreference = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = inventoryPreference.edit();
    }

    public boolean isIsLogin() {
        return inventoryPreference.getBoolean("IS_LOGIN", false);
    }

    public void setISLogin(boolean isLogin) {
        editor.putBoolean("IS_LOGIN", isLogin);
        editor.commit();
    }
    public String getTimeStamp(){
        return inventoryPreference.getString(KEY_CURRENTTIMESTAMP,"");
    }
    public String getDeviceToken() {
        return inventoryPreference.getString(KEY_DEVICE_TOKEN, "");
    }

    public void setDeviceToken(String device_token) {
        editor.putString(KEY_DEVICE_TOKEN, device_token);
        editor.commit();
    }

    public String getKeyUserId() {
        return inventoryPreference.getString(KEY_USER_ID, "");
    }

    public void setKeyUserId(String user_id) {
        editor.putString(KEY_USER_ID, user_id);
        editor.commit();
    }
    public void setCurrentTimeStamp(String userCellNumber) {
        editor.putString(KEY_CURRENTTIMESTAMP, userCellNumber);
        editor.commit();
    }
    public String getKeyUserType() {
        return inventoryPreference.getString(KEY_USER_TYPE, "");
    }

    public void setKeyUserType(String userType) {
        editor.putString(KEY_USER_TYPE, userType);
        editor.commit();
    }

    public String getKeyUserName() {
        return inventoryPreference.getString(KEY_USER_NAME, "");
    }

    public void setKeyUserName(String userName) {
        editor.putString(KEY_USER_NAME, userName);
        editor.commit();
    }



    public String getKeyUserEmail() {
        return inventoryPreference.getString(KEY_USER_EMAIL, "");
    }

    public void setKeyUserEmail(String userEmail) {
        editor.putString(KEY_USER_EMAIL, userEmail);
        editor.commit();
    }

    public String getKeyUserCellNumber() {
        return inventoryPreference.getString(KEY_USER_CELL_NUMBER, "");
    }

    public void setKeyUserCellNumber(String userCellNumber) {
        editor.putString(KEY_USER_CELL_NUMBER, userCellNumber);
        editor.commit();
    }


    public void user_logout() {
        editor.clear();
        editor.commit();
    }


}
