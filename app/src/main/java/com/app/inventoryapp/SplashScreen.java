package com.app.inventoryapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.session.InventoryAppSession;

import org.json.JSONObject;


public class SplashScreen extends AppCompatActivity {

    String strData="";
    InventoryAppSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getData();


    }

    private void getData()
    {
        if (getIntent().getExtras() != null)
        {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                if (key.equalsIgnoreCase("data"))
                {
                    strData = String.valueOf(value);
                }
            }
            reDirectionsNotifications(strData);
        }




    }


    private void reDirectionsNotifications(String strDataMM) {
        try {
            JSONObject jsonObject = new JSONObject(strDataMM);

            String project_id = jsonObject.getString("project_id");
            String type = jsonObject.getString("type");
            String item = "";
            if (!jsonObject.isNull("item")){
                item  = jsonObject.getString("item");
                InventoryAppSingleton.getInstance().setItem_id(item);
            }



            System.out.println("OUT PROJECT==" + type + "====" + item);

            if (type.equals("add_project")) {

               if(session.getKeyUserType().equals("Admin"))
               {

                   startActivity(new Intent(SplashScreen.this, HomeActivity.class));
                   finish();
               }
               else
               {
                   startActivity(new Intent(SplashScreen.this, HomeActivityOther.class));
                   finish();
               }

            }
            else {
                InventoryAppSingleton.getInstance().setItem_id(item);
                startActivity(new Intent(SplashScreen.this, HistoryActivity.class));
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run()
            {
                startActivity(new Intent(SplashScreen.this,SignInActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        }, 3000);
    }
}
