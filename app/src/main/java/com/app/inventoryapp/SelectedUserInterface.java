package com.app.inventoryapp;

import com.app.inventoryapp.model.UsersResponse;

import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/9/2018.
 */

public interface SelectedUserInterface {
    public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList);
    public void changeBadgeICon(boolean t);
}
