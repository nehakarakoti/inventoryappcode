package com.app.inventoryapp.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.app.inventoryapp.BaseActivity;
import com.app.inventoryapp.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;




public class MyFirebaseMessagingService extends FirebaseMessagingService
{

    String TAG = MyFirebaseMessagingService.this.getClass().getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {

        BaseActivity.notification_alert=true;
        sendBroadcast(new Intent("IconChange"));

    }


}
