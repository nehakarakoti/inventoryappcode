package com.app.inventoryapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.inventoryapp.model.ItemDocModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DocumentViewActivity extends BaseActivity {
    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.LL_webView_container)
    LinearLayout LLWebViewContainer;


    @BindView(R.id.progressLoading)
    ProgressBar progressLoading;

    @BindView(R.id.title)
    TextView title;

    ItemDocModel itemDocModel;

    String doc_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_view);
        ButterKnife.bind(this);

        if (getIntent().getExtras().get("doc_url") != null && !getIntent().getExtras().get("doc_url").equals("NA") && getIntent().getExtras().get("doc_url").toString().length() > 3) {
            doc_url = (String) getIntent().getExtras().get("doc_url");
            title.setText(doc_url.split("/")[1].split("_")[1].split("\\.")[0]);
            setDocument();
        }else{
            Toast.makeText(DocumentViewActivity.this,"No Preview Document!",Toast.LENGTH_SHORT).show();
        }

        setClick();
    }

    private void setClick() {
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void setDocument() {
        String fileExtension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(doc_url);

        System.out.println("FILE EXTENSION====" + fileExtension);
        System.out.println("FILE EXTENSION====" + doc_url);


        if (fileExtension.equals("pdf")) {
            String htmlTag = "<iframe src='http://docs.google.com/gview?embedded=true&url=" + "http://dharmani.com/railway_api/web/" + doc_url + "'width='100%' height='100%'></iframe>";

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setWebViewClient(new MyWebViewClient());

            webView.getSettings().setUseWideViewPort(true);

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            /*webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setJavaScriptEnabled(true);*/

            // System.out.println("DOC"+"http://dharmani.com/railway_api/web/"+itemDocModel.getDoc_name());
            webView.loadData(htmlTag, "text/html", "UTF-16");
        } else {
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new MyWebViewClient());

            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);

            webView.loadUrl("http://dharmani.com/railway_api/web/" + doc_url);
        }


    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressLoading.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressLoading.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(DocumentViewActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }


    }
}
