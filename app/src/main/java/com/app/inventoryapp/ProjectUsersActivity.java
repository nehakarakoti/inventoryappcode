package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ProjectUsersListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.model.UserResponseModel;
import com.app.inventoryapp.my_interface.UserRemoveProjectListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectUsersActivity extends AppCompatActivity implements UserRemoveProjectListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.view)
    View view;
    InventoryAppSession inventoryAppSession;
    @BindView(R.id.imageView_add_user)
    ImageView imageView_add_user;
    @BindView(R.id.LL_back_btn_container)
    LinearLayout LL_back_btn_container;
    String up_physical_user = "";
    String up_super_user = "";
    String up_fin_user = "";
    AlertDialog.Builder alertDialogBuilder;
    private ArrayList<UserDataModel> userDataModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_users);

        ButterKnife.bind(this);
//    inventoryAppSession=new InventoryAppSession(ProjectUsersActivity.this);
        LL_back_btn_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        imageView_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("userDataModelArrayList", userDataModelArrayList);
//                startActivity(new Intent(ProjectUsersActivity.this, AddUserProjectActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));
                startActivity(new Intent(ProjectUsersActivity.this, EditProjectUserActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        });
        httpGetProjectsUser();
        setStatus();
        intit();

    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(ProjectUsersActivity.this);
    }

    private void setStatus() {
        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }


    private void httpGetProjectsUser() {
        try {

            String url = Content.baseURL + "users/history?project_id=" + InventoryAppSingleton.getInstance().getProject_id();
            CommonLoadingDialog.showLoadingDialog(ProjectUsersActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("RESPONSE=======" + response);
                            Gson gson = new Gson();

                            UserResponseModel userResponseModel = gson.fromJson(response, UserResponseModel.class);
                            userDataModelArrayList = userResponseModel.getData();
                            setUsersList();

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectUsersActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    public void setUsersList() {

        setUsers();

        ProjectUsersListAdapter projectUsersListAdapter = new ProjectUsersListAdapter(userDataModelArrayList, ProjectUsersActivity.this, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(projectUsersListAdapter);

    }

    private void setUsers() {

        String s_user = "";
        String f_user = "";
        String user_id = "";

        if (userDataModelArrayList.size() != 0) {
            for (int i = 0; i < userDataModelArrayList.size(); i++) {
                if (userDataModelArrayList.get(i).getUsertype().equals("Supervisor")) {
                    if (s_user.equals("")) {
                        s_user = userDataModelArrayList.get(i).getId().trim();
                    } else {
                        s_user = s_user + "," + userDataModelArrayList.get(i).getId().trim();
                    }
                }

                if (userDataModelArrayList.get(i).getUsertype().equals("Financial")) {
                    if (f_user.equals("")) {
                        f_user = userDataModelArrayList.get(i).getId().trim();
                    } else {
                        f_user = f_user + "," + userDataModelArrayList.get(i).getId().trim();
                    }
                }

                if (userDataModelArrayList.get(i).getUsertype().equals("Physical")) {
                    if (user_id.equals("")) {
                        user_id = userDataModelArrayList.get(i).getId().trim();
                    } else {
                        user_id = user_id + "," + userDataModelArrayList.get(i).getId().trim();
                    }
                }


            }


            InventoryAppSingleton.getInstance().setUser_id(user_id);
            InventoryAppSingleton.getInstance().setS_user(s_user);
            InventoryAppSingleton.getInstance().setF_user(f_user);


        }


       /* System.out.println("SIZE OF CAREER USER DATA===" + userDataModelArrayList.size());
        System.out.println("SIZE OF CAREER S_USER===" + s_user);
        System.out.println("SIZE OF CAREER F_USER===" + f_user);
        System.out.println("SIZE OF CAREER P_USER===" + user_id);*/
    }


    @Override
    public void removeUserFromProject(UserDataModel userDataModel) {

//        for(int i=0;i<userDataModelArrayList.size();i++){
//            if(userDataModelArrayList.get(0).getId()==userDataModel.getId()){
//
//            }
//        }
        String user_id = userDataModel.getId();
        String userType = userDataModel.getUsertype();

        String physical_user = InventoryAppSingleton.getInstance().getUser_id();
        String super_user = InventoryAppSingleton.getInstance().getS_user();
        String fin_user = InventoryAppSingleton.getInstance().getF_user();


        up_physical_user = InventoryAppSingleton.getInstance().getUser_id();
        up_super_user = InventoryAppSingleton.getInstance().getS_user();
        up_fin_user = InventoryAppSingleton.getInstance().getF_user();


        if (userType.equals("Financial")) {
            if (!fin_user.contains(",")) {
                CommonValidationDialog.showLoadingDialog(ProjectUsersActivity.this, "You must have atleast one Bill Clerk");
            } else {
                String[] financial_user_array = fin_user.split(",");
                up_fin_user = "";
                for (int i = 0; i < financial_user_array.length; i++) {
                    if (financial_user_array[i].equals(user_id)) {
                        System.out.println("MATCH FOUND===");
                    } else {
                        if (up_fin_user.equals("")) {
                            up_fin_user = financial_user_array[i];
                        } else {
                            up_fin_user = up_fin_user + "," + financial_user_array[i];
                        }
                    }
                }
                showConfirmationDialog();
            }


        }
        if (userType.equals("Physical")) {

            if (!physical_user.contains(",")) {
                CommonValidationDialog.showLoadingDialog(ProjectUsersActivity.this, "You must have atleast one SSE");

            } else {
                String[] physical_user_array = physical_user.split(",");
                up_physical_user = "";
                for (int i = 0; i < physical_user_array.length; i++) {
                    if (physical_user_array[i].equals(user_id)) {
                        System.out.println("MATCH FOUND===");
                    } else {
                        if (up_physical_user.equals("")) {
                            up_physical_user = physical_user_array[i];
                        } else {
                            up_physical_user = up_physical_user + "," + physical_user_array[i];
                        }
                    }
                }
                showConfirmationDialog();
            }
        }
        if (userType.equals("Supervisor")) {
            if (!super_user.contains(",")) {
                CommonValidationDialog.showLoadingDialog(ProjectUsersActivity.this, "You must have atleast one Officer In charge");
            } else {
                String[] super_user_array = super_user.split(",");
                up_super_user = "";

                for (int i = 0; i < super_user_array.length; i++) {
                    if (super_user_array[i].equals(user_id)) {
                        System.out.println("MATCH FOUND===");
                    } else {
                        if (up_super_user.equals("")) {
                            up_super_user = super_user_array[i];
                        } else {
                            up_super_user = up_super_user + "," + super_user_array[i];
                        }
                    }
                }
                showConfirmationDialog();
            }

        }


        System.out.println("RITUPARNA KAHDKA=======PHY USER==" + physical_user);
        System.out.println("RITUPARNA KAHDKA=======SUPER USER==" + super_user);
        System.out.println("RITUPARNA KAHDKA=======FINANCE USER==" + fin_user);

        System.out.println("RITUPARNA KAHDKA=======PHY USER==" + up_physical_user);
        System.out.println("RITUPARNA KAHDKA=======SUPER USER==" + up_super_user);
        System.out.println("RITUPARNA KAHDKA=======FINANCE USER==" + up_fin_user);
    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to remove?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                httpRemoveUser();
                //new UpdateDetailsPhysicalActivity.UpdateProjectData().execute();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }


    private void httpRemoveUser() {

        // String url = Content.baseURL + "projects/"+InventoryAppSingleton.getInstance().getProject_id();

        try {
            final String url = Content.baseURL + "projects/" + InventoryAppSingleton.getInstance().getProject_id();

            CommonLoadingDialog.showLoadingDialog(ProjectUsersActivity.this, "Creating...");


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            System.out.println("RESPONSE==========" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    InventoryAppSingleton.getInstance().setS_user(up_super_user);
                                    InventoryAppSingleton.getInstance().setF_user(up_fin_user);
                                    InventoryAppSingleton.getInstance().setUser_id(up_physical_user);
                                    startActivity(new Intent(ProjectUsersActivity.this, ProjectUsersActivity.class));
                                    finish();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    String startingDate[] = InventoryAppSingleton.getInstance().getStarting_date().split("-");
                    String endingDate[] = InventoryAppSingleton.getInstance().getEnding_date().split("-");


                    params.put("starting_date", startingDate[2] + "-" + startingDate[1] + "-" + startingDate[0]);
                    params.put("ending_date", endingDate[2] + "-" + endingDate[1] + "-" + endingDate[0]);
                    params.put("user_id", up_physical_user);
                    params.put("f_user", up_fin_user);
                    params.put("s_user", up_super_user);

                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectUsersActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendBroadcast(new Intent("comingBack"));
    }
    protected void onResume() {
        super.onResume();
//        if(inventoryAppSession.getTimeStamp()!=null){
//
//            if(!inventoryAppSession.getTimeStamp().equals("")) {
//                Calendar cal = Calendar.getInstance();
//                Date currentLocalTime = cal.getTime();
//                DateFormat date2 = new SimpleDateFormat("HH:mm a");
//                String localTime = date2.format(currentLocalTime);
//                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
//                    inventoryAppSession.user_logout();
//                    startActivity(new Intent(ProjectUsersActivity.this,SignInActivity.class));
//                }
//                else {
//                    Calendar cal1 = Calendar.getInstance();
//                    Date currentLocalTime1 = cal1.getTime();
//                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
//
//                    String localTime1 = date21.format(currentLocalTime1);
//                    inventoryAppSession.setCurrentTimeStamp(localTime1);
//                }
//            }
//            else {
//                Calendar cal = Calendar.getInstance();
//                Date currentLocalTime = cal.getTime();
//                DateFormat date2 = new SimpleDateFormat("HH:mm a");
//
//                String localTime = date2.format(currentLocalTime);
//                inventoryAppSession.setCurrentTimeStamp(localTime);
//            }
//
//
//        }

    }
    public int difftime(String string, String string2) {
        int hours;
        int min = 0;
        int days;
        long difference;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date1 = simpleDateFormat.parse(string2);
            Date date2 = simpleDateFormat.parse(string);

            difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours))
                    / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= Hours", " :: " + hours);
            return min;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return min;
    }
}
