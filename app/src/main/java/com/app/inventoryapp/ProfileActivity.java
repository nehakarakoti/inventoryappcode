package com.app.inventoryapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.font.CustomTextView;
import com.app.inventoryapp.model.AddUserErrorResponse;
import com.app.inventoryapp.model.ErrorData;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.Utilities;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity {
    Activity mActivity = ProfileActivity.this;
    String TAG = ProfileActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgLogoutIV)
    ImageView imgLogoutIV;
    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.txtEmailValueTV)
    CustomTextView txtEmailValueTV;
    @BindView(R.id.txtNameValueTV)
    CustomTextView txtNameValueTV;
    @BindView(R.id.txtCellNoValueTV)
    CustomTextView txtCellNoValueTV;
    @BindView(R.id.txtRoleValueTV)
    CustomTextView txtRoleValueTV;
    @BindView(R.id.layoutCellNumberLL)
    LinearLayout layoutCellNumberLL;
    @BindView(R.id.changPwdLL)
    LinearLayout changPwdLL;
    AlertDialog.Builder alertDialogBuilder;
    String strUserID;
    private InventoryAppSession inventoryAppSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        inventoryAppSession = new InventoryAppSession(mActivity);
        strUserID = inventoryAppSession.getKeyUserId();
        Log.e(TAG, "===UserID===" + strUserID);
        alertDialogBuilder = new AlertDialog.Builder(mActivity);

        setProfileData();
    }

    private void setProfileData() {
        txtEmailValueTV.setText(inventoryAppSession.getKeyUserEmail());
        txtNameValueTV.setText(inventoryAppSession.getKeyUserName());
        txtCellNoValueTV.setText(inventoryAppSession.getKeyUserCellNumber());
        txtRoleValueTV.setText(Utilities.getUpdatedRoleText(inventoryAppSession.getKeyUserType()));
    }

    @OnClick({R.id.imgLogoutIV, R.id.imgBackIV, R.id.changPwdLL})
    public void onViewClicked(View mView) {
        switch (mView.getId()) {
            case R.id.imgLogoutIV:

                if (inventoryAppSession.getKeyUserType().equals("Supervisor")) {
                    System.out.println("U ARE SUPERVISOR");
                    showConfirmationDialog();

                }
                if (inventoryAppSession.getKeyUserType().equals("Financial")) {
                    System.out.println("U ARE FINANCILA");
                    showConfirmationDialog();

                }
                if (inventoryAppSession.getKeyUserType().equals("Physical")) {
                    System.out.println("U ARE PHYSICAL");
                    showConfirmationDialog();

                }
                if (inventoryAppSession.getKeyUserType().equals("Admin")) {
                    System.out.println("U ARE ADMIN");
                    showConfirmationDialog();

                }
                break;

            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.changPwdLL:
                changePasswordDialog();
                break;
        }
    }

    private void changePasswordDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_change_pwd, null);
        final EditText editNewPwdET = (EditText) dialogView.findViewById(R.id.editNewPwdET);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editNewPwdET.getText().toString().trim().equals("")) {
                    editNewPwdET.setError("Please enter password!");
                } else {
                    alertDialog.dismiss();
                    //Execute Change Password API:
                    updateUserAPI(editNewPwdET.getText().toString().trim());
                }

            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog.dismiss();


                httpClearToken();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog.dismiss();

            }
        });
    }


    private void httpClearToken() {
        try {
            final String url = Content.baseURL + "site/cleartoken";
            CommonLoadingDialog.showLoadingDialog(mActivity, "Creating...");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    FirebaseAuth.getInstance().signOut();
                                    inventoryAppSession.user_logout();
                                    startActivity(new Intent(mActivity, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", inventoryAppSession.getKeyUserId());


                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    private void updateUserAPI(final String strPassword) {
        try {
            final String url = Content.baseURL + "users/" + strUserID;

            CommonLoadingDialog.showLoadingDialog(mActivity, "Updating...");

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    Toast.makeText(getApplicationContext(), "Password updated successfully", Toast.LENGTH_LONG).show();
                                } else {
                                    Gson gson = new Gson();
                                    AddUserErrorResponse addUserErrorResponse = gson.fromJson(response, AddUserErrorResponse.class);
                                    ArrayList<ErrorData> errorDataArrayList = addUserErrorResponse.getData();
                                    Toast.makeText(mActivity, "" + errorDataArrayList.get(0).getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", txtEmailValueTV.getText().toString().trim());
                    params.put("name", txtNameValueTV.getText().toString().trim());
                    params.put("password", strPassword);
                    System.out.println("OUTPUT PARAM=" + params);

                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ProfileActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);


            }
        }
    }
}
