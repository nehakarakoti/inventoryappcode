package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ProjectListAdapterOther;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.model.ProjectModel;
import com.app.inventoryapp.model.ProjectResponse;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeActivityOther extends BaseActivity {
    @BindView(R.id.ivBadge)
    ImageView ivBadge;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.imageView_add_project)
    ImageView imageView_add_project;

    @BindView(R.id.imageProfileIV)
    ImageView imageProfileIV;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;

    InventoryAppSession inventoryAppSession;
    ArrayList<ProjectModel> projectModelArrayList = new ArrayList<>();
    ArrayList<ProjectDataModel> projectDataModelArrayList = new ArrayList<>();
    InventoryAppSession session;
    ProjectListAdapterOther projectListAdapterOther;

    /*boolean isScrolling=false;
    int currentItems, totalItems, scrolloutItems;*/
    AlertDialog.Builder alertDialogBuilder;
    String strData = "";
    @BindView(R.id.imageView_notification)
    ImageView imageView_notification;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private String url = Content.baseURL;
    private boolean itShouldLoadMore = true;
    private int pageNo = 1;
    SelectedUserInterface selectedUserInterface;
    public BroadcastReceiver IconChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ivBadge.setVisibility(View.VISIBLE);

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_other);

        ButterKnife.bind(this);
        inventoryAppSession=new InventoryAppSession(HomeActivityOther.this);
        createHomeInfo();
        registerReceiver(IconChange, new IntentFilter("IconChange"));

        selectedUserInterface = new SelectedUserInterface() {
            @Override
            public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList) {

            }

            @Override
            public void changeBadgeICon(boolean t) {
                if (t) {
                    ivBadge.setVisibility(View.VISIBLE);
                } else {
                    ivBadge.setVisibility(View.GONE);
                }
            }
        };
        if (BaseActivity.notification_alert) {
            ivBadge.setVisibility(View.VISIBLE);
        } else
            ivBadge.setVisibility(View.GONE);
    }

    private void createHomeInfo() {
        imageView_add_project.setVisibility(View.VISIBLE);
        //imageLogOut.setVisibility(View.VISIBLE);
        //setList();
        session = new InventoryAppSession(getApplicationContext());
        refreshLayout.setRefreshing(true);

        /*Hidding Add project button for non admin user*/
        if (!session.getKeyUserType().equals("Admin")) {
            imageView_add_project.setVisibility(View.GONE);


        } else {

        }

        intit();
        reDefineUrl();
        httpAdminProject();
        imageView_add_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivityOther.this, AddProjectActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                InventoryAppSingleton.getInstance().setFromWhere("HOME");
            }
        });

        imageProfileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivityOther.this, ProfileActivity.class));
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                    if (itShouldLoadMore) {

                        loadMoreHttpAdminProject();

                    } else {

                    }
                }
            }
        });


        imageView_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseActivity.notification_alert) {
                    BaseActivity.notification_alert = false;
                    selectedUserInterface.changeBadgeICon(false);
                } else if (!BaseActivity.notification_alert) {
                    selectedUserInterface.changeBadgeICon(false);
                }
                startActivity(new Intent(HomeActivityOther.this, ProjectNotifications.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                httpAdminProject();
            }
        });
    }

    public void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }





    }


    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(HomeActivityOther.this);
    }


    private void flusGlobalData() {
        InventoryAppSingleton.getInstance().setItem_id("");
        InventoryAppSingleton.getInstance().setProject_id("");
        InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());

        InventoryAppSingleton.getInstance().setPhysicalProgress("");
        InventoryAppSingleton.getInstance().setFinancialProgress("");


    }

    private void reDefineUrl() {
        if (session.getKeyUserType().equals("Admin")) {
            url = url + "projectd?expand=tblIteams&page=";
        }
        if (session.getKeyUserType().equals("Supervisor")) {
            url = url + "projectd?expand=tblIteams&s_user=" + session.getKeyUserId() + "&page=";
        }
        if (session.getKeyUserType().equals("Financial")) {
            url = url + "projectd?expand=tblIteams&f_user=" + session.getKeyUserId() + "&page=";
        }
        if (session.getKeyUserType().equals("Physical")) {
            url = url + "projectd?expand=tblIteams&user_id=" + session.getKeyUserId() + "&page=";
        }

    }

    private void httpAdminProject() {

        try {

            refreshLayout.setRefreshing(true);
            pageNo = 1;


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url + pageNo++ + "&sort=-id",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            refreshLayout.setRefreshing(false);


                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();

                                    ProjectResponse projectResponse = gson.fromJson(response, ProjectResponse.class);

                                    if (projectDataModelArrayList.size() > 0) {
                                        projectDataModelArrayList.clear();
                                        projectListAdapterOther.updateAdapter(projectDataModelArrayList);
                                        projectDataModelArrayList = projectResponse.getData();

                                        ArrayList<ProjectDataModel> mTempAL = new ArrayList();
                                        for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                                            ProjectDataModel mModel = projectDataModelArrayList.get(i);
                                            if (mModel.getTrash().equals("0")) {
                                                mTempAL.add(mModel);
                                            }
                                        }


                                        if (mTempAL.size() < 20) {
                                            itShouldLoadMore = false;
                                        } else {
                                            itShouldLoadMore = true;
                                        }


                                        setList(mTempAL);


                                    } else {
                                        projectDataModelArrayList = projectResponse.getData();


                                        ArrayList<ProjectDataModel> mTempAL = new ArrayList();
                                        for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                                            ProjectDataModel mModel = projectDataModelArrayList.get(i);
                                            if (mModel.getTrash().equals("0")) {
                                                mTempAL.add(mModel);
                                            }
                                        }

                                        if (mTempAL.size() < 20) {
                                            itShouldLoadMore = false;
                                        }

                                        setList(mTempAL);
                                    }


                                }


                            } catch (Exception ex) {
                                refreshLayout.setRefreshing(false);
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            refreshLayout.setRefreshing(false);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivityOther.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    private void loadMoreHttpAdminProject() {
        try {

            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            //final String url = Content.baseURL + "projectds?expand=tblIteams";
            System.out.println("OUTPUT URL=====" + url);

            progressbar.setVisibility(View.VISIBLE);


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url + pageNo++,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressbar.setVisibility(View.GONE);


                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();

                                    ProjectResponse projectResponse = gson.fromJson(response, ProjectResponse.class);
                                    // projectDataModelArrayList= filterUserAsPerUser(projectResponse.getData());

                                    System.out.println("NUMBER OF PROJECT OF THE USER===" + projectDataModelArrayList.size());

                                    // projectDataModelArrayList = projectResponse.getData();

                                    System.out.println("REFRESH DATA SIZE=====" + projectResponse.getData().size());

                                    if (projectResponse.getData().size() != 0) {
                                        for (int i = 0; i < projectResponse.getData().size(); i++) {
                                            ProjectDataModel projectDataModel = projectResponse.getData().get(i);
                                            if (projectDataModel.getTrash().equals("0"))
                                                projectDataModelArrayList.add(projectDataModel);

                                        }
                                        projectListAdapterOther.updateAdapter(projectDataModelArrayList);

                                    } else {
                                        itShouldLoadMore = false;
                                        Toast.makeText(HomeActivityOther.this, "No more data to pull", Toast.LENGTH_SHORT).show();
                                    }


                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);

                                }


                            } catch (Exception ex) {
                                progressbar.setVisibility(View.VISIBLE);


                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            progressbar.setVisibility(View.VISIBLE);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivityOther.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex);
        }
    }

    private void setList(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        projectListAdapterOther = new ProjectListAdapterOther(HomeActivityOther.this, projectDataModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(projectListAdapterOther);


    }


    private ArrayList<ProjectDataModel> filterUserAsPerUser(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        ArrayList<ProjectDataModel> projectDataModelArrayList1 = new ArrayList<>();

        if (session.getKeyUserType().equals("Admin")) {
            projectDataModelArrayList1 = projectDataModelArrayList;
        }
        if (session.getKeyUserType().equals("Supervisor")) {

            for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                ProjectDataModel projectDataModel = projectDataModelArrayList.get(i);
                if (projectDataModel.getS_user().equals(session.getKeyUserId())) {
                    projectDataModelArrayList1.add(projectDataModel);
                }
            }
        }

        if (session.getKeyUserType().equals("Financial")) {
            for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                ProjectDataModel projectDataModel = projectDataModelArrayList.get(i);
                if (projectDataModel.getF_user().equals(session.getKeyUserId())) {
                    projectDataModelArrayList1.add(projectDataModel);
                }
            }
        }
        if (session.getKeyUserType().equals("Physical")) {
            for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                ProjectDataModel projectDataModel = projectDataModelArrayList.get(i);
                if (projectDataModel.getUser_id().equals(session.getKeyUserId())) {
                    projectDataModelArrayList1.add(projectDataModel);
                }
            }
        }

        return projectDataModelArrayList1;
    }


    private void httpClearToken() {
        try {
            final String url = Content.baseURL + "site/cleartoken";
            CommonLoadingDialog.showLoadingDialog(HomeActivityOther.this, "Creating...");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    session.user_logout();
                                    startActivity(new Intent(HomeActivityOther.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    finish();
                                }
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", session.getKeyUserId());


                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivityOther.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                httpClearToken();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
    }
}
