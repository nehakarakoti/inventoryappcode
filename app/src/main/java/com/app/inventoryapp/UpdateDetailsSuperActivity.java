package com.app.inventoryapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.DocumentListAdapter;
import com.app.inventoryapp.adapter.PaymentDocListAdapter;
import com.app.inventoryapp.adapter.UpdateDocListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.HistoryDataItem;
import com.app.inventoryapp.model.HistoryResponse;
import com.app.inventoryapp.model.ItemDataModel;
import com.app.inventoryapp.model.ItemDocModel;
import com.app.inventoryapp.model.ItemResponse;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.InventoryApp;
import butterknife.BindView;
import butterknife.ButterKnife;


public class UpdateDetailsSuperActivity extends BaseActivity {

    ImageView imageView;
    Button btn_histry,btn_info;

    @BindView(R.id.piechart)
    PieChart pieChart;

    @BindView(R.id.piechart2)
    PieChart pieChart2;

    @BindView(R.id.input_quantity)
    EditText input_quantity;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.textNoCertificate)
    TextView textNoCertificate;

    @BindView(R.id.TV_inspection_certificate_update)
    TextView TV_inspection_certificate_update;

    @BindView(R.id.TV_inspection_certificate_payment)
    TextView TV_inspection_certificate_payment;


    @BindView(R.id.view)
    View view;

    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.input_updated_quantity)
    EditText input_updated_quantity;

    @BindView(R.id.input_payment_amount)
    EditText input_payment_amount;

    @BindView(R.id.input_total_amount)
    EditText input_total_amount;

    @BindView(R.id.rv_paymentDoc_list)
    RecyclerView rv_paymentDoc_list;



    int []color={R.color.color_blue,R.color.color_pink};
    ProjectItemModel projectItemModel=new ProjectItemModel();


    ArrayList<String> stringArrayListUpdateDoc = new ArrayList<>();
    ArrayList<String> stringArrayListPaymentD0c = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details_super);

        ButterKnife.bind(this);
        //imageView =(ImageView)findViewById(R.id.imageView);
        btn_histry=(Button)findViewById(R.id.btn_histry);
        btn_info=(Button)findViewById(R.id.btn_info);


        System.out.println("OUTPUT======ITEM ID==="+ InventoryAppSingleton.getInstance().getItem_id());
        System.out.println("OUTPUT======PROJECT ID==="+ InventoryAppSingleton.getInstance().getProject_id());

        projectItemModel=(ProjectItemModel)getIntent().getExtras().getSerializable("ProjectItemModel");

        //projectItemModel = (ProjectItemModel) getIntent().getExtras().getSerializable("ProjectItemModel");
        if(projectItemModel!=null)
            InventoryAppSingleton.getInstance().setProjectItemModel(projectItemModel);


        /*System.out.println("OUTPUT======= ITEM ID===="+InventoryAppSingleton.getInstance().getItem_id());
        System.out.println("OUTPUT======= PROJECT ID===="+InventoryAppSingleton.getInstance().getProject_id());
        System.out.println("OUTPUT======= ITEM RATE===="+InventoryAppSingleton.getInstance().getItem_rate());
        System.out.println("OUTPUT=======CHART PHYSICAL PROGRESS===="+projectItemModel.getTotal_qty_done());
        System.out.println("OUTPUT=======CHART FINANCIAL PROGRESS===="+projectItemModel.getTotal_payment_done());
        System.out.println("OUTPUT MODEL============"+projectItemModel);*/



        btn_histry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(UpdateDetailsSuperActivity.this,HistoryActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });


        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle=new Bundle();
                bundle.putSerializable("projectItemModel",projectItemModel);
                startActivity(new Intent(UpdateDetailsSuperActivity.this,ProjectInfoOther.class).putExtras(bundle));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

            }
        });



        setStatus();

        httpGetItem();

        setData();

        setPhysicalChart(projectItemModel.getTotal_qty_done());
        setFinancialChart(projectItemModel.getTotal_payment_done());



    }

    private void setStatus()
    {
        /*Setting color in item level*/
        String itemLevelPhysicalProgress=InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel();
        String itemLevelFinancialProgress=InventoryAppSingleton.getInstance().getFinancialProgressItemlevel();
        System.out.println("ITEM LEVEL PROGRESS========PHYSICAL"+itemLevelPhysicalProgress);
        System.out.println("ITEM LEVEL PROGRESS========FINANCIAL"+itemLevelFinancialProgress);


        long diff= InventoryAppSingleton.getInstance().getDiff();
        if(diff>=0)
        {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        }
        else
        {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }

    private void setPhysicalChart(String dataPhysicalProgress)
    {
        Float DoneQnty=0.0f;
        Float notDoneQnty=0.0f;

        try
        {
            DoneQnty=Float.parseFloat(dataPhysicalProgress);
            notDoneQnty=100-DoneQnty;

            if(DoneQnty==0.0)
            {
                setSingleSet(notDoneQnty) ;
                System.out.println("PHYSICAL PROGRESS NOT DEFINE");

            }
            else if (DoneQnty==100.0)
            {
                System.out.println("PHYSICAL PROGRESS 100%");
                setSingleSetDone(DoneQnty);
            }
            else
            {
                System.out.println("PHYSICAL PROGRESS DEFINE");
                setDoubleSet(DoneQnty,notDoneQnty);
            }


        }
        catch (Exception ex)
        {
            DoneQnty=0.0f;
            notDoneQnty=0.0f;
        }


    }

    private void setSingleSetDone(Float doneQnty)
    {
        ArrayList<Entry> entries=new ArrayList<>() ;
        ArrayList<String> PieEntryLabels=new ArrayList<>() ;
        PieDataSet pieDataSet ;
        PieData pieData ;

        entries.add(new BarEntry(doneQnty,0));

        PieEntryLabels.add("");


        final int[] piecolors = new int[]{
                Color.rgb(0,128,0)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors));

        pieChart.setData(pieData);

        pieDataSet.setValueTextColor(Color.WHITE);
        pieChart.setDescription("");
        pieChart.animateXY(3000,3000);
        pieChart.getLegend().setEnabled(false);
        pieChart.setTouchEnabled(false);
    }

    private void setSingleSet(Float notDoneQnty)
    {
        ArrayList<Entry> entries=new ArrayList<>() ;
        ArrayList<String> PieEntryLabels=new ArrayList<>() ;
        PieDataSet pieDataSet ;
        PieData pieData ;
        entries.add(new BarEntry(notDoneQnty,0));
        PieEntryLabels.add("");

        final int[] piecolors = new int[]{
                Color.rgb(0,0,255)};

        pieDataSet = new PieDataSet(entries, "");
        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieDataSet.setColors(ColorTemplate.createColors(piecolors));
        pieChart.setData(pieData);
        pieDataSet.setValueTextColor(Color.WHITE);
        pieChart.setDescription("");
        pieChart.animateXY(3000,3000);
        pieChart.getLegend().setEnabled(false);
        pieChart.setTouchEnabled(false);
    }


    private void setDoubleSet(Float DoneQnty,Float notDoneQnty)
    {
        ArrayList<Entry> entries=new ArrayList<>() ;
        ArrayList<String> PieEntryLabels=new ArrayList<>() ;
        PieDataSet pieDataSet ;
        PieData pieData ;
        entries.add(new BarEntry( DoneQnty,0));
        entries.add(new BarEntry(notDoneQnty,1));

        PieEntryLabels.add("");
        PieEntryLabels.add("");


        final int[] piecolors = new int[]{
                Color.rgb(0,128,0),
                Color.rgb(0,0,255)};


        pieDataSet = new PieDataSet(entries, "");
        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieDataSet.setColors(ColorTemplate.createColors(piecolors));
        pieChart.setData(pieData);
        pieDataSet.setValueTextColor(Color.WHITE);
        pieChart.setDescription("");
        pieChart.animateXY(3000,3000);
        pieChart.getLegend().setEnabled(false);
        pieChart.setTouchEnabled(false);
    }

    private void setFinancialChart(String dataFinancialProgress)
    {


        Float DoneQnty=0.0f;
        Float notDoneQnty=0.0f;

        try {
            DoneQnty=Float.parseFloat(dataFinancialProgress);

            if(DoneQnty==0.0)
            {
                setOnlyNotDone(100.0f);

                System.out.println("FINANCIAL PROGRESS IS ZERO.......");
            }
            else if(DoneQnty==100.0)
            {
                System.out.println("FINANCIAL PROGRESS IS 100%.......");
                setOnlyDone(100.0f);
            }
            else
            {
                System.out.println("FINANCIAL PROGRESS IS NOT ZERO.......");

                setDoneAndNotQuantity(DoneQnty, 100f-DoneQnty);
            }







            notDoneQnty=100-DoneQnty;
        }
        catch (Exception ex)
        {
            DoneQnty=0.0f;
            notDoneQnty=0.0f;
        }


    }

    private void setOnlyDone(float qntyDone)
    {
        ArrayList<Entry> entries=new ArrayList<>() ;
        ArrayList<String> PieEntryLabels=new ArrayList<>() ;

        PieDataSet pieDataSet;
        PieData pieData ;
        entries.add(new BarEntry( qntyDone,0));


        PieEntryLabels.add("");


        final int[] piecolors2 = new int[]{
                Color.rgb(0,128,0)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors2));
        pieDataSet.setValueTextColor(Color.WHITE);
        pieChart2.setData(pieData);
        pieChart2.setDescription("");

        pieChart2.animateXY(3000,3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);

    }

    private void setDoneAndNotQuantity(Float doneQnty, float qntyNotDone)
    {
        ArrayList<Entry> entries2=new ArrayList<>() ;
        ArrayList<String> PieEntryLabels2=new ArrayList<>() ;

        PieDataSet pieDataSet2 ;
        PieData pieData2 ;
        entries2.add(new BarEntry( doneQnty,0));
        entries2.add(new BarEntry(qntyNotDone,1));

        PieEntryLabels2.add("");
        PieEntryLabels2.add("");

        final int[] piecolors2 = new int[]{
                Color.rgb(0,128,0),
                Color.rgb(0,0,255)};


        pieDataSet2 = new PieDataSet(entries2, "");

        pieData2 = new PieData(PieEntryLabels2, pieDataSet2);
        pieData2.setValueFormatter(new PercentFormatter());

        pieDataSet2.setColors(ColorTemplate.createColors(piecolors2));
        pieDataSet2.setValueTextColor(Color.WHITE);
        pieChart2.setData(pieData2);
        pieChart2.setDescription("");

        pieChart2.animateXY(3000,3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);
    }

    private void setOnlyNotDone(Float notDoneQuantity)
    {
        ArrayList<Entry> entries=new ArrayList<>() ;
        ArrayList<String> PieEntryLabels=new ArrayList<>() ;

        PieDataSet pieDataSet;
        PieData pieData ;
        entries.add(new BarEntry( notDoneQuantity,0));


        PieEntryLabels.add("");


        final int[] piecolors2 = new int[]{
                Color.rgb(0,0,255)};


        pieDataSet = new PieDataSet(entries, "");

        pieData = new PieData(PieEntryLabels, pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());

        pieDataSet.setColors(ColorTemplate.createColors(piecolors2));
        pieDataSet.setValueTextColor(Color.WHITE);
        pieChart2.setData(pieData);
        pieChart2.setDescription("");

        pieChart2.animateXY(3000,3000);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setTouchEnabled(false);
    }


    private void setDocList(ArrayList<ItemDocModel> itemDocModelArrayList)
    {
        DocumentListAdapter documentListAdapter =new DocumentListAdapter(UpdateDetailsSuperActivity.this, itemDocModelArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(documentListAdapter);

    }



   private void httpGetItem() {


       try {

           String url = Content.baseURL + "docps/history?i_id=" + InventoryAppSingleton.getInstance().getItem_id();

           System.out.println("OUTPUT URL====" + url);
           CommonLoadingDialog.showLoadingDialog(UpdateDetailsSuperActivity.this, "Loading....");


           StringRequest stringRequest = new StringRequest(Request.Method.OPTIONS, url,
                   new Response.Listener<String>() {
                       @Override
                       public void onResponse(String response) {
                           CommonLoadingDialog.closeLoadingDialog();


                           try {


                               JSONObject jsonObject = new JSONObject(response);
                               Gson gson = new Gson();
                               HistoryResponse historyResponse = gson.fromJson(response, HistoryResponse.class);


                               System.out.println("OUTPUT======SIZE===" + historyResponse.getData().size());
                               if (historyResponse.getData().size() == 0) {
                                   textNoCertificate.setVisibility(View.VISIBLE);
                               } else {

                                   setRemainingItem(historyResponse.getData());
                                   for (int i = 0; i < historyResponse.getData().size(); i++) {
                                       stringArrayListUpdateDoc.add(historyResponse.getData().get(i).getDocName());
                                       if (historyResponse.getData().get(i).getPayStatus() == 1) {
                                           stringArrayListPaymentD0c.add(historyResponse.getData().get(i).getPayment().get(0).getPay_img());
                                       }
                                   }


                                   setUpdateDocList();
                                   setPaymentDocList();

                               }


                               System.out.println("OUTPUT======SIZE===PAYMENT DOC" + stringArrayListPaymentD0c.size());
                               System.out.println("OUTPUT======SIZE===UPDATE DOC" + stringArrayListUpdateDoc.size());


                           } catch (Exception ex) {
                               CommonLoadingDialog.closeLoadingDialog();

                               System.out.println("OUTPUT ======ERROR==" + ex);
                           }


                       }
                   },
                   new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError error) {
                           System.out.println("OUTPUT ======ERROR==" + error.toString());
                           CommonLoadingDialog.closeLoadingDialog();
                       }
                   }) {
               @Override
               public Map<String, String> getHeaders() throws AuthFailureError {
                   HashMap<String, String> headers = new HashMap<String, String>();
                   System.out.println("HELLO HEDER===" + headers);
                   return headers;
               }
           };
           ;


           stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                   DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                   DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           RequestQueue requestQueue = Volley.newRequestQueue(UpdateDetailsSuperActivity.this);
           stringRequest.setShouldCache(false);
           requestQueue.add(stringRequest);
       } catch (Exception ex) {
           System.out.println("Error=====" + ex);
       }


   }
    private void setData()
    {
        input_quantity.setText(projectItemModel.getQty());
       // input_updated_quantity.setText(projectItemModel.getDone_work());
        /*Total Amount Calculation=======*/
        Double total_quantity=Double.parseDouble(projectItemModel.getQty());
        Double total_amount=total_quantity*Double.parseDouble(projectItemModel.getC_rate());
        input_total_amount.setText("" + String.format( "%.2f", total_amount ));
       // input_total_amount.setText(""+total_amount);
        /*Payment Amount Calculation*/
        input_payment_amount.setText(projectItemModel.getDone_payment());
    }


    private void setUpdateDocList() {

        if(stringArrayListUpdateDoc.size()>0)
        {
            TV_inspection_certificate_update.setVisibility(View.VISIBLE);
        }
        UpdateDocListAdapter updateDocListAdapter = new UpdateDocListAdapter(UpdateDetailsSuperActivity.this, stringArrayListUpdateDoc);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(updateDocListAdapter);
    }

    private void setPaymentDocList() {

        if (stringArrayListPaymentD0c.size()>0)
        {
            TV_inspection_certificate_payment.setVisibility(View.VISIBLE);
        }
        System.out.println("PAYMENT DOC  "+stringArrayListPaymentD0c.size());
        PaymentDocListAdapter paymentDocListAdapter = new PaymentDocListAdapter(UpdateDetailsSuperActivity.this, stringArrayListPaymentD0c);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        layoutManager.setReverseLayout(false);
        rv_paymentDoc_list.setLayoutManager(layoutManager);
        rv_paymentDoc_list.setAdapter(paymentDocListAdapter);
    }

    private void setRemainingItem(List<HistoryDataItem> historyDataItemArrayList)
    {
        Double remainingItem=0.0;
        for(int i=0; i<historyDataItemArrayList.size();i++)
        {
            if(historyDataItemArrayList.get(i).getPayStatus()!=2)
                remainingItem= remainingItem + Double.parseDouble(historyDataItemArrayList.get(i).getQtyDone());
        }
        input_updated_quantity.setText("" + String.format( "%.2f", +remainingItem ));



    }
    @Override
    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {
            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                    loggingOut(UpdateDetailsSuperActivity.this);
                }

            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }
        }
    }

}
