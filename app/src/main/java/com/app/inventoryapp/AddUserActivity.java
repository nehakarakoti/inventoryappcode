package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.CustomSpinnerAdapter;
import com.app.inventoryapp.adapter.SupervisorSpinnerAdapter;
import com.app.inventoryapp.font.CustomTextView;
import com.app.inventoryapp.model.AddUserErrorResponse;
import com.app.inventoryapp.model.ErrorData;
import com.app.inventoryapp.model.UserTypeModel;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.EmailValidator;
import com.app.inventoryapp.utils.Utilities;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddUserActivity extends BaseActivity {

    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.input_user_name)
    EditText input_user_name;

    @BindView(R.id.spinner_user_type)
    Spinner spinner_user_type;

    @BindView(R.id.input_user_id)
    EditText input_user_id;

    @BindView(R.id.input_password)
    EditText input_password;
    @BindView(R.id.input_CellNo)
    EditText input_CellNo;

    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.btn_submit)
    Button btn_submit;

    String userType = "";

    String strSupervisorID = "";
    String strSupervisorName = "";

    @BindView(R.id.spinnerSupervisorSPN)
    Spinner spinnerSupervisorSPN;
    @BindView(R.id.supervisorLL)
    LinearLayout supervisorLL;
    AlertDialog.Builder alertDialogBuilder;
    SupervisorSpinnerAdapter mSupervisorSpinnerAdapter;
    CustomSpinnerAdapter customSpinnerAdapter;
    private ArrayList<UserTypeModel> userTypeModelArrayList = new ArrayList<>();
    private ArrayList<UsersResponse> userSupervisorAL = new ArrayList<UsersResponse>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);


        ButterKnife.bind(this);

        UsersResponse mUsersResponse = new UsersResponse();
        mUsersResponse.setId("0");
        mUsersResponse.setName("Select Officer In charge");
        mUsersResponse.setCreated_on("0");
        mUsersResponse.setUpdated_on("0");

        userSupervisorAL.add(mUsersResponse);
        setSuperVisorSpinnerAdapter();


        intit();
       /* imageView =(ImageView)findViewById(R.id.imageView);*/

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddUserActivity.this, HistoryActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateUserName() && validateCellNumber() && validateEmail() && validatePassword() && validateRole() && validateSupervisor()) {
                    createUserHttpCall();
                }
            }
        });


        spinner_user_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = ((CustomTextView) view.findViewById(R.id.textspinner));
                Log.e("USER_TYPE", "=========USER_TYPE====" + userType);
                if (textView == null) {
                    userType = "Select role";
                } else {
                    userType = Utilities.getPreviousRoleText(textView.getText().toString());
                }

                System.out.println("OUTPUT USER TYPE ====" + userType);

                if (userType.equals("Physical")) {
                    supervisorLL.setVisibility(View.VISIBLE);
                } else {
                    supervisorLL.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        UsersResponse mUsersResponse99 = (UsersResponse) ((Spinner) findViewById(R.id.spinnerSupervisorSPN)).getSelectedItem();
        strSupervisorID = mUsersResponse99.getId();
        strSupervisorName = mUsersResponse99.getName();
        System.out.print("=======SUPERVISOR ID=======" + strSupervisorID + "====SUPERVISOR NAME===" + strSupervisorName);

        spinnerSupervisorSPN.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //strSupervisorName = spinnerSupervisorSPN.getItemAtPosition(position).toString();

                UsersResponse mUsersResponse = (UsersResponse) ( (Spinner) findViewById(R.id.spinnerSupervisorSPN) ).getSelectedItem();
                strSupervisorID = mUsersResponse.getId();
                strSupervisorName = mUsersResponse.getName();

                System.out.print("=======SUPERVISOR ID=======" + strSupervisorID + "====SUPERVISOR NAME===" + strSupervisorName);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        getUserTypeHttpCall();

    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(AddUserActivity.this);
    }

    private void getUserTypeHttpCall() {
        try {
            final String url = Content.baseURL + "types";
            CommonLoadingDialog.showLoadingDialog(AddUserActivity.this, "Loading..");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            /*
                            * Setting for first element of Array list As select role
                            *
                            * */

                            UserTypeModel userTypeModel2 = new UserTypeModel();
                            userTypeModel2.setId("0");

                            userTypeModel2.setUser_name("Select role");
                            userTypeModel2.setType_code("0");
                            userTypeModel2.setCreated_on("0");
                            userTypeModel2.setUpdated_on("0");

                            userTypeModelArrayList.add(userTypeModel2);
                            /*================================================*/
                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                    UserTypeModel userTypeModel = new UserTypeModel();
                                    userTypeModel.setId(jsonObject.getString("id"));
                                    userTypeModel.setUser_name(jsonObject.getString("user_name"));
                                    userTypeModel.setType_code(jsonObject.getString("type_code"));
                                    userTypeModel.setCreated_on(jsonObject.getString("created_on"));
                                    userTypeModel.setUpdated_on(jsonObject.getString("updated_on"));

                                    userTypeModelArrayList.add(userTypeModel);

                                    setSpinner();

                                }

                                System.out.println("OUT PUT===" + userTypeModelArrayList.size());

                                getAllSupervisorApiExecute();

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AddUserActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void getAllSupervisorApiExecute() {
        try {
            final String url = Content.GET_ALL_SUPERVISOR;
            //CommonLoadingDialog.showLoadingDialog(AddUserActivity.this, "Creating...");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            /*==========================================*/

                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                System.out.println("SIZE====" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                    UsersResponse usersResponse = new UsersResponse();
                                    usersResponse.setId(jsonObject1.getString("id"));
                                    usersResponse.setUsername(jsonObject1.getString("username"));
                                    usersResponse.setUsertype(jsonObject1.getString("usertype"));
                                    usersResponse.setName(jsonObject1.getString("name"));
                                    //trial adding
                                    usersResponse.setPassword(jsonObject1.getString("password"));
                                    userSupervisorAL.add(usersResponse);
                                }

                                System.out.println("Supervisor SIZE====" + userSupervisorAL.size());
                                setSuperVisorSpinnerAdapter();
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AddUserActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setSuperVisorSpinnerAdapter() {
        mSupervisorSpinnerAdapter = new SupervisorSpinnerAdapter(AddUserActivity.this, userSupervisorAL);
        spinnerSupervisorSPN.setAdapter(mSupervisorSpinnerAdapter);
    }

    private void createUserHttpCall() {
        try {
            final String url = Content.baseURL + "users";
            CommonLoadingDialog.showLoadingDialog(AddUserActivity.this, "Creating...");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    Gson gson = new Gson();
                                    //AddUserErrorResponse addUserErrorResponse = gson.fromJson(response, AddUserErrorResponse.class);
                                    Toast.makeText(AddUserActivity.this, "User added successfully", Toast.LENGTH_SHORT).show();
                                    clearField();
                                    startActivity(new Intent(AddUserActivity.this, UserActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                    // finish();
                                } else {
                                    Gson gson = new Gson();
                                    AddUserErrorResponse addUserErrorResponse = gson.fromJson(response, AddUserErrorResponse.class);
                                    ArrayList<ErrorData> errorDataArrayList = addUserErrorResponse.getData();
                                    Toast.makeText(AddUserActivity.this, "" + errorDataArrayList.get(0).getMessage(), Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", input_user_id.getText().toString().trim());
                    params.put("usertype", userType.trim());
                    params.put("name", input_user_name.getText().toString().trim());
                    params.put("password", input_password.getText().toString().trim());
                    params.put("cellno", input_CellNo.getText().toString().trim());
                    if (userType.equals("Physical")) {
                        params.put("supervisor", strSupervisorID);
                    }
                    Log.e("Inventory signin == ", url + params);

                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AddUserActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    private void clearField() {


        input_user_name.setText("");
        input_user_id.setText("");
        input_password.setText("");
        spinner_user_type.setSelection(0);


    }

    private void setSpinner() {
        customSpinnerAdapter = new CustomSpinnerAdapter(AddUserActivity.this, userTypeModelArrayList);
        spinner_user_type.setAdapter(customSpinnerAdapter);
    }

    //=======================================================================================================
    private boolean validateUserName() {
        if (input_user_name.getText().toString().trim().equals("")) {

            //showCustomDialog("Name should not be empty");
            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Name should not be empty");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateCellNumber() {
        if (input_CellNo.getText().toString().trim().equals("") || input_CellNo.getText().toString().trim().length() != 10) {

            //showCustomDialog("Name should not be empty");
            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Cell No should not be empty, It must be a 10 digits");
            return false;
        } else {
            return true;
        }
    }


    private boolean validateRole() {
        if (userType.equals("Select role")) {//Select role
            //showCustomDialog("Please select role");
            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Please select role");
            return false;
        }
        return true;
    }

    private boolean validateSupervisor() {
        if (userType.equals("Physical") && strSupervisorName.equals("Select Officer In charge")) {
            //showCustomDialog("Please select role");
            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Please select Officer In charge");
            return false;
        }
        return true;
    }


    private boolean validatePassword() {

        if (input_password.getText().toString().trim().equals("")) {
            // showCustomDialog("Password should not be empty");
            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Password should not be empty");

            return false;
//        } else if (input_password.getText().toString().trim().length() < 8) {
//            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Password must be of 8 characterts");
//            return false;
        } else {
            return true;
        }
        //return true;

    }

    private boolean validateEmail() {
        if (input_user_id.getText().toString().trim().equals("")) {
            //showCustomDialog("Email should not be empty");
            CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Email should not be empty");

            return false;
        } else {
            if (new EmailValidator().validateEmail(input_user_id.getText().toString().trim())) {

                return true;
            } else {
                //showCustomDialog("Invalid email format");
                CommonValidationDialog.showLoadingDialog(AddUserActivity.this, "Invalid email format");

                return false;
            }
        }

    }
    /*private void showErrorMsgDialog( String errorMsg)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(
                AddUserActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("");

        // Setting Dialog Message
        alertDialog.setMessage(""+errorMsg);

        // Setting Icon to Dialog


        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
               // Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }*/

    private void showCustomDialog(String msg) {

        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText(msg);

        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
        /*final android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        final View dialogView = LayoutInflater.inflate(R.layout.dialog_layout, null);


        // ButterKnife.bind(this, dialogView);

        dialogBuilder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();

      // alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();*/


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {

            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                    loggingOut(AddUserActivity.this);
                } else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }
        }

    }
}
