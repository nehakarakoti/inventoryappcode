package com.app.inventoryapp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inventoryapp.model.LoginResponse;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtpActivity extends AppCompatActivity {
    public static final int REQUEST_PERMISSION_CODE = 919;

    Activity mActivity = OtpActivity.this;
    String TAG = OtpActivity.this.getClass().getSimpleName();
    @BindView(R.id.flag_imv)
    ImageView flagImv;
    @BindView(R.id.etCountryCode)
    TextView etCountryCode;
    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;
    @BindView(R.id.btnSendConfirmationCode)
    AppCompatButton btnSendConfirmationCode;

    String receiveSms = android.Manifest.permission.RECEIVE_SMS;
    String readSms = android.Manifest.permission.READ_SMS;
    String read_PhoneState = android.Manifest.permission.READ_PHONE_STATE;
    //LoginResponse mLoginResponse;
    private InventoryAppSession inventoryAppSession;
    String strCellNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        inventoryAppSession = new InventoryAppSession(mActivity);
        strCellNumber = inventoryAppSession.getKeyUserCellNumber();
        Log.e(TAG,"====Cell Number===="+strCellNumber);
//        if ((LoginResponse) getIntent().getSerializableExtra("MODEL") != null) {
//            mLoginResponse = (LoginResponse) getIntent().getSerializableExtra("MODEL");
//            inventoryAppSession.setKeyUserCellNumber(mLoginResponse.getData().get(0).getCellno());
//        }
        if (checkPermission()) {
        } else {
            requestPermission();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        etPhoneNumber.setEnabled(false);
        etPhoneNumber.setText(strCellNumber);
    }

    @OnClick(R.id.btnSendConfirmationCode)
    public void onViewClicked() {
        sendConfimationsClick();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void sendConfimationsClick() {
        Utilities.hideKeyBoardFromView(mActivity);
        etPhoneNumber.setError(null);
        if (validate()) {
            Intent verificationIntent = new Intent(mActivity, VerificationCodeActivity.class);
            verificationIntent.putExtra(Content.PhoneNumber, etPhoneNumber.getText().toString().trim());
            verificationIntent.putExtra(Content.PhoneCode, "+91");
            //verificationIntent.putExtra("MODEL", mLoginResponse);
            verificationIntent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            startActivity(verificationIntent);
            finish();
        }
    }

    private boolean validate() {
        if (TextUtils.isEmpty(etPhoneNumber.getText().toString().trim())) {
            etPhoneNumber.setError("Please enter phone number");
            etPhoneNumber.requestFocus();
            return false;
        } else if (etPhoneNumber.getText().toString().trim().length() != 10) {
            etPhoneNumber.setError("Please enter valid phone number");
            etPhoneNumber.requestFocus();
            return false;
        }
        return true;
    }

    /*********
     * Support for Marshmallows Version
     .RECEIVE_SMS;
     .READ_SMS;
     .READ_PHONE_STATE
     **********/
    private boolean checkPermission() {
        int receiveSMS = ContextCompat.checkSelfPermission(mActivity, receiveSms);
        int readSMS = ContextCompat.checkSelfPermission(mActivity, readSms);
        int readPhoneState = ContextCompat.checkSelfPermission(mActivity, read_PhoneState);
        return receiveSMS == PackageManager.PERMISSION_GRANTED && readSMS == PackageManager.PERMISSION_GRANTED && readPhoneState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{receiveSms, readSms, read_PhoneState}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //sendConfimationsClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission();
                }
                return;
            }

        }
    }


}
