package com.app.inventoryapp.app;

import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.model.UsersResponse;

import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/6/2018.
 */

public class InventoryAppSingleton {
    private static InventoryAppSingleton inventoryAppSingleton;
    ProjectItemModel projectItemModel = new ProjectItemModel();
    private String fromWhere = "";
    private String project_id;
    private String project_quantity;
    private String project_rate;
    private String item_id = "";
    private String user_id = "";
    private String f_user = "";
    private String s_user = "";
    private String starting_date = "";
    private String ending_date = "";
    private long diff;
    private String financialProgress;
    private String physicalProgress;
    private String financialProgressItemlevel;
    private String physicalProgressItemLevel;
    private String item_rate = "";
    private ArrayList<UsersResponse> usersResponseArrayList;

    private InventoryAppSingleton() {

    }

    public static InventoryAppSingleton getInstance() {
        if (inventoryAppSingleton == null) {
            inventoryAppSingleton = new InventoryAppSingleton();
        }
        return inventoryAppSingleton;
    }

    public String getStarting_date() {
        return starting_date;
    }

    public void setStarting_date(String starting_date) {
        this.starting_date = starting_date;
    }

    public String getEnding_date() {
        return ending_date;
    }

    public void setEnding_date(String ending_date) {
        this.ending_date = ending_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getF_user() {
        return f_user;
    }

    public void setF_user(String f_user) {
        this.f_user = f_user;
    }

    public String getS_user() {
        return s_user;
    }

    public void setS_user(String s_user) {
        this.s_user = s_user;
    }

    public String getItem_rate() {
        return item_rate;
    }

    public void setItem_rate(String item_rate) {
        this.item_rate = item_rate;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getFromWhere() {
        return fromWhere;
    }

    public void setFromWhere(String fromWhere) {
        this.fromWhere = fromWhere;
    }


    public ArrayList<UsersResponse> getUsersResponseArrayList() {
        return usersResponseArrayList;
    }

    public void setUsersResponseArrayList(ArrayList<UsersResponse> usersResponseArrayList) {
        this.usersResponseArrayList = usersResponseArrayList;
    }


    public String getFinancialProgress() {
        return financialProgress;
    }

    public void setFinancialProgress(String financialProgress) {
        this.financialProgress = financialProgress;
    }

    public String getPhysicalProgress() {
        return physicalProgress;
    }

    public void setPhysicalProgress(String physicalProgress) {
        this.physicalProgress = physicalProgress;
    }


    public String getFinancialProgressItemlevel() {
        return financialProgressItemlevel;
    }

    public void setFinancialProgressItemlevel(String financialProgressItemlevel) {
        this.financialProgressItemlevel = financialProgressItemlevel;
    }

    public String getPhysicalProgressItemLevel() {
        return physicalProgressItemLevel;
    }

    public void setPhysicalProgressItemLevel(String physicalProgressItemLevel) {
        this.physicalProgressItemLevel = physicalProgressItemLevel;
    }

    public ProjectItemModel getProjectItemModel() {
        return projectItemModel;
    }

    public void setProjectItemModel(ProjectItemModel projectItemModel) {
        this.projectItemModel = projectItemModel;
    }


    public long getDiff() {
        return diff;
    }

    public void setDiff(long diff) {
        this.diff = diff;
    }
}
