package com.app.inventoryapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.TrashProjectListAdapter;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.model.ProjectResponse;
import com.app.inventoryapp.my_interface.PaginationInterface;
import com.app.inventoryapp.my_interface.ProjectDeleteUndoListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrashProjectActivity extends BaseActivity {
    private  boolean itShouldLoadMore = false;
    private  int pageNo = 1;
    Activity mActivity = TrashProjectActivity.this;
    String TAG = TrashProjectActivity.this.getClass().getSimpleName();
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    @BindView(R.id.LL_rv_container)
    LinearLayout LLRvContainer;
    @BindView(R.id.LL_container)
    LinearLayout LLContainer;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    TrashProjectListAdapter mTrashProjectListAdapter;
    ArrayList<ProjectDataModel> projectDataModelArrayList = new ArrayList<>();
    InventoryAppSession session;
    AlertDialog.Builder alertDialogBuilder;
    ProjectDeleteUndoListener mProjectDeleteUndoListener = new ProjectDeleteUndoListener() {
        @Override
        public void undoProject(String project_id, String strStartDate, String strEndDate) {
            /*Execute Undo Api*/
            undoProjectApi(project_id, strStartDate, strEndDate);
        }

        @Override
        public void deleteProject(String project_id) {
            /*Execute Delete Api*/
            show_delete_confirmation_dialog(project_id);
        }
    };
    private String url = Content.baseURL;
    PaginationInterface mPaginationInterface = new PaginationInterface() {
        @Override
        public void loadMorePage() {
            if (itShouldLoadMore) {
                loadMoreHttpAdminProject();

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash_project);
        ButterKnife.bind(this);
        intit();
        session = new InventoryAppSession(mActivity);
        reDefineUrl();
        swipeRefreshWithPaginations();
    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(mActivity);
    }

    private void reDefineUrl() {

        if (session.getKeyUserType().equals("Admin")) {
            url = url + "projectd?expand=tblIteams&page=";
        }
        if (session.getKeyUserType().equals("Supervisor")) {
            url = url + "projectd?expand=tblIteams&s_user=" + session.getKeyUserId() + "&page=";
        }
        if (session.getKeyUserType().equals("Financial")) {
            url = url + "projectd?expand=tblIteams&f_user=" + session.getKeyUserId() + "&page=";
        }
        if (session.getKeyUserType().equals("Physical")) {
            url = url + "projectd?expand=tblIteams&user_id=" + session.getKeyUserId() + "&page=";
        }

    }

    private void swipeRefreshWithPaginations() {
        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                    if (itShouldLoadMore) {
                        loadMoreHttpAdminProject();
                        //System.out.println("ITS TIME TO HIT API");
                    } else {

                    }
                }
            }
        });*/

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                httpAdminProject();
            }
        });

    }

    private void httpAdminProject() {
        try {
            refreshLayout.setRefreshing(true);

//url + pageNo++ + "&sort=-id"
            StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://dharmani.com/railway_api/web/projectds?page=1" + "&trash=1",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            refreshLayout.setRefreshing(false);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();
                                    projectDataModelArrayList.clear();
                                    ProjectResponse projectResponse = gson.fromJson(response, ProjectResponse.class);

                                    if (projectDataModelArrayList.size() > 0) {
                                        Log.i("kjskfjijsflwjfj",String.valueOf(projectDataModelArrayList.size()));
                                        projectDataModelArrayList.clear();
                                        mTrashProjectListAdapter.updateAdapter(projectDataModelArrayList);
                                        projectDataModelArrayList = projectResponse.getData();

//                                        ArrayList<ProjectDataModel> mTempAL = new ArrayList();
//                                        for (int i = 0; i < projectDataModelArrayList.size(); i++) {
//                                            ProjectDataModel mModel = projectDataModelArrayList.get(i);
//
//                                                mTempAL.add(mModel);
//                                           // }
//                                        }


                                        if (projectDataModelArrayList.size() < 20) {
                                            itShouldLoadMore = false;
                                        } else {
                                            itShouldLoadMore = true;
                                        }

                                        setList(projectDataModelArrayList);

                                    } else {
             projectDataModelArrayList.clear();
                                        projectDataModelArrayList = projectResponse.getData();
                                        Log.i("kjskfjijsflwjfj",String.valueOf(projectDataModelArrayList.size()));

//                                        ArrayList<ProjectDataModel> mTempAL = new ArrayList();
//                                        for (int i = 0; i < projectDataModelArrayList.size(); i++) {
//                                            ProjectDataModel mModel = projectDataModelArrayList.get(i);
////                                            if (mModel.getTrash().equals("1")) {
//                                                mTempAL.add(mModel);
//                                           // }
                                  //      }


                                        if (projectDataModelArrayList.size() < 20) {
                                            itShouldLoadMore = false;
                                        } else {
                                            itShouldLoadMore = true;
                                        }


                                        setList(projectDataModelArrayList);

                                    }

                                }


                            } catch (Exception ex) {
                                refreshLayout.setRefreshing(false);

                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            if (CommonLoadingDialog.alertDialog != null)
                                CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void loadMoreHttpAdminProject() {
        try {

            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            //final String url = Content.baseURL + "projectds?expand=tblIteams";//url + pageNo++ + "&sort=-id"
            System.out.println("OUTPUT URL=====" + url);
//+ pageNo++
            progressbar.setVisibility(View.VISIBLE);
            pageNo=pageNo+1;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://dharmani.com/railway_api/web/projectds?page="+pageNo  + "&trash=1",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //CommonLoadingDialog.closeLoadingDialog();
                            progressbar.setVisibility(View.GONE);

                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();

                                    ProjectResponse projectResponse = gson.fromJson(response, ProjectResponse.class);
                                    // projectDataModelArrayList= filterUserAsPerUser(projectResponse.getData());
                                    System.out.println("NUMBER OF PROJECT OF THE USER===" + projectDataModelArrayList.size());
                                    // projectDataModelArrayList = projectResponse.getData();
                                    System.out.println("REFRESH DATA SIZE=====" + projectResponse.getData().size());

//                                    ArrayList<ProjectDataModel> mTempAL = new ArrayList<>();
//                                    mTempAL.clear();
                                    if (projectResponse.getData().size() != 0) {

                                        for (int i = 0; i < projectResponse.getData().size(); i++) {

                                            ProjectDataModel projectDataModel = projectResponse.getData().get(i);
                                            projectDataModelArrayList.add(projectDataModel);
                                        }

                                        //projectDataModelArrayList.addAll(mTempAL);
                                        mTrashProjectListAdapter.updateAdapter(projectDataModelArrayList);
                                        itShouldLoadMore = true;

                                    } else {
                                        itShouldLoadMore = false;
                                        Toast.makeText(mActivity, "No more data to pull", Toast.LENGTH_SHORT).show();
                                    }


                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);

                                }


                            } catch (Exception ex) {
                                progressbar.setVisibility(View.GONE);

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            progressbar.setVisibility(View.GONE);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex);
        }
    }

    private void setList(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mTrashProjectListAdapter = new TrashProjectListAdapter(mActivity, projectDataModelArrayList, mProjectDeleteUndoListener, mPaginationInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mTrashProjectListAdapter);
    }

    @OnClick(R.id.back_btn)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void show_delete_confirmation_dialog(final String project_id) {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure you want to delete permanently?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProjectApi(project_id);
                alertDialog.dismiss();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    private void deleteProjectApi(String project_id) {
        try {
            String url = Content.baseURL + "projects/" + project_id;
            System.out.println("CURRENT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(mActivity, "Deleting....");
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {

                                    Toast.makeText(mActivity, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();

//                                    startActivity(new Intent(mActivity, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
//                                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

//                                    finish();
                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);

                                    httpAdminProject();

                                } else {
                                    Toast.makeText(mActivity, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();

                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    /*Undo Project*/

    private void undoProjectApi(String project_id, String strStartDate, String strEndDate) {
        try {


            String startArray[] = strStartDate.split("-");
            String endArray[] = strEndDate.split("-");

            final String finalStartDate = startArray[2] + "-" + startArray[1] + "-" + startArray[0];
            final String finalEndDate = endArray[2] + "-" + endArray[1] + "-" + endArray[0];
            Log.e(TAG, "====startDate====" + finalStartDate);
            Log.e(TAG, "====endDate====" + finalStartDate);


            String url = Content.baseURL + "projects/" + project_id;
            System.out.println("CURRENT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(mActivity, "Undo....");
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    if (jsonObject.getBoolean("status")) {
                                        Toast.makeText(mActivity, "Project Undo!", Toast.LENGTH_SHORT).show();
                                        /*Again Getting/Refreshed the Project*/
                                        httpAdminProject();

                                    } else {
                                        Toast.makeText(mActivity, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                                    }


                                } else {
                                    Toast.makeText(mActivity, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("trash", "0");
                    params.put("starting_date", finalStartDate);
                    params.put("ending_date", finalEndDate);
                    return params;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        httpAdminProject();
        if (inventoryAppSession.getTimeStamp() != null) {

            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();
                    loggingOut(TrashProjectActivity.this);
                } else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");
                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);

                    httpAdminProject();
                }
            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
                httpAdminProject();
            }


        }

    }
}
