package com.app.inventoryapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class ProjectInfoPhysical extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_info_physical);
    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ProjectInfoPhysical.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }

    }
}
