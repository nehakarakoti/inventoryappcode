package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.font.CustomButton;
import com.app.inventoryapp.font.CustomEditText;
import com.app.inventoryapp.model.AddUserErrorResponse;
import com.app.inventoryapp.model.ErrorData;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.EmailValidator;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EditUser extends AppCompatActivity {
    @BindView(R.id.back_btn)
    ImageView back_btn;


    UsersResponse usersResponse = new UsersResponse();
    String UserHasPassword = "";
    @BindView(R.id.input_user_name)
    EditText input_user_name;
    @BindView(R.id.input_user_email)
    CustomEditText inputUserEmail;
    @BindView(R.id.input_user_password)
    CustomEditText inputUserPassword;
    @BindView(R.id.btn_submit)
    CustomButton btnSubmit;
    String password = "";
    AlertDialog.Builder alertDialogBuilder;
    /*@BindView(R.id.spinner_user_type)
    Spinner spinner_user_type;*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        ButterKnife.bind(this);
        usersResponse = (UsersResponse) getIntent().getExtras().getSerializable("UsersResponse");
        //   UserHasPassword=getIntent().getExtras().getString("password");
//        if(!UserHasPassword.equals("")){
//            password=UserHasPassword;
//            Toast.makeText(EditUser.this,"iamempty",Toast.LENGTH_SHORT).show();
//        }
        System.out.println("NAME====" + usersResponse.getName());

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setData();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateName() && validateEmail() /*&& validatePassword()*/) {
                    showConfirmationDialog();
                }

            }
        });

        intit();

    }


    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(EditUser.this);
    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure you want  to edit?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  new AddProjectActivity.UpdateProjectData().execute();
                alertDialog.dismiss();

                editUserApi();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }

    private void editUserApi() {
        try {
            final String url = Content.baseURL + "users/" + usersResponse.getId();

            CommonLoadingDialog.showLoadingDialog(EditUser.this, "Creating...");


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean status = jsonObject.getBoolean("status");
                                if (status) {


                                    startActivity(new Intent(EditUser.this, UserActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                    finish();
                                    Toast.makeText(getApplicationContext(), "User updated successfully", Toast.LENGTH_LONG).show();
                                } else {
                                    Gson gson = new Gson();
                                    AddUserErrorResponse addUserErrorResponse = gson.fromJson(response, AddUserErrorResponse.class);
                                    ArrayList<ErrorData> errorDataArrayList = addUserErrorResponse.getData();

                                    Toast.makeText(EditUser.this, "" + errorDataArrayList.get(0).getMessage(), Toast.LENGTH_SHORT).show();


                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", inputUserEmail.getText().toString().trim());
                    params.put("name", input_user_name.getText().toString().trim());
                    if (!inputUserPassword.getText().toString().trim().equals(""))
                        params.put("password", inputUserPassword.getText().toString().trim());

                    Log.e("Inventory signin == ", url + params);

                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(EditUser.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setData() {
        inputUserEmail.setText(usersResponse.getUsername());
        input_user_name.setText(usersResponse.getName());
        // inputUserPassword.setText(password);
    }


    public boolean validateName() {
        if (input_user_name.getText().toString().trim().equals("")) {
            CommonValidationDialog.showLoadingDialog(EditUser.this, "Name should not be empty");
            return false;
        }

        return true;
    }


    public boolean validateEmail() {
        if (inputUserEmail.getText().toString().trim().equals("")) {
            //showCustomDialog("Email should not be empty");
            CommonValidationDialog.showLoadingDialog(EditUser.this, "Email should not be empty");

            return false;
        } else {
            if (new EmailValidator().validateEmail(inputUserEmail.getText().toString().trim())) {

                return true;
            } else {
                //showCustomDialog("Invalid email format");
                CommonValidationDialog.showLoadingDialog(EditUser.this, "Invalid email format");

                return false;
            }
        }
    }

    public boolean validatePassword() {
        if (inputUserPassword.getText().toString().trim().equals("")) {
            //showCustomDialog("Email should not be empty");
            //CommonValidationDialog.showLoadingDialog(EditUser.this,"Please enter password");

            return true;
        } else {
            if (inputUserPassword.getText().length() > 8) {

                return true;
            } else {
                //showCustomDialog("Invalid email format");
                CommonValidationDialog.showLoadingDialog(EditUser.this, "Please enter password of more than 8 characters");

                return false;
            }
        }

    }
}
