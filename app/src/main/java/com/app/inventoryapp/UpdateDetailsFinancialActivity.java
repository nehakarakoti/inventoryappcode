package com.app.inventoryapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Header;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.UpdateDetailsPaymentAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.font.CustomButton;
import com.app.inventoryapp.model.DataItem;
import com.app.inventoryapp.model.DummyUpdateDetailsModel;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.model.UnpaidResponseModel;
import com.app.inventoryapp.my_interface.PaymentDocUploadInterface;
import com.app.inventoryapp.my_interface.UpdateDetailsInterface;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.CompressImage;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.DocPath;
import com.app.inventoryapp.utils.MultipartUtility;
import com.app.inventoryapp.utils.MyFileExtension;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UpdateDetailsFinancialActivity extends AppCompatActivity implements UpdateDetailsInterface, PaymentDocUploadInterface {
    private static final int CHOOSE_FILE_REQUESTCODE = 102;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    public static double rate_per_unit;
    ProjectItemModel projectItemModel = new ProjectItemModel();
    InventoryAppSession inventoryAppSession;
    Activity mActivity = UpdateDetailsFinancialActivity.this;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.RL_recyclerView_container)
    LinearLayout RLRecyclerViewContainer;
    @BindView(R.id.btn_histry)
    CustomButton btnHistry;
    @BindView(R.id.btn_info)
    CustomButton btnInfo;
    @BindView(R.id.LL_button_container)
    LinearLayout LLButtonContainer;
    @BindView(R.id.RL_container)
    RelativeLayout RLContainer;
    @BindView(R.id.LL_no_data_info)
    LinearLayout LL_no_data_info;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    InventoryAppSession session;
    File fileToUpload;
    String filePath, fileName;
    ImageView docImage;
    RelativeLayout RL_image_container_d;
    RelativeLayout RL_doc_container;
    ImageView docImagepdf;
    ImageView close_image2;
    ImageView close_image_d;
    ImageView docImage2_d;
    TextView textView;
    /*This user id is used to hold the physical user's id*/
    String user_id;
    String quantity;
    String subItem;
    String amount;
    String itSelfID;
    String fileExtension;
    boolean isFileSelected = false;
    UpdateDetailsPaymentAdapter updateDetailsAdapter;
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog.Builder alertDialogBuilderPermission;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private ArrayList<DummyUpdateDetailsModel> dummyUpdateDetailsModelArrayList = new ArrayList<>();
    private ArrayList<DataItem> dataItemArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details_financial);

        ButterKnife.bind(this);
inventoryAppSession=new InventoryAppSession(UpdateDetailsFinancialActivity.this);
        projectItemModel = (ProjectItemModel) getIntent().getExtras().getSerializable("ProjectItemModel");
        if (projectItemModel != null)
            InventoryAppSingleton.getInstance().setProjectItemModel(projectItemModel);


        System.out.println("OUTPUT======= ITEM ID====" + InventoryAppSingleton.getInstance().getItem_id());
        System.out.println("OUTPUT======= PROJECT ID====" + InventoryAppSingleton.getInstance().getProject_id());
        System.out.println("OUTPUT======= ITEM RATE====" + InventoryAppSingleton.getInstance().getItem_rate());
        System.out.println("OUTPUT=======CHART PHYSICAL PROGRESS====" + projectItemModel.getTotal_qty_done());
        System.out.println("OUTPUT=======CHART FINANCIAL PROGRESS====" + projectItemModel.getTotal_payment_done());


        init();

        setStatus();

        btnHistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UpdateDetailsFinancialActivity.this, HistoryActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("projectItemModel", projectItemModel);
                startActivity(new Intent(UpdateDetailsFinancialActivity.this, ProjectInfoOther.class).putExtras(bundle));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);


            }
        });


        httpGetItemTobePaid();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void init() {
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilderPermission = new AlertDialog.Builder(this);
        session = new InventoryAppSession(this);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
    }
    public void onResume() {
        super.onResume();
//        permissionControl();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    startActivity(new Intent(UpdateDetailsFinancialActivity.this,SignInActivity.class));
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                    permissionControl(); }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
                permissionControl();
            }


        }


    }
    private void setStatus() {
        /*Setting color in item level*/
        String itemLevelPhysicalProgress = InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel();
        String itemLevelFinancialProgress = InventoryAppSingleton.getInstance().getFinancialProgressItemlevel();
        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }

    }

    private void httpGetItemTobePaid() {
        try {
            String url = Content.baseURL + "docps?check=1&i_id=" + InventoryAppSingleton.getInstance().getItem_id();
            System.out.println("OUTPUT URL====" + url);
            CommonLoadingDialog.showLoadingDialog(UpdateDetailsFinancialActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    rate_per_unit = Double.parseDouble(InventoryAppSingleton.getInstance().getItem_rate());
                                    System.out.println("RESPONSE======" + response);
                                    Gson gson = new Gson();
                                    UnpaidResponseModel unpaidResponseModel = gson.fromJson(response, UnpaidResponseModel.class);
                                    dataItemArrayList = (ArrayList<DataItem>) unpaidResponseModel.getData();
                                    System.out.println("SIZE OF UNPAID DATA=====" + dataItemArrayList);
                                    if (dataItemArrayList.size() == 0) {
                                        LL_no_data_info.setVisibility(View.VISIBLE);
                                    } else {
                                        setPaymentList(dataItemArrayList);
                                    }

                                    try {
                                        rate_per_unit = Double.parseDouble(InventoryAppSingleton.getInstance().getItem_rate());

                                    } catch (Exception ex) {
                                        System.out.println("ERROR OUTPUT===========" + ex);
                                        rate_per_unit = 0;
                                    }
                                } else {
                                    Toast.makeText(UpdateDetailsFinancialActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                                }

                                System.out.println("RESPONSE=============" + response);

                                //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UpdateDetailsFinancialActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setPaymentList(ArrayList<DataItem> dataItemArrayList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        updateDetailsAdapter = new UpdateDetailsPaymentAdapter(UpdateDetailsFinancialActivity.this, dataItemArrayList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(updateDetailsAdapter);
    }

    @Override
    public void UpdateUI() {
        System.out.println("HELLO M CALLED");
        LL_no_data_info.setVisibility(View.VISIBLE);
    }

    @Override
    public void showPaymentUploadDialog(String itSelfID, Double amount, String user_id, String quantity, String subItem) {
        this.user_id = user_id;
        this.amount = "" + amount;
        this.quantity = quantity;
        this.subItem = subItem;
        this.itSelfID = itSelfID;

        System.out.println("PARAM====================IT_SELF_ID===" + this.itSelfID);
        System.out.println("PARAM====================AMOUNT===" + this.amount);
        System.out.println("PARAM====================QUANTITY==" + this.quantity);
        System.out.println("PARAM====================USER_ID==" + this.user_id);
        System.out.println("PARAM====================SUBITEM==" + this.subItem);

        //docUploadDialog();

        /*Execute Api without Document
        * And Payment Pay by only Admin
        * */
        //new paymentUpload().execute();
        /*
        * Execute Api for comment
        * For Financial User
        * */
        dialogPutCommentByFinancialUser();
    }

    private void dialogPutCommentByFinancialUser() {
        LayoutInflater LayoutInflater = getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_comment_financial, null);

        final EditText editCommentET = (EditText) dialogView.findViewById(R.id.editCommentET);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();


        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editCommentET.getText().toString().trim().equals("")){
                    Toast.makeText(UpdateDetailsFinancialActivity.this,"Please enter comment",Toast.LENGTH_SHORT).show();
                }else{
                    alertDialog.dismiss();
                    executePutCommentApi(editCommentET.getText().toString().trim());
                }
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    private void executePutCommentApi(final String strComment) {
            try {
                final String url = Content.PUT_COMMENT_FINANCIAL + ""+itSelfID ;
                System.out.println("OUTPUT URL====" + url);
                CommonLoadingDialog.showLoadingDialog(UpdateDetailsFinancialActivity.this, "Loading....");
                StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                CommonLoadingDialog.closeLoadingDialog();
                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    if (jsonObject.getBoolean("status") == true){
                                        String strMessage = jsonObject.getString("data");
                                        Toast.makeText(mActivity,""+strMessage,Toast.LENGTH_SHORT).show();
                                        /*Execute  Get Updated Details*/
                                        httpGetItemTobePaid();
                                    }else{
                                        Toast.makeText(mActivity,"Server Error!",Toast.LENGTH_SHORT).show();
                                    }

                                } catch (Exception ex) {
                                    CommonLoadingDialog.closeLoadingDialog();
                                    System.out.println("OUTPUT ======ERROR==" + ex);
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                System.out.println("OUTPUT ======ERROR==" + error.toString());
                                CommonLoadingDialog.closeLoadingDialog();
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("fuser_comment", strComment);
                        params.put("c_status", "10");
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {

                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/x-www-form-urlencoded");
                        return headers;

                    }
                };


                stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(UpdateDetailsFinancialActivity.this);
                stringRequest.setShouldCache(false);
                requestQueue.add(stringRequest);
            } catch (Exception ex) {
                System.out.println("ERROR======" + ex);
            }
        }



    private void docUploadDialog() {
        LayoutInflater LayoutInflater = getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_payment_certificate_upload, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        docImage = (ImageView) dialogView.findViewById(R.id.docImage);
        RL_image_container_d = (RelativeLayout) dialogView.findViewById(R.id.RL_image_container);
        close_image_d = (ImageView) dialogView.findViewById(R.id.close_image);
        docImage2_d = (ImageView) dialogView.findViewById(R.id.docImage2);
        textView = (TextView) dialogView.findViewById(R.id.textView);


        RL_doc_container = (RelativeLayout) dialogView.findViewById(R.id.RL_doc_container);
        docImagepdf = (ImageView) dialogView.findViewById(R.id.docImagepdf);
        close_image2 = (ImageView) dialogView.findViewById(R.id.close_image2);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();


        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        docImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent, CHOOSE_FILE_REQUESTCODE);
            }
        });

        close_image_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RL_image_container_d.setVisibility(View.GONE);
                docImage.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
                filePath = "";


            }
        });

        close_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RL_doc_container.setVisibility(View.GONE);
                docImage.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
                filePath = "";
            }
        });


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (isFileSelected) {
                    if (!filePath.equals("")) {
                        fileExtension = MyFileExtension.getFileExtension(fileToUpload);
                        if (fileExtension.equals("pdf") || fileExtension.equals("jpg") || fileExtension.equals("png") || fileExtension.equals("jpeg")) {
                            new paymentUpload().execute();
                        } else {
                            CommonValidationDialog.showLoadingDialog(UpdateDetailsFinancialActivity.this, "Documunt must be in pdf, jpg, jpeg or png format");
                        }

                    } else {
                        CommonValidationDialog.showLoadingDialog(UpdateDetailsFinancialActivity.this, "Please select document of jpg, jpeg, pdf or png format");
                    }
                } else {
                    CommonValidationDialog.showLoadingDialog(UpdateDetailsFinancialActivity.this, "Please select document of jpg, jpeg, pdf or png format");
                }


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                filePath = "";

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case CHOOSE_FILE_REQUESTCODE:

                try {


                    if (data != null) {
                        Uri fileUri = data.getData();

                        //System.out.println("PHOTO URI====="+fileUri);

                        if (!DocPath.getPath(getApplicationContext(), data.getData()).equals("")) {
                            fileToUpload = new File(DocPath.getPath(getApplicationContext(), data.getData()));
                            filePath = fileToUpload.getAbsolutePath();
                            fileName = fileToUpload.getName();


                            fileExtension = MyFileExtension.getFileExtension(new File(filePath));//(filePath);

                            System.out.println("OUTPUT=============" + fileExtension);

                            if (fileExtension.equals("jpg") || fileExtension.equals("png") || fileExtension.equals("jpeg")) {

                                RL_image_container_d.setVisibility(View.VISIBLE);
                                RL_doc_container.setVisibility(View.GONE);
                                docImage.setVisibility(View.GONE);
                                textView.setVisibility(View.GONE);
                                fileToUpload = new File(new CompressImage(getApplicationContext()).compressImage(fileToUpload.getAbsolutePath()));
                                System.out.println("File SIZE BEFORE UPLOAD======" + CompressImage.getReadableFileSize(fileToUpload.length()));
                                Picasso.with(getApplicationContext())
                                        .load(fileToUpload)
                                        .placeholder(R.drawable.place_holder)
                                        .resize(300, 300)
                                        .into(docImage2_d);


                            } else {

                                RL_doc_container.setVisibility(View.VISIBLE);
                                docImage.setVisibility(View.GONE);
                                textView.setVisibility(View.GONE);
                                docImagepdf.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.doc_image2));
                            }


                            isFileSelected = true;


                            System.out.println("PAYMENT PROOF FILE PATH==========" + filePath);
                            System.out.println("PAYMENT PROOF FILE NAME==========" + fileName);

                            //imageViewAddDoc.setBackground(getResources().getDrawable(R.drawable.doc_image));
                            // textFileName.setText(fileName.substring(0, fileName.lastIndexOf(".")));
                            // textFileName.setVisibility(View.VISIBLE);

                            // System.out.println("path====="+filePath);
                        }


                    } else {
                        isFileSelected = false;
                        Toast.makeText(this, "You have not choose file..", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    System.out.println("ERROR================" + ex);
                }

               /* //System.out.println("OUTPUT OF FILE PATH==="+data);
                //System.out.println("OUTPUT OF FILE PATH==="+fileToUpload.getPath());*/

                break;
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();

        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;

        float scaleHeight = ((float) newHeight) / height;

// CREATE A MATRIX FOR THE MANIPULATION

        Matrix matrix = new Matrix();

// RESIZE THE BIT MAP

        matrix.postScale(scaleWidth, scaleHeight);

// RECREATE THE NEW BITMAP

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;

    }

    private void showCustomDialog(String msg) {

        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText(msg);

        alertDialogBuilder.setView(dialogView);
        AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


    }

    private void permissionControl() {
        if (ActivityCompat.checkSelfPermission(UpdateDetailsFinancialActivity.this, permissionsRequired[0]) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(UpdateDetailsFinancialActivity.this, permissionsRequired[0])) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDetailsFinancialActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app need Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(UpdateDetailsFinancialActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(permissionsRequired[0], false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDetailsFinancialActivity.this);
                builder.setTitle("Permissions");
                builder.setMessage("This Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(UpdateDetailsFinancialActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
            }


            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();
        } else {
            //You already have the permission, just go ahead.
            proceedAfterPermission();
        }
    }

    private void proceedAfterPermission() {

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(UpdateDetailsFinancialActivity.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                proceedAfterPermission();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            //check if all permissions are granted
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                proceedAfterPermission();
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(UpdateDetailsFinancialActivity.this, permissionsRequired[0])
                    ) {
                //  txtPermissions.setText("Permissions Required");
                AlertDialog.Builder builder = new AlertDialog.Builder(UpdateDetailsFinancialActivity.this);
                builder.setTitle("Permission");
                builder.setMessage("This app needs Storage permissions.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(UpdateDetailsFinancialActivity.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class paymentUpload extends AsyncTask<String, String, String> {
        StringBuilder response = new StringBuilder();
        String stringData = "";

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            String charset = "UTF-8";
            String requestURL = Content.baseURL + "docps?check=2";

            System.out.println("URL=========================" + requestURL);
            try {

                MultipartUtility multipart = new MultipartUtility(requestURL, charset);
                multipart.addFormField("project_id", InventoryAppSingleton.getInstance().getProject_id());
                multipart.addFormField("user_id", user_id.trim());
                multipart.addFormField("iteam_id", InventoryAppSingleton.getInstance().getItem_id());
                multipart.addFormField("paid_user", "" + session.getKeyUserId());
                multipart.addFormField("qty_done", "" + quantity.trim());
                multipart.addFormField("sub_item", "" + subItem.trim());
                multipart.addFormField("amount_paid", "" + amount.trim());
//                multipart.addFilePart("pay_img", fileToUpload);

                response = multipart.finish();


                System.out.println("RESPONSE MULTIPART===" + response);

            } catch (Exception ex) {
                CommonLoadingDialog.closeLoadingDialog();
                System.out.println(" " + ex);
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonLoadingDialog.showLoadingDialog(UpdateDetailsFinancialActivity.this, "Uploading....");
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            System.out.println("POST EXECUTION");
            try {
                CommonLoadingDialog.closeLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    System.out.println("PAYMENT RESPONSE========================" + jsonObject.toString());
                    System.out.println("RESPONSE MULTIPART======" + jsonObject);
                    if (jsonObject.getBoolean("status")) {
                        startActivity(new Intent(UpdateDetailsFinancialActivity.this, HistoryActivity.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                    }
                } catch (Exception ex) {
                    System.out.println("ERROR=========1" + ex.toString());
                }

                //finish();
            } catch (Exception ex) {
                System.out.println("ERROR=========2" + ex.toString());
            }
        }


    }

    public int difftime(String string, String string2) {
        int hours;
        int min = 0;
        int days;
        long difference;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date1 = simpleDateFormat.parse(string2);
            Date date2 = simpleDateFormat.parse(string);

            difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours))
                    / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= Hours", " :: " + hours);
            return min;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return min;
    }
}