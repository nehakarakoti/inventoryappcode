package com.app.inventoryapp.model;

import java.io.Serializable;

/**
 * Created by RITUPARNA on 5/17/2018.
 */

public class ItemResponse implements Serializable
{
    private String status;

    private ItemDataModel data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ItemDataModel getData ()
    {
        return data;
    }

    public void setData (ItemDataModel data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
