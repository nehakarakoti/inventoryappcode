package com.app.inventoryapp.model;

public class ItemEditResponse
{
    private String status;

    private ItemEditData data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ItemEditData getData ()
    {
        return data;
    }

    public void setData (ItemEditData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
