package com.app.inventoryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DataIndividualOther implements Serializable
{
    private String s_user;

    private String project_name;

    private String status;

    private ArrayList<ProjectItemModel> tblIteams;

    private String created_on;

    private String inpection_cl;

    private String qty;

    private String ending_date;

    private String amount;

    private String id;

    private String project_desc;

    private String unit;

    private String starting_date;

    private String f_user;

    private String rate;

    private String updated_on;

    private String user_id;

    private String p_total_qty_done;

    private String p_total_payment_done;


    public String getS_user ()
    {
        return s_user;
    }

    public void setS_user (String s_user)
    {
        this.s_user = s_user;
    }

    public String getProject_name ()
    {
        return project_name;
    }

    public void setProject_name (String project_name)
    {
        this.project_name = project_name;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<ProjectItemModel> getTblIteams ()
    {
        return tblIteams;
    }

    public void setTblIteams (ArrayList<ProjectItemModel> tblIteams)
    {
        this.tblIteams = tblIteams;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getInpection_cl ()
    {
        return inpection_cl;
    }

    public void setInpection_cl (String inpection_cl)
    {
        this.inpection_cl = inpection_cl;
    }

    public String getQty ()
    {
        return qty;
    }

    public void setQty (String qty)
    {
        this.qty = qty;
    }

    public String getEnding_date ()
    {
        return ending_date;
    }

    public void setEnding_date (String ending_date)
    {
        this.ending_date = ending_date;
    }

    public String getAmount ()
    {
        return amount;
    }

    public void setAmount (String amount)
    {
        this.amount = amount;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getProject_desc ()
    {
        return project_desc;
    }

    public void setProject_desc (String project_desc)
    {
        this.project_desc = project_desc;
    }

    public String getUnit ()
    {
        return unit;
    }

    public void setUnit (String unit)
    {
        this.unit = unit;
    }

    public String getStarting_date ()
    {
        return starting_date;
    }

    public void setStarting_date (String starting_date)
    {
        this.starting_date = starting_date;
    }

    public String getF_user ()
    {
        return f_user;
    }

    public void setF_user (String f_user)
    {
        this.f_user = f_user;
    }

    public String getRate ()
    {
        return rate;
    }

    public void setRate (String rate)
    {
        this.rate = rate;
    }

    public String getUpdated_on ()
    {
        return updated_on;
    }

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }


    public String getP_total_qty_done() {
        return p_total_qty_done;
    }

    public void setP_total_qty_done(String p_total_qty_done) {
        this.p_total_qty_done = p_total_qty_done;
    }

    public String getP_total_payment_done() {
        return p_total_payment_done;
    }

    public void setP_total_payment_done(String p_total_payment_done) {
        this.p_total_payment_done = p_total_payment_done;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [s_user = "+s_user+", project_name = "+project_name+", status = "+status+", tblIteams = "+tblIteams+", created_on = "+created_on+", inpection_cl = "+inpection_cl+", qty = "+qty+", ending_date = "+ending_date+", amount = "+amount+", id = "+id+", project_desc = "+project_desc+", unit = "+unit+", starting_date = "+starting_date+", f_user = "+f_user+", rate = "+rate+", updated_on = "+updated_on+", user_id = "+user_id+"]";
    }
}
