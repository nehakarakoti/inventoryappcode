package com.app.inventoryapp.model;

import java.io.Serializable;

public class ProjectItemDetails implements Serializable
{

    private String s_user;
    private String total;
    private String l_rate;
    private String created_on;
    private String done_work;
    private String prop;
    private String iteam;
    private String done_payment;
    private String qty;
    private String ending_date;
    private String c_rate;
    private String id;
    private String total_payment_done;
    private String starting_date;
    private String f_user;
    private String ref;
    private String unit;
    private String in_dec;
    private String c_amount;
    private String project_id;
    private String updated_on;
    private String user_id;
    private String total_qty_done;
    private String l_amount;

    private String contractor_name;
    private String contractor_email;

    public String getContractor_name() {
        return contractor_name;
    }

    public void setContractor_name(String contractor_name) {
        this.contractor_name = contractor_name;
    }

    public String getContractor_email() {
        return contractor_email;
    }

    public void setContractor_email(String contractor_email) {
        this.contractor_email = contractor_email;
    }

    public String getS_user ()
    {
        return s_user;
    }

    public void setS_user (String s_user)
    {
        this.s_user = s_user;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getL_rate ()
    {
        return l_rate;
    }

    public void setL_rate (String l_rate)
    {
        this.l_rate = l_rate;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getDone_work ()
    {
        return done_work;
    }

    public void setDone_work (String done_work)
    {
        this.done_work = done_work;
    }

    public String getProp ()
    {
        return prop;
    }

    public void setProp (String prop)
    {
        this.prop = prop;
    }

    public String getIteam ()
    {
        return iteam;
    }

    public void setIteam (String iteam)
    {
        this.iteam = iteam;
    }

    public String getDone_payment ()
    {
        return done_payment;
    }

    public void setDone_payment (String done_payment)
    {
        this.done_payment = done_payment;
    }

    public String getQty ()
    {
        return qty;
    }

    public void setQty (String qty)
    {
        this.qty = qty;
    }

    public String getEnding_date ()
    {
        return ending_date;
    }

    public void setEnding_date (String ending_date)
    {
        this.ending_date = ending_date;
    }

    public String getC_rate ()
    {
        return c_rate;
    }

    public void setC_rate (String c_rate)
    {
        this.c_rate = c_rate;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTotal_payment_done ()
    {
        return total_payment_done;
    }

    public void setTotal_payment_done (String total_payment_done)
    {
        this.total_payment_done = total_payment_done;
    }

    public String getStarting_date ()
    {
        return starting_date;
    }

    public void setStarting_date (String starting_date)
    {
        this.starting_date = starting_date;
    }

    public String getF_user ()
    {
        return f_user;
    }

    public void setF_user (String f_user)
    {
        this.f_user = f_user;
    }

    public String getRef ()
    {
        return ref;
    }

    public void setRef (String ref)
    {
        this.ref = ref;
    }

    public String getUnit ()
    {
        return unit;
    }

    public void setUnit (String unit)
    {
        this.unit = unit;
    }

    public String getIn_dec ()
    {
        return in_dec;
    }

    public void setIn_dec (String in_dec)
    {
        this.in_dec = in_dec;
    }

    public String getC_amount ()
    {
        return c_amount;
    }

    public void setC_amount (String c_amount)
    {
        this.c_amount = c_amount;
    }

    public String getProject_id ()
    {
        return project_id;
    }

    public void setProject_id (String project_id)
    {
        this.project_id = project_id;
    }

    public String getUpdated_on ()
    {
        return updated_on;
    }

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getTotal_qty_done ()
    {
        return total_qty_done;
    }

    public void setTotal_qty_done (String total_qty_done)
    {
        this.total_qty_done = total_qty_done;
    }

    public String getL_amount ()
    {
        return l_amount;
    }

    public void setL_amount (String l_amount)
    {
        this.l_amount = l_amount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [s_user = "+s_user+", total = "+total+", l_rate = "+l_rate+", created_on = "+created_on+", done_work = "+done_work+", prop = "+prop+", iteam = "+iteam+", done_payment = "+done_payment+", qty = "+qty+", ending_date = "+ending_date+", c_rate = "+c_rate+", id = "+id+", total_payment_done = "+total_payment_done+", starting_date = "+starting_date+", f_user = "+f_user+", ref = "+ref+", unit = "+unit+", in_dec = "+in_dec+", c_amount = "+c_amount+", project_id = "+project_id+", updated_on = "+updated_on+", user_id = "+user_id+", total_qty_done = "+total_qty_done+", l_amount = "+l_amount+"]";
    }
}
