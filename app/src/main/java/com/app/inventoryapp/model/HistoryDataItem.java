package com.app.inventoryapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryDataItem
{@SerializedName("cancelled_by")
    private int cancelled_by;
@SerializedName("approved_by")
private int approved_by;
    @SerializedName("edit_count")
    private int edit_count;
    @SerializedName("editable")
 private int   editable;

    public String getWork_type() {
        return work_type;
    }

    public void setWork_type(String work_type) {
        this.work_type = work_type;
    }

    public String getWork_remark() {
        return work_remark;
    }

    public void setWork_remark(String work_remark) {
        this.work_remark = work_remark;
    }

    public int getCancelled_by() {
        return cancelled_by;
    }

    public void setCancelled_by(int cancelled_by) {
        this.cancelled_by = cancelled_by;
    }

    public int getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(int approved_by) {
        this.approved_by = approved_by;
    }

    public int getEdit_count() {
        return edit_count;
    }

    public void setEdit_count(int edit_count) {
        this.edit_count = edit_count;
    }

    public int getEditable() {
        return editable;
    }

    public void setEditable(int editable) {
        this.editable = editable;
    }

    @SerializedName("qty_done")
    private String qtyDone;

    @SerializedName("work_type")
    private String work_type;
    @SerializedName("work_remark")
    private String work_remark;
    @SerializedName("updated_on")
    private String updatedOn;

    @SerializedName("pay_status")
    private int payStatus;

    @SerializedName("doc_name")
    private String docName;

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("project_id")
    private int projectId;

    @SerializedName("iteam_id")
    private int iteamId;

    @SerializedName("user_id")
    private int userId;

    @SerializedName("created_on")
    private String createdOn;

    @SerializedName("fuser_comment")
    private String fuserComment;

    @SerializedName("payment")
    private List<HistoryPayment> payment;

    @SerializedName("id")
    private int id;




    @SerializedName("user")
    private History_user user;




    public String getFuserComment() {
        return fuserComment;
    }

    public void setFuserComment(String fuserComment) {
        this.fuserComment = fuserComment;
    }

    public void setQtyDone(String qtyDone){
        this.qtyDone = qtyDone;
    }

    public String getQtyDone(){
        return qtyDone;
    }

    public void setUpdatedOn(String updatedOn){
        this.updatedOn = updatedOn;
    }

    public String getUpdatedOn(){
        return updatedOn;
    }

    public void setPayStatus(int payStatus){
        this.payStatus = payStatus;
    }

    public int getPayStatus(){
        return payStatus;
    }

    public void setDocName(String docName){
        this.docName = docName;
    }

    public String getDocName(){
        return docName;
    }

    public void setProjectId(int projectId){
        this.projectId = projectId;
    }

    public int getProjectId(){
        return projectId;
    }

    public void setIteamId(int iteamId){
        this.iteamId = iteamId;
    }

    public int getIteamId(){
        return iteamId;
    }

    public void setUserId(int userId){
        this.userId = userId;
    }

    public int getUserId(){
        return userId;
    }

    public void setCreatedOn(String createdOn){
        this.createdOn = createdOn;
    }

    public String getCreatedOn(){
        return createdOn;
    }

    public void setPayment(List<HistoryPayment> payment){
        this.payment = payment;
    }

    public List<HistoryPayment> getPayment(){
        return payment;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setUser(History_user user){
        this.user = user;
    }

    public History_user getUser(){
        return user;
    }

    @Override
    public String toString(){
        return
                "DataItem{" +
                        "qty_done = '" + qtyDone + '\'' +
                        ",updated_on = '" + updatedOn + '\'' +
                        ",pay_status = '" + payStatus + '\'' +
                        ",doc_name = '" + docName + '\'' +
                        ",project_id = '" + projectId + '\'' +
                        ",iteam_id = '" + iteamId + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",created_on = '" + createdOn + '\'' +
                        ",payment = '" + payment + '\'' +
                        ",id = '" + id + '\'' +
                        ",user = '" + user + '\'' +
                        "}";
    }
}
