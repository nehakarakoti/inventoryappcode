package com.app.inventoryapp.model;

import com.google.gson.annotations.SerializedName;

public class History_user
{


    @SerializedName("updated_on")
    private String updatedOn;

    @SerializedName("authKey")
    private String authKey;

    @SerializedName("role")
    private String role;

    @SerializedName("usertype")
    private String usertype;

    @SerializedName("lastvisit")
    private String lastvisit;

    @SerializedName("cellno")
    private String cellno;

    @SerializedName("created_on")
    private String createdOn;

    @SerializedName("device_token")
    private String deviceToken;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("user_location")
    private String userLocation;

    @SerializedName("supervisor")
    private int supervisor;

    @SerializedName("superuser")
    private int superuser;

    @SerializedName("username")
    private String username;

    @SerializedName("status")
    private int status;


    public void setUpdatedOn(String updatedOn){
        this.updatedOn = updatedOn;
    }

    public String getUpdatedOn(){
        return updatedOn;
    }

    public void setAuthKey(String authKey){
        this.authKey = authKey;
    }

    public String getAuthKey(){
        return authKey;
    }

    public void setRole(String role){
        this.role = role;
    }

    public String getRole(){
        return role;
    }

    public void setUsertype(String usertype){
        this.usertype = usertype;
    }

    public String getUsertype(){
        return usertype;
    }

    public void setLastvisit(String lastvisit){
        this.lastvisit = lastvisit;
    }

    public String getLastvisit(){
        return lastvisit;
    }

    public void setCellno(String cellno){
        this.cellno = cellno;
    }

    public String getCellno(){
        return cellno;
    }

    public void setCreatedOn(String createdOn){
        this.createdOn = createdOn;
    }

    public String getCreatedOn(){
        return createdOn;
    }

    public void setDeviceToken(String deviceToken){
        this.deviceToken = deviceToken;
    }

    public String getDeviceToken(){
        return deviceToken;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setUserLocation(String userLocation){
        this.userLocation = userLocation;
    }

    public String getUserLocation(){
        return userLocation;
    }

    public void setSupervisor(int supervisor){
        this.supervisor = supervisor;
    }

    public int getSupervisor(){
        return supervisor;
    }

    public void setSuperuser(int superuser){
        this.superuser = superuser;
    }

    public int getSuperuser(){
        return superuser;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return username;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "User{" +
                        "updated_on = '" + updatedOn + '\'' +
                        ",authKey = '" + authKey + '\'' +
                        ",role = '" + role + '\'' +
                        ",usertype = '" + usertype + '\'' +
                        ",lastvisit = '" + lastvisit + '\'' +
                        ",cellno = '" + cellno + '\'' +
                        ",created_on = '" + createdOn + '\'' +
                        ",device_token = '" + deviceToken + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",user_location = '" + userLocation + '\'' +
                        ",supervisor = '" + supervisor + '\'' +
                        ",superuser = '" + superuser + '\'' +
                        ",username = '" + username + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }


}
