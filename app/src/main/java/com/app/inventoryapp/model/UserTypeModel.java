package com.app.inventoryapp.model;

/**
 * Created by RITUPARNA on 5/4/2018.
 */

public class UserTypeModel
{
    private String id;

    private String user_name;

    private String created_on;

    private String updated_on;

    private String type_code;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUser_name ()
    {
        return user_name;
    }

    public void setUser_name (String user_name)
    {
        this.user_name = user_name;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getUpdated_on ()
    {
        return updated_on;
    }

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getType_code ()
    {
        return type_code;
    }

    public void setType_code (String type_code)
    {
        this.type_code = type_code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", user_name = "+user_name+", created_on = "+created_on+", updated_on = "+updated_on+", type_code = "+type_code+"]";
    }
}
