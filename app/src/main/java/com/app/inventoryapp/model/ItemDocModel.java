package com.app.inventoryapp.model;

import java.io.Serializable;

/**
 * Created by RITUPARNA on 5/17/2018.
 */

public class ItemDocModel implements Serializable
{
    private String id;

    private String doc_name;

    private String created_on;

    private String project_id;

    private String updated_on;

    private String iteam_id;

    private String user_id;

    private String qty_done;



    private String pay_status;

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }

    public String getQty_done()
    {
        return qty_done;
    }

    public void setQty_done(String qty_done)
    {
        this.qty_done = qty_done;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDoc_name ()
    {
        return doc_name;
    }

    public void setDoc_name (String doc_name)
    {
        this.doc_name = doc_name;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getProject_id ()
    {
        return project_id;
    }

    public void setProject_id (String project_id)
    {
        this.project_id = project_id;
    }

    public String getUpdated_on ()
{
    return updated_on;
}

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getIteam_id ()
    {
        return iteam_id;
    }

    public void setIteam_id (String iteam_id)
    {
        this.iteam_id = iteam_id;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", doc_name = "+doc_name+", created_on = "+created_on+", project_id = "+project_id+", updated_on = "+updated_on+", iteam_id = "+iteam_id+", user_id = "+user_id+"]";
    }
}
