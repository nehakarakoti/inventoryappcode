package com.app.inventoryapp.model;

/**
 * Created by RITUPARNA on 5/11/2018.
 */

public class DummyUpdateDetailsModel
{
    String date;
    String quantity;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
