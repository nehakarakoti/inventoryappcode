package com.app.inventoryapp.model;

import java.util.ArrayList;

public class NotificationData
{
    private String id;

    private String massage;

    private String user_role;

    private String activity_for;

    private String status;

    private String created_on;

    private String item_id;

    private String doc_id;

    private String project_id;

    private String activity_by;

    private String activity_name;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getMassage ()
    {
        return massage;
    }

    public void setMassage (String massage)
    {
        this.massage = massage;
    }

    public String getUser_role ()
    {
        return user_role;
    }

    public void setUser_role (String user_role)
    {
        this.user_role = user_role;
    }

    public String getActivity_for ()
    {
        return activity_for;
    }

    public void setActivity_for (String activity_for)
    {
        this.activity_for = activity_for;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getItem_id ()
    {
        return item_id;
    }

    public void setItem_id (String item_id)
    {
        this.item_id = item_id;
    }

    public String getDoc_id ()
    {
        return doc_id;
    }

    public void setDoc_id (String doc_id)
    {
        this.doc_id = doc_id;
    }

    public String getProject_id ()
    {
        return project_id;
    }

    public void setProject_id (String project_id)
    {
        this.project_id = project_id;
    }

    public String getActivity_by ()
    {
        return activity_by;
    }

    public void setActivity_by (String activity_by)
    {
        this.activity_by = activity_by;
    }

    public String getActivity_name ()
    {
        return activity_name;
    }

    public void setActivity_name (String activity_name)
    {
        this.activity_name = activity_name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", massage = "+massage+", user_role = "+user_role+", activity_for = "+activity_for+", status = "+status+", created_on = "+created_on+", item_id = "+item_id+", doc_id = "+doc_id+", project_id = "+project_id+", activity_by = "+activity_by+", activity_name = "+activity_name+"]";
    }
}
