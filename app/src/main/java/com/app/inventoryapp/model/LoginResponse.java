package com.app.inventoryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/3/2018.
 */

public class LoginResponse implements Serializable
{
    private String status;

    private ArrayList<LoginData> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<LoginData> getData ()
    {
        return data;
    }

    public void setData (ArrayList<LoginData> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
