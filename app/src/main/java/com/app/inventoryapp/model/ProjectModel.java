package com.app.inventoryapp.model;

/**
 * Created by android-da on 5/1/18.
 */

public class ProjectModel
{
    private String project_name;
    private String projectDate;

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProjectDate() {
        return projectDate;
    }

    public void setProjectDate(String projectDate) {
        this.projectDate = projectDate;
    }
}
