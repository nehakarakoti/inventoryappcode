package com.app.inventoryapp.model;

import java.io.Serializable;

public class ResponseProjectIndividual implements Serializable
{
    private String status;

    private DataIndividual data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public DataIndividual getData ()
    {
        return data;
    }

    public void setData (DataIndividual data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
