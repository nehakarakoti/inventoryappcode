package com.app.inventoryapp.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class DataItem implements Serializable{

	@SerializedName("qty_done")
	private String qtyDone;

	@SerializedName("updated_on")
	private String updatedOn;

	@SerializedName("pay_status")
	private int payStatus;

	@SerializedName("doc_name")
	private String docName;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("project_id")
	private int projectId;

	@SerializedName("iteam_id")
	private int iteamId;

	@SerializedName("created_on")
	private String createdOn;

	@SerializedName("fuser_comment")
	private String fuser_comment;

	@SerializedName("id")
	private int id;

	public String getFuser_comment() {
		return fuser_comment;
	}

	public void setFuser_comment(String fuser_comment) {
		this.fuser_comment = fuser_comment;
	}

	public void setQtyDone(String qtyDone){
		this.qtyDone = qtyDone;
	}

	public String getQtyDone(){
		return qtyDone;
	}

	public void setUpdatedOn(String updatedOn){
		this.updatedOn = updatedOn;
	}

	public String getUpdatedOn(){
		return updatedOn;
	}

	public void setPayStatus(int payStatus){
		this.payStatus = payStatus;
	}

	public int getPayStatus(){
		return payStatus;
	}

	public void setDocName(String docName){
		this.docName = docName;
	}

	public String getDocName(){
		return docName;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setProjectId(int projectId){
		this.projectId = projectId;
	}

	public int getProjectId(){
		return projectId;
	}

	public void setIteamId(int iteamId){
		this.iteamId = iteamId;
	}

	public int getIteamId(){
		return iteamId;
	}

	public void setCreatedOn(String createdOn){
		this.createdOn = createdOn;
	}

	public String getCreatedOn(){
		return createdOn;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"qty_done = '" + qtyDone + '\'' + 
			",updated_on = '" + updatedOn + '\'' + 
			",pay_status = '" + payStatus + '\'' + 
			",doc_name = '" + docName + '\'' + 
			",user_id = '" + userId + '\'' + 
			",project_id = '" + projectId + '\'' + 
			",iteam_id = '" + iteamId + '\'' + 
			",created_on = '" + createdOn + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}