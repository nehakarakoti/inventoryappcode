package com.app.inventoryapp.model;

import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/3/2018.
 */

public class AddUserErrorResponse
{
    private String status;

    private ArrayList<ErrorData> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<ErrorData> getData ()
    {
        return data;
    }

    public void setData (ArrayList<ErrorData> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
