package com.app.inventoryapp.model;

import java.io.Serializable;

/**
 * Created by android-da on 12/6/18.
 */

public class ReviewHistoryModel implements Serializable{

    String id = "";
    String activity_name = "";
    String massage = "";
    String activity_by = "";
    String activity_for = "";
    String user_role = "";
    String project_id = "";
    String item_id = "";
    String doc_id = "";
    String status = "";
    String created_on = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivity_name() {
        return activity_name;
    }

    public void setActivity_name(String activity_name) {
        this.activity_name = activity_name;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getActivity_by() {
        return activity_by;
    }

    public void setActivity_by(String activity_by) {
        this.activity_by = activity_by;
    }

    public String getActivity_for() {
        return activity_for;
    }

    public void setActivity_for(String activity_for) {
        this.activity_for = activity_for;
    }

    public String getUser_role() {
        return user_role;
    }

    public void setUser_role(String user_role) {
        this.user_role = user_role;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}
