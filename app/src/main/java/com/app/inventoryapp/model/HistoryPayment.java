package com.app.inventoryapp.model;

import com.google.gson.annotations.SerializedName;

public class HistoryPayment
{







    @SerializedName("qty_done")
    private String qtyDone;

    @SerializedName("updated_on")
    private Object updatedOn;

    @SerializedName("amount_paid")
    private String amountPaid;

    @SerializedName("project_id")
    private int projectId;

    @SerializedName("user_id")
    private int userId;

    @SerializedName("iteam_id")
    private int iteamId;

    @SerializedName("sub_item")
    private int subItem;

    @SerializedName("created_on")
    private String createdOn;

    @SerializedName("paid_user")
    private int paidUser;

    @SerializedName("id")
    private int id;

    @SerializedName("paid_name")
    private String paidName;

    @SerializedName("paid_date")
    private String paidDate;


    @SerializedName("pay_img")
    private String pay_img;

    public String getPay_img() {
        return pay_img;
    }

    public void setPay_img(String pay_img) {
        this.pay_img = pay_img;
    }

    public void setQtyDone(String qtyDone){
        this.qtyDone = qtyDone;
    }

    public String getQtyDone(){
        return qtyDone;
    }

    public void setUpdatedOn(Object updatedOn){
        this.updatedOn = updatedOn;
    }

    public Object getUpdatedOn(){
        return updatedOn;
    }

    public void setAmountPaid(String amountPaid){
        this.amountPaid = amountPaid;
    }

    public String getAmountPaid(){
        return amountPaid;
    }

    public void setProjectId(int projectId){
        this.projectId = projectId;
    }

    public int getProjectId(){
        return projectId;
    }

    public void setUserId(int userId){
        this.userId = userId;
    }

    public int getUserId(){
        return userId;
    }

    public void setIteamId(int iteamId){
        this.iteamId = iteamId;
    }

    public int getIteamId(){
        return iteamId;
    }

    public void setSubItem(int subItem){
        this.subItem = subItem;
    }

    public int getSubItem(){
        return subItem;
    }

    public void setCreatedOn(String createdOn){
        this.createdOn = createdOn;
    }

    public String getCreatedOn(){
        return createdOn;
    }

    public void setPaidUser(int paidUser){
        this.paidUser = paidUser;
    }

    public int getPaidUser(){
        return paidUser;
    }

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setPaidName(String paidName){
        this.paidName = paidName;
    }

    public String getPaidName(){
        return paidName;
    }

    public void setPaidDate(String paidDate){
        this.paidDate = paidDate;
    }

    public String getPaidDate(){
        return paidDate;
    }

    @Override
    public String toString(){
        return
                "PaymentItem{" +
                        "qty_done = '" + qtyDone + '\'' +
                        ",updated_on = '" + updatedOn + '\'' +
                        ",amount_paid = '" + amountPaid + '\'' +
                        ",project_id = '" + projectId + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",iteam_id = '" + iteamId + '\'' +
                        ",sub_item = '" + subItem + '\'' +
                        ",created_on = '" + createdOn + '\'' +
                        ",paid_user = '" + paidUser + '\'' +
                        ",id = '" + id + '\'' +
                        ",paid_name = '" + paidName + '\'' +
                        ",paid_date = '" + paidDate + '\'' +
                        ",pay_img = '" + pay_img + '\'' +
                        "}";
    }
}
