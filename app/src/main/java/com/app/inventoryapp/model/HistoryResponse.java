package com.app.inventoryapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HistoryResponse
{


    @SerializedName("data")
    private List<HistoryDataItem> data;

    @SerializedName("status")
    private boolean status;

    public void setData(List<HistoryDataItem> data){
        this.data = data;
    }

    public List<HistoryDataItem> getData(){
        return data;
    }

    public void setStatus(boolean status){
        this.status = status;
    }

    public boolean isStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "Response{" +
                        "data = '" + data + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}
