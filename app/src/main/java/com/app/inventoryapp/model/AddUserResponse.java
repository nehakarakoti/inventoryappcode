package com.app.inventoryapp.model;

import java.io.Serializable;

/**
 * Created by RITUPARNA on 5/3/2018.
 */

public class AddUserResponse implements Serializable
{
    private String status;

    private UsersResponse data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public UsersResponse getData ()
    {
        return data;
    }

    public void setData (UsersResponse data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
