package com.app.inventoryapp.model;

import java.util.ArrayList;

public class NotificationResponse
{
    private String status;

    private ArrayList<NotificationData> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<NotificationData> getData ()
    {
        return data;
    }

    public void setData (ArrayList<NotificationData> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
