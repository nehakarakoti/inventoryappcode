package com.app.inventoryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserResponseModel implements Serializable
{
    private String status;

    private ArrayList<UserDataModel> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<UserDataModel> getData ()
    {
        return data;
    }

    public void setData (ArrayList<UserDataModel> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
