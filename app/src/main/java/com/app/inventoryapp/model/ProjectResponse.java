package com.app.inventoryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/8/2018.
 */

public class ProjectResponse implements Serializable
{
    private String status;

    private ArrayList<ProjectDataModel> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ArrayList<ProjectDataModel> getData ()
    {
        return data;
    }

    public void setData (ArrayList<ProjectDataModel> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
