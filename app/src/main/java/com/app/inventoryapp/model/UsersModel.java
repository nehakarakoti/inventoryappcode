package com.app.inventoryapp.model;

/**
 * Created by dharmaniz on 2/5/18.
 */

public class UsersModel {
    private String userInfo;

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

}
