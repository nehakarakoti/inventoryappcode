package com.app.inventoryapp.model;

/**
 * Created by android-da on 5/2/18.
 */

public class ProjectDetailModel
{
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
