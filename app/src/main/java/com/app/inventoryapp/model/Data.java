package com.app.inventoryapp.model;


import com.google.gson.annotations.SerializedName;


public class Data
{

	@SerializedName("updated_on")
	private String updatedOn;

	@SerializedName("done_payment")
	private String donePayment;

	@SerializedName("l_amount")
	private String lAmount;

	@SerializedName("c_rate")
	private String cRate;

	@SerializedName("starting_date")
	private String startingDate;

	@SerializedName("l_rate")
	private String lRate;

	@SerializedName("c_amount")
	private String cAmount;

	@SerializedName("in_dec")
	private String inDec;

	@SerializedName("total_payment_done")
	private String totalPaymentDone;

	@SerializedName("unit")
	private String unit;

	@SerializedName("ref")
	private String ref;

	@SerializedName("total")
	private String total;

	@SerializedName("total_qty_done")
	private String totalQtyDone;

	@SerializedName("project_id")
	private int projectId;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("created_on")
	private String createdOn;

	@SerializedName("qty")
	private String qty;

	@SerializedName("prop")
	private String prop;

	@SerializedName("done_work")
	private String doneWork;

	@SerializedName("s_user")
	private String sUser;

	@SerializedName("id")
	private int id;

	@SerializedName("iteam")
	private String iteam;

	@SerializedName("f_user")
	private String fUser;

	@SerializedName("ending_date")
	private String endingDate;

	public void setUpdatedOn(String updatedOn){
		this.updatedOn = updatedOn;
	}

	public String getUpdatedOn(){
		return updatedOn;
	}

	public void setDonePayment(String donePayment){
		this.donePayment = donePayment;
	}

	public String getDonePayment(){
		return donePayment;
	}

	public void setLAmount(String lAmount){
		this.lAmount = lAmount;
	}

	public String getLAmount(){
		return lAmount;
	}

	public void setCRate(String cRate){
		this.cRate = cRate;
	}

	public String getCRate(){
		return cRate;
	}

	public void setStartingDate(String startingDate){
		this.startingDate = startingDate;
	}

	public String getStartingDate(){
		return startingDate;
	}

	public void setLRate(String lRate){
		this.lRate = lRate;
	}

	public String getLRate(){
		return lRate;
	}

	public void setCAmount(String cAmount){
		this.cAmount = cAmount;
	}

	public String getCAmount(){
		return cAmount;
	}

	public void setInDec(String inDec){
		this.inDec = inDec;
	}

	public String getInDec(){
		return inDec;
	}

	public void setTotalPaymentDone(String totalPaymentDone){
		this.totalPaymentDone = totalPaymentDone;
	}

	public String getTotalPaymentDone(){
		return totalPaymentDone;
	}

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setRef(String ref){
		this.ref = ref;
	}

	public String getRef(){
		return ref;
	}

	public void setTotal(String total){
		this.total = total;
	}

	public String getTotal(){
		return total;
	}

	public void setTotalQtyDone(String totalQtyDone){
		this.totalQtyDone = totalQtyDone;
	}

	public String getTotalQtyDone(){
		return totalQtyDone;
	}

	public void setProjectId(int projectId){
		this.projectId = projectId;
	}

	public int getProjectId(){
		return projectId;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setCreatedOn(String createdOn){
		this.createdOn = createdOn;
	}

	public String getCreatedOn(){
		return createdOn;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setProp(String prop){
		this.prop = prop;
	}

	public String getProp(){
		return prop;
	}

	public void setDoneWork(String doneWork){
		this.doneWork = doneWork;
	}

	public String getDoneWork(){
		return doneWork;
	}

	public void setSUser(String sUser){
		this.sUser = sUser;
	}

	public String getSUser(){
		return sUser;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIteam(String iteam){
		this.iteam = iteam;
	}

	public String getIteam(){
		return iteam;
	}

	public void setFUser(String fUser){
		this.fUser = fUser;
	}

	public String getFUser(){
		return fUser;
	}

	public void setEndingDate(String endingDate){
		this.endingDate = endingDate;
	}

	public String getEndingDate(){
		return endingDate;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"updated_on = '" + updatedOn + '\'' + 
			",done_payment = '" + donePayment + '\'' + 
			",l_amount = '" + lAmount + '\'' + 
			",c_rate = '" + cRate + '\'' + 
			",starting_date = '" + startingDate + '\'' + 
			",l_rate = '" + lRate + '\'' + 
			",c_amount = '" + cAmount + '\'' + 
			",in_dec = '" + inDec + '\'' + 
			",total_payment_done = '" + totalPaymentDone + '\'' + 
			",unit = '" + unit + '\'' + 
			",ref = '" + ref + '\'' + 
			",total = '" + total + '\'' + 
			",total_qty_done = '" + totalQtyDone + '\'' + 
			",project_id = '" + projectId + '\'' + 
			",user_id = '" + userId + '\'' + 
			",created_on = '" + createdOn + '\'' + 
			",qty = '" + qty + '\'' + 
			",prop = '" + prop + '\'' + 
			",done_work = '" + doneWork + '\'' + 
			",s_user = '" + sUser + '\'' + 
			",id = '" + id + '\'' + 
			",iteam = '" + iteam + '\'' + 
			",f_user = '" + fUser + '\'' + 
			",ending_date = '" + endingDate + '\'' + 
			"}";
		}
}