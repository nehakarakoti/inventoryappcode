package com.app.inventoryapp.model;

import com.google.gson.annotations.SerializedName;

public class PaymentInfoModel
{

   @SerializedName("qty_done")
   private String qtyDone;

   @SerializedName("updated_on")
   private String updatedOn;

   @SerializedName("pay_status")
   private int payStatus;

   @SerializedName("doc_name")
   private String docName;

   @SerializedName("user_id")
   private int userId;

   @SerializedName("project_id")
   private int projectId;

   @SerializedName("iteam_id")
   private int iteamId;

   @SerializedName("created_on")
   private String createdOn;

   @SerializedName("id")
   private int id;




   public String getQtyDone() {
      return qtyDone;
   }

   public void setQtyDone(String qtyDone) {
      this.qtyDone = qtyDone;
   }

   public String getUpdatedOn() {
      return updatedOn;
   }

   public void setUpdatedOn(String updatedOn) {
      this.updatedOn = updatedOn;
   }

   public int getPayStatus() {
      return payStatus;
   }

   public void setPayStatus(int payStatus) {
      this.payStatus = payStatus;
   }

   public String getDocName() {
      return docName;
   }

   public void setDocName(String docName) {
      this.docName = docName;
   }

   public int getUserId() {
      return userId;
   }

   public void setUserId(int userId) {
      this.userId = userId;
   }

   public int getProjectId() {
      return projectId;
   }

   public void setProjectId(int projectId) {
      this.projectId = projectId;
   }

   public int getIteamId() {
      return iteamId;
   }

   public void setIteamId(int iteamId) {
      this.iteamId = iteamId;
   }

   public String getCreatedOn() {
      return createdOn;
   }

   public void setCreatedOn(String createdOn) {
      this.createdOn = createdOn;
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }
}
