package com.app.inventoryapp.model;

import java.io.Serializable;

/**
 * Created by RITUPARNA on 5/3/2018.
 */

public class LoginData implements Serializable
{
    private String lastvisit;

    private String authKey;

    private String status;

    private String created_on;

    private String user_location;

    private String usertype;

    private String password;

    private String cellno;

    private String id;

    private String username;

    private String email;

    private String device_token;

    private String supervisor;

    private String name;

    private String updated_on;

    private String role;

    private String superuser;

    public String getLastvisit ()
    {
        return lastvisit;
    }

    public void setLastvisit (String lastvisit)
    {
        this.lastvisit = lastvisit;
    }

    public String getAuthKey ()
    {
        return authKey;
    }

    public void setAuthKey (String authKey)
    {
        this.authKey = authKey;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getUser_location ()
    {
        return user_location;
    }

    public void setUser_location (String user_location)
    {
        this.user_location = user_location;
    }

    public String getUsertype ()
    {
        return usertype;
    }

    public void setUsertype (String usertype)
    {
        this.usertype = usertype;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getCellno ()
    {
        return cellno;
    }

    public void setCellno (String cellno)
    {
        this.cellno = cellno;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getDevice_token ()
    {
        return device_token;
    }

    public void setDevice_token (String device_token)
    {
        this.device_token = device_token;
    }

    public String getSupervisor ()
    {
        return supervisor;
    }

    public void setSupervisor (String supervisor)
    {
        this.supervisor = supervisor;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUpdated_on ()
    {
        return updated_on;
    }

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getRole ()
    {
        return role;
    }

    public void setRole (String role)
    {
        this.role = role;
    }

    public String getSuperuser ()
    {
        return superuser;
    }

    public void setSuperuser (String superuser)
    {
        this.superuser = superuser;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lastvisit = "+lastvisit+", authKey = "+authKey+", status = "+status+", created_on = "+created_on+", user_location = "+user_location+", usertype = "+usertype+", password = "+password+", cellno = "+cellno+", id = "+id+", username = "+username+", email = "+email+", device_token = "+device_token+", supervisor = "+supervisor+", name = "+name+", updated_on = "+updated_on+", role = "+role+", superuser = "+superuser+"]";
    }
}
