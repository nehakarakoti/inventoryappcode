package com.app.inventoryapp.model;

import java.io.Serializable;

public class ResponseProjectIndividualOther implements Serializable
{
    private String status;

    private DataIndividualOther data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public DataIndividualOther getData ()
    {
        return data;
    }

    public void setData (DataIndividualOther data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
