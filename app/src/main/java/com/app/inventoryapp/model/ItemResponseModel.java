package com.app.inventoryapp.model;

import java.io.Serializable;

public class ItemResponseModel implements Serializable
{
    private String status;

    private ItemsDataModel data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public ItemsDataModel getData ()
    {
        return data;
    }

    public void setData (ItemsDataModel data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
}
