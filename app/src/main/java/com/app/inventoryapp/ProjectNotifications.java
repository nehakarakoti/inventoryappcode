package com.app.inventoryapp;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.NotificationAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.DataIndividual;
import com.app.inventoryapp.model.NotificationData;
import com.app.inventoryapp.model.NotificationResponse;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.model.ResponseProjectIndividual;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectNotifications extends BaseActivity {


    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.view)
    View view;


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressbar)
    ProgressBar progressbar;

    InventoryAppSession session;


    private boolean itShouldLoadMore = true;
    private int pageNo = 1;


    ArrayList<NotificationData> notificationDataArrayList=new ArrayList<>();

    NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_notifications);

        ButterKnife.bind(this);
        session=new InventoryAppSession(this);

        System.out.println("USER ID==="+session.getKeyUserId());
        
        setStatus();


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        getNotification();




        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                    if (itShouldLoadMore) {
                     //   loadMoreHttpAdminProject();
                        //System.out.println("ITS TIME TO HIT API");
                        getMoreNotification();
                    } else {

                    }
                }
            }
        });

    }

    private  void getMoreNotification()
    {
        try {

            //  http://dharmani.com/railway_api/web/site/getactivities

            //   http://dharmani.com/railway_api/web/activities?sort=-id&activity_for=19&status=0
            pageNo=pageNo+1;

            String url= Content.baseURL+"activities?sort=-id&activity_for="+session.getKeyUserId()+"project_id="+ InventoryAppSingleton.getInstance().getProject_id()+"&status=0&page="+pageNo;


            //CommonLoadingDialog.showLoadingDialog(ProjectNotifications.this, "Loading....");

            progressbar.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url ,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            Log.d("RESPONSE=",response);
                            progressbar.setVisibility(View.GONE);
                            try {

                                Gson gson=new Gson();
                                NotificationResponse notificationResponse=gson.fromJson(response,NotificationResponse.class);
                                if (notificationResponse.getData().size() != 0)
                                {
                                    for (int i = 0; i < notificationResponse.getData().size(); i++) {

                                        NotificationData notificationData=notificationResponse.getData().get(i);
                                        notificationDataArrayList.add(notificationData);

                                    }

                                    notificationAdapter.updateList(notificationDataArrayList);
                                    //projectListAdapter.updateAdapter(projectDataModelArrayList);
                                } else {
                                    itShouldLoadMore = false;
                                    Toast.makeText(ProjectNotifications.this, "No more data to pull", Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });



            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectNotifications.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void getNotification()
    {
        try {

          //  http://dharmani.com/railway_api/web/site/getactivities

         //   http://dharmani.com/railway_api/web/activities?sort=-id&activity_for=19&status=0
            String url= Content.baseURL+"activities?sort=-id&activity_for="+session.getKeyUserId()+"project_id="+ InventoryAppSingleton.getInstance().getProject_id()+"&status=0&page="+pageNo;

            CommonLoadingDialog.showLoadingDialog(ProjectNotifications.this, "Loading....");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url ,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            Log.d("RESPONSE=",response);

                            try {

                                    Gson gson=new Gson();
                                    NotificationResponse notificationResponse=gson.fromJson(response,NotificationResponse.class);
                                    if(notificationResponse.getData().size()>0)
                                    {

                                        if(notificationResponse.getData().size()<20)
                                        {
                                            itShouldLoadMore=false;
                                        }
                                        notificationDataArrayList=notificationResponse.getData();
                                        setNotificationList(notificationDataArrayList);


                                    }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });



            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectNotifications.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setNotificationList(ArrayList<NotificationData> notificationDataArrayList)
    {
        notificationAdapter=new NotificationAdapter(ProjectNotifications.this, notificationDataArrayList);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(notificationAdapter);
    }

    private void setStatus()
    {


        long diff= InventoryAppSingleton.getInstance().getDiff();
        if(diff>=0)
        {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        }
        else
        {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ProjectNotifications.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }


    }
}
