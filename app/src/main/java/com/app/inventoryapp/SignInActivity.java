package com.app.inventoryapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.model.LoginResponse;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.app.inventoryapp.utils.EmailValidator;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SignInActivity extends BaseActivity {

    @BindView(R.id.text_sign_up)
    TextView text_sign_up;

    @BindView(R.id.text_error)
    TextView text_error;

    @BindView(R.id.input_password)
    EditText input_password;

    @BindView(R.id.input_user_id)
    EditText input_user_id;


    @BindView(R.id.LL_errorSection)
    LinearLayout LL_errorSection;
    String strData;
    private InventoryAppSession inventoryAppSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        ButterKnife.bind(this);
        inventoryAppSession = new InventoryAppSession(SignInActivity.this);

        if (inventoryAppSession.isIsLogin() == true) {

            if (inventoryAppSession.getKeyUserType().equals("Admin")) {
                startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                finish();
            } else {
                startActivity(new Intent(SignInActivity.this, HomeActivityOther.class));
                finish();
            }

        } else {
            onCreateFunction();
        }

    }

    private void onCreateFunction() {
        //getData();

        System.out.println("OUTPUT=============TOKEN" + FirebaseInstanceId.getInstance().getToken());

        /*startActivity(new Intent(SignInActivity.this,HomeActivity.class));
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        finish();*/
        text_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* startActivity(new Intent(SignInActivity.this,HomeActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                LL_errorSection.setVisibility(View.GONE);*/
                if (validateUserId() && validateEmail() && validatePassword()) {
                    logingHttpCall();
                }

                //finish();
            }
        });

    }


    private boolean validateUserId() {
        if (input_user_id.getText().toString().trim().equals("")) {
            printErrorMsg("Email field should not be empty");
            return false;
        } else {
            return true;
        }
    }

    private boolean validateEmail() {
        if (!new EmailValidator().validateEmail(input_user_id.getText().toString().trim())) {
            printErrorMsg("Email must be valid ");
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        if (input_password.getText().toString().trim().equals("")) {
            printErrorMsg("Password field should not be empty");
            return false;
        } else {
            return true;
        }
    }


    private void printErrorMsg(String errorMsg) {
        /*LL_errorSection.setVisibility(View.VISIBLE);

        text_error.setText(""+errorMsg);*/
        Toast.makeText(this, "" + errorMsg, Toast.LENGTH_SHORT).show();
    }

    private void logingHttpCall() {
        try {
            final String url = Content.baseURL + "site/login";
            /*SimpleProgressBar.showProgress(SignInActivity.this);*/

            CommonLoadingDialog.showLoadingDialog(SignInActivity.this, "Log in....");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Object object = jsonObject.get("status");

                                if (object instanceof Boolean) {
                                    Gson gson = new Gson();
                                    LoginResponse loginResponse = gson.fromJson(response, LoginResponse.class);
                                    inventoryAppSession.setKeyUserId(loginResponse.getData().get(0).getId().trim());
                                    inventoryAppSession.setKeyUserType(loginResponse.getData().get(0).getUsertype().trim());
                                    inventoryAppSession.setKeyUserName(loginResponse.getData().get(0).getName());
                                    inventoryAppSession.setKeyUserEmail(loginResponse.getData().get(0).getUsername());
                                    inventoryAppSession.setKeyUserCellNumber(loginResponse.getData().get(0).getCellno());

//                                    inventoryAppSession.setISLogin(true);


                                    /*if (inventoryAppSession.getKeyUserType().equals("Supervisor")) {
                                        System.out.println("U ARE SUPERVISOR");
                                        startActivity(new Intent(SignInActivity.this, HomeActivityOther.class));
                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                        finish();
                                    }
                                    if (inventoryAppSession.getKeyUserType().equals("Financial")) {
                                        System.out.println("U ARE FINANCILA");
                                        startActivity(new Intent(SignInActivity.this, HomeActivityOther.class));
                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                        finish();
                                    }
                                    if (inventoryAppSession.getKeyUserType().equals("Physical")) {
                                        System.out.println("U ARE PHYSICAL");
                                        startActivity(new Intent(SignInActivity.this, HomeActivityOther.class));
                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                        finish();
                                    }
                                    if (inventoryAppSession.getKeyUserType().equals("Admin")) {
                                        System.out.println("U ARE ADMIN");
                                        startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                        finish();
                                    }*/


                                    //startActivity(new Intent(SignInActivity.this,OtpActivity.class));
                                    Intent mIntent = new Intent(SignInActivity.this,OtpActivity.class);
                                    //mIntent.putExtra("MODEL",loginResponse);
                                    startActivity(mIntent);
                                    finish();

                                    System.out.println("OUTPUT======success==" + loginResponse.getStatus());
                                    System.out.println("OUTPUT======success==" + inventoryAppSession.getKeyUserId());


                                } else {
                                    /*LL_errorSection.setVisibility(View.VISIBLE);
                                    text_error.setText(""+jsonObject.getString("data"));*/

                                    Toast.makeText(SignInActivity.this, "" + jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                                    System.out.println("OUTPUT======fail");
                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username", input_user_id.getText().toString().trim());
                    params.put("password", input_password.getText().toString());
                    params.put("device_token", FirebaseInstanceId.getInstance().getToken());
                    Log.e("Inventory signin == ", url + params);

                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SignInActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex);
        }
    }
}
