package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ProjectListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.model.ProjectModel;
import com.app.inventoryapp.model.ProjectResponse;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.PaginationInterface;
import com.app.inventoryapp.my_interface.ProjectRemoveListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeActivity extends BaseActivity implements ProjectRemoveListener {
    private static boolean itShouldLoadMore = false;
    String TAG = "HomeActivity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    SelectedUserInterface selectedUserInterface;
    @BindView(R.id.imageView_add_project)
    ImageView imageView_add_project;
    @BindView(R.id.imageProfileIV)
    ImageView imageProfileIV;
    @BindView(R.id.imgProjectDeleteIV)
    ImageView imgProjectDeleteIV;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    ArrayList<ProjectModel> projectModelArrayList = new ArrayList<>();
    ArrayList<ProjectDataModel> projectDataModelArrayList = new ArrayList<>();
    InventoryAppSession session;
    ProjectListAdapter projectListAdapter;
    /*boolean isScrolling=false;
    int currentItems, totalItems, scrolloutItems;*/
    AlertDialog.Builder alertDialogBuilder;
    String strData = "";
    @BindView(R.id.imageView_notification)
    ImageView imageView_notification;
    @BindView(R.id.ivBadge)
    ImageView ivBadge;
    BroadcastReceiver IconChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ivBadge.setVisibility(View.VISIBLE);
        }
    };
    private String url = Content.baseURL;
    private int pageNo = 1;


    PaginationInterface mPaginationInterface = new PaginationInterface() {
        @Override
        public void loadMorePage() {
            if (itShouldLoadMore)
                loadMoreHttpAdminProject();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        createHomeInfo();
        registerReceiver(IconChange, new IntentFilter("IconChange"));

        selectedUserInterface = new SelectedUserInterface() {
            @Override
            public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList) {

            }

            @Override
            public void changeBadgeICon(boolean t) {
                if (t) {
                    ivBadge.setVisibility(View.VISIBLE);
                } else {
                    ivBadge.setVisibility(View.GONE);
                }
            }
        };
        if (BaseActivity.notification_alert) {
            ivBadge.setVisibility(View.VISIBLE);
        } else
            ivBadge.setVisibility(View.GONE);
    }

    private void createHomeInfo() {
        imageView_add_project.setVisibility(View.VISIBLE);
        //imageLogOut.setVisibility(View.VISIBLE);
        refreshLayout.setRefreshing(true);

        //setList();
        session = new InventoryAppSession(getApplicationContext());
        /*Hidding Add project button for non admin user*/
        if (!session.getKeyUserType().equals("Admin")) {
            imageView_add_project.setVisibility(View.GONE);
        } else {

        }

        intit();
        /*This function is used to redefine url as per user type*/
        reDefineUrl();
        httpAdminProject();
        /*This function is used to flush singletone's data*/
        //flusGlobalData();

        imageView_add_project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, AddProjectActivity.class);
                i.putExtra("added", true);
                startActivity(i);
                //   startActivity(new Intent(HomeActivity.this, AddProjectActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                InventoryAppSingleton.getInstance().setFromWhere("HOME");
            }
        });


//        imageLogOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showConfirmationDialog();
//                // Toast.makeText(HomeActivity.this, "Hello", Toast.LENGTH_SHORT).show();
//            }
//        });


        imageProfileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
            }
        });

        /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                    if (itShouldLoadMore) {
                        loadMoreHttpAdminProject();
                        //System.out.println("ITS TIME TO HIT API");
                    } else {

                    }
                }
            }
        });*/

        imageView_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseActivity.notification_alert) {
                    BaseActivity.notification_alert = false;
                    selectedUserInterface.changeBadgeICon(false);
                } else if (!BaseActivity.notification_alert) {
                    selectedUserInterface.changeBadgeICon(false);
                }
                startActivity(new Intent(HomeActivity.this, ProjectNotifications.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                httpAdminProject();
            }
        });

        imgProjectDeleteIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, TrashProjectActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });
    }

    protected void onResume() {
        super.onResume();
        if (inventoryAppSession.getTimeStamp() != null) {

            if (!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if (difftime(localTime, inventoryAppSession.getTimeStamp()) >= 60) {
                    inventoryAppSession.user_logout();

                } else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                    httpAdminProject();
                }
            } else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);

            }


        }


    }

    private void reDirectionsNotifications(String strDataMM) {
        try {
            JSONObject jsonObject = new JSONObject(strDataMM);

            String project_id = jsonObject.getString("project_id");
            String type = jsonObject.getString("type");
            String item = "";
            if (!jsonObject.isNull("item")) {
                item = jsonObject.getString("item");
                InventoryAppSingleton.getInstance().setItem_id(item);
            }


            System.out.println("OUT PROJECT==" + type + "====" + item);

            if (type.equals("add_project")) {
                //startActivity(new Intent(HomeActivity.this, HomeActivity.class));
            } else {
                startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
                finish();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {


        System.out.println("OUTPUT==========OF EXTRA=" + getIntent().getExtras());
        super.onBackPressed();

    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(HomeActivity.this);
    }

    private void flusGlobalData() {
        InventoryAppSingleton.getInstance().setItem_id("");
        InventoryAppSingleton.getInstance().setProject_id("");
        InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());

        InventoryAppSingleton.getInstance().setPhysicalProgress("");
        InventoryAppSingleton.getInstance().setFinancialProgress("");
    }

    private void reDefineUrl() {
        if (session.getKeyUserType().equals("Admin")) {
            url = url + "projectd?expand=tblIteams&page=";
        }
        if (session.getKeyUserType().equals("Supervisor")) {
            url = url + "projectd?expand=tblIteams&s_user=" + session.getKeyUserId() + "&page=";
        }
        if (session.getKeyUserType().equals("Financial")) {
            url = url + "projectd?expand=tblIteams&f_user=" + session.getKeyUserId() + "&page=";
        }
        if (session.getKeyUserType().equals("Physical")) {
            url = url + "projectd?expand=tblIteams&user_id=" + session.getKeyUserId() + "&page=";
        }
    }

    private void httpAdminProject() {
        try {

            refreshLayout.setRefreshing(true);
            pageNo = 1;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url + "1" + "&sort=-id&trash=0",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            refreshLayout.setRefreshing(false);
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();
                                    projectDataModelArrayList.clear();
                                    ProjectResponse projectResponse = gson.fromJson(response, ProjectResponse.class);

                                    if (projectDataModelArrayList.size() > 0) {
                                        projectDataModelArrayList.clear();
                                        projectListAdapter.updateAdapter(projectDataModelArrayList);
                                        projectDataModelArrayList = projectResponse.getData();
//                                        ArrayList<ProjectDataModel> mTempAL = new ArrayList();
//                                        for (int i = 0; i < projectDataModelArrayList.size(); i++) {
//                                            ProjectDataModel mModel = projectDataModelArrayList.get(i);
//                                            if (mModel.getTrash().equals("0")) {
//                                                mTempAL.add(mModel);
//                                            }
//                                        }

                                        if (projectDataModelArrayList.size() < 20) {
                                            itShouldLoadMore = false;
                                        } else {
                                            itShouldLoadMore = true;
                                        }


                                        setList(projectDataModelArrayList);

                                    } else {
                                        projectDataModelArrayList = projectResponse.getData();
                                        ArrayList<ProjectDataModel> mTempAL = new ArrayList();
//                                        for (int i = 0; i < projectDataModelArrayList.size(); i++) {
//                                            ProjectDataModel mModel = projectDataModelArrayList.get(i);
////                                            if (mModel.getTrash().equals("0")) {
////                                                mTempAL.add(mModel);
////                                            }
//                                        }

                                        if (projectDataModelArrayList.size() < 20) {
                                            itShouldLoadMore = false;
                                        } else {
                                            itShouldLoadMore = true;
                                        }
                                        setList(projectDataModelArrayList);
                                    }
                                }
                            } catch (Exception ex) {
                                refreshLayout.setRefreshing(false);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            if (CommonLoadingDialog.alertDialog != null)
                                CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void loadMoreHttpAdminProject() {
        try {

            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            //final String url = Content.baseURL + "projectds?expand=tblIteams";
            System.out.println("OUTPUT URL=====" + url);
            pageNo = pageNo + 1;
            progressbar.setVisibility(View.VISIBLE);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url + pageNo + "&sort=-id",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //CommonLoadingDialog.closeLoadingDialog();
                            progressbar.setVisibility(View.GONE);

                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();

                                    ProjectResponse projectResponse = gson.fromJson(response, ProjectResponse.class);
                                    // projectDataModelArrayList= filterUserAsPerUser(projectResponse.getData());

                                    System.out.println("NUMBER OF PROJECT OF THE USER===" + projectDataModelArrayList.size());

                                    // projectDataModelArrayList = projectResponse.getData();

                                    System.out.println("REFRESH DATA SIZE=====" + projectResponse.getData().size());

                                    if (projectResponse.getData().size() != 0) {
                                        for (int i = 0; i < projectResponse.getData().size(); i++) {

                                            ProjectDataModel projectDataModel = projectResponse.getData().get(i);

                                            projectDataModelArrayList.add(projectDataModel);

                                        }
                                        projectListAdapter.updateAdapter(projectDataModelArrayList);

                                    } else {
                                        itShouldLoadMore = false;
                                        Toast.makeText(HomeActivity.this, "No more data to pull", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } catch (Exception ex) {
                                progressbar.setVisibility(View.GONE);

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            progressbar.setVisibility(View.GONE);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex);
        }
    }

    private void setList(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        projectListAdapter = new ProjectListAdapter(HomeActivity.this, projectDataModelArrayList, this, mPaginationInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(projectListAdapter);


    }


    private ArrayList<ProjectDataModel> filterUserAsPerUser(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        ArrayList<ProjectDataModel> projectDataModelArrayList1 = new ArrayList<>();

        if (session.getKeyUserType().equals("Admin")) {
            projectDataModelArrayList1 = projectDataModelArrayList;
        }
        if (session.getKeyUserType().equals("Supervisor")) {

            for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                ProjectDataModel projectDataModel = projectDataModelArrayList.get(i);
                if (projectDataModel.getS_user().equals(session.getKeyUserId())) {
                    projectDataModelArrayList1.add(projectDataModel);
                }
            }
        }

        if (session.getKeyUserType().equals("Financial")) {
            for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                ProjectDataModel projectDataModel = projectDataModelArrayList.get(i);
                if (projectDataModel.getF_user().equals(session.getKeyUserId())) {
                    projectDataModelArrayList1.add(projectDataModel);
                }
            }
        }
        if (session.getKeyUserType().equals("Physical")) {
            for (int i = 0; i < projectDataModelArrayList.size(); i++) {
                ProjectDataModel projectDataModel = projectDataModelArrayList.get(i);
                if (projectDataModel.getUser_id().equals(session.getKeyUserId())) {
                    projectDataModelArrayList1.add(projectDataModel);
                }
            }
        }

        return projectDataModelArrayList1;
    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //  session.user_logout();
                httpClearToken();


            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDialog.dismiss();

            }
        });
    }

    private void httpClearToken() {
        try {
            final String url = Content.baseURL + "site/cleartoken";

            CommonLoadingDialog.showLoadingDialog(HomeActivity.this, "Creating...");


            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    session.user_logout();
                                    startActivity(new Intent(HomeActivity.this, SignInActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    finish();
                                }
                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", session.getKeyUserId());


                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    @Override
    public void removeProject(String project_id, String startDate, String endDate) {
        show_delete_confirmation_dialog(project_id, startDate, endDate);
    }


    /*private void deleteProjectApi(String project_id) {
        try {
            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            String url = Content.baseURL + "projects/" + project_id;
            System.out.println("CURRENT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(HomeActivity.this, "Deleting....");
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {

                                    Toast.makeText(HomeActivity.this, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();

                                    startActivity(new Intent(HomeActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                    overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                                    finish();
                                    //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);

                                } else {
                                    Toast.makeText(HomeActivity.this, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();

                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }


    }*/


    private void deleteProjectApi(String project_id, String strStartDate, String strEndDate) {

        try {
            String startArray[] = strStartDate.split("-");
            String endArray[] = strEndDate.split("-");

            final String finalStartDate = startArray[2] + "-" + startArray[1] + "-" + startArray[0];
            final String finalEndDate = endArray[2] + "-" + endArray[1] + "-" + endArray[0];
            Log.e(TAG, "====startDate====" + finalStartDate);
            Log.e(TAG, "====endDate====" + finalStartDate);

            String url = Content.baseURL + "projects/" + project_id;
            System.out.println("CURRENT URL=====" + url);
            CommonLoadingDialog.showLoadingDialog(HomeActivity.this, "Undo....");
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    Toast.makeText(HomeActivity.this, "Project Deleted!", Toast.LENGTH_SHORT).show();
                                    //*Again Getting/Refreshed the Project*//*
                                    httpAdminProject();

                                } else {
                                    Toast.makeText(HomeActivity.this, jsonObject.getString("data"), Toast.LENGTH_SHORT).show();
                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("trash", "1");
                    params.put("starting_date", finalStartDate);
                    params.put("ending_date", finalEndDate);
                    return params;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


    private void show_delete_confirmation_dialog(final String project_id, final String startDate, final String endDate) {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure you want to delete?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProjectApi(project_id, startDate, endDate);
                alertDialog.dismiss();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(IconChange);
    }
}
