package com.app.inventoryapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.UpdateAdminPaymentAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.font.CustomButton;
import com.app.inventoryapp.font.CustomTextView;
import com.app.inventoryapp.model.DataItem;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.model.UnpaidResponseModel;
import com.app.inventoryapp.my_interface.PaymentDocUploadInterface;
import com.app.inventoryapp.my_interface.UpdateDetailsInterface;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UpdatePaymentByAdminActivity extends AppCompatActivity implements PaymentDocUploadInterface, UpdateDetailsInterface {
    public static double rate_per_unit;
    ProjectItemModel projectItemModel = new ProjectItemModel();
    InventoryAppSession inventoryAppSession;
    @Override
    public void UpdateUI() {

    }

    InventoryAppSession session;

    /*This user id is used to hold the physical user's id*/
    String user_id;
    String quantity;
    String subItem;
    String amount;
    String itSelfID;

    UpdateAdminPaymentAdapter mUpdateAdminPaymentAdapter;

    String strItemID = "";

    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.textSave)
    CustomTextView textSave;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.RL_recyclerView_container)
    LinearLayout RLRecyclerViewContainer;
    @BindView(R.id.LL_no_data_info)
    LinearLayout LL_no_data_info;
    @BindView(R.id.btn_histry)
    CustomButton btnHistry;
    @BindView(R.id.btn_info)
    CustomButton btnInfo;
    @BindView(R.id.LL_button_container)
    LinearLayout LLButtonContainer;


    @BindView(R.id.RL_container)
    RelativeLayout RLContainer;

    ImageView back_btn;


    private ArrayList<DataItem> dataItemArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_payment_by_admin);
        ButterKnife.bind(this);

        session = new InventoryAppSession(UpdatePaymentByAdminActivity.this);
inventoryAppSession=new InventoryAppSession(UpdatePaymentByAdminActivity.this);
        setStatus();

        btnHistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UpdatePaymentByAdminActivity.this, HistoryActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("projectItemModel", projectItemModel);
                startActivity(new Intent(UpdatePaymentByAdminActivity.this, ProjectInfoOther.class).putExtras(bundle));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });


        httpGetItemTobePaid();

        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }



    private void setStatus() {
        /*Setting color in item level*/
        String itemLevelPhysicalProgress = InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel();
        String itemLevelFinancialProgress = InventoryAppSingleton.getInstance().getFinancialProgressItemlevel();
        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }

    }

    private void httpGetItemTobePaid() {
        dataItemArrayList.clear();
        String strItemID = InventoryAppSingleton.getInstance().getItem_id();
        try {
            String url = Content.baseURL + "docps?check=1&i_id=" + strItemID;
            System.out.println("OUTPUT URL====" + url);
            CommonLoadingDialog.showLoadingDialog(UpdatePaymentByAdminActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    rate_per_unit = Double.parseDouble(InventoryAppSingleton.getInstance().getItem_rate());
                                    System.out.println("RESPONSE======" + response);
                                    Gson gson = new Gson();
                                    UnpaidResponseModel unpaidResponseModel = gson.fromJson(response, UnpaidResponseModel.class);
                                    dataItemArrayList = (ArrayList<DataItem>) unpaidResponseModel.getData();
                                    System.out.println("SIZE OF UNPAID DATA=====" + dataItemArrayList);
                                    if (dataItemArrayList.size() == 0) {
                                        LL_no_data_info.setVisibility(View.VISIBLE);
                                        mUpdateAdminPaymentAdapter.notifyDataSetChanged();
                                    } else {
                                        setPaymentList(dataItemArrayList);
                                    }

                                    try {
                                        rate_per_unit = Double.parseDouble(InventoryAppSingleton.getInstance().getItem_rate());
                                    } catch (Exception ex) {
                                        System.out.println("ERROR OUTPUT===========" + ex);
                                        rate_per_unit = 0;
                                    }
                                } else {
                                    Toast.makeText(UpdatePaymentByAdminActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UpdatePaymentByAdminActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setPaymentList(ArrayList<DataItem> dataItemArrayList) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(UpdatePaymentByAdminActivity.this);
        mUpdateAdminPaymentAdapter = new UpdateAdminPaymentAdapter(UpdatePaymentByAdminActivity.this, dataItemArrayList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mUpdateAdminPaymentAdapter);
    }


    @Override
    public void showPaymentUploadDialog(String itSelfID, Double amount, String user_id, String quantity, String subItem) {
        this.user_id = user_id;
        this.amount = "" + amount;
        this.quantity = quantity;
        this.subItem = subItem;
        this.itSelfID = itSelfID;

        System.out.println("PARAM====================IT_SELF_ID===" + this.itSelfID);
        System.out.println("PARAM====================AMOUNT===" + this.amount);
        System.out.println("PARAM====================QUANTITY==" + this.quantity);
        System.out.println("PARAM====================USER_ID==" + this.user_id);
        System.out.println("PARAM====================SUBITEM==" + this.subItem);

        //docUploadDialog();

//        new paymentUpload().execute();

        executePaymentUploadApi();

    }

    private void executePaymentUploadApi() {
        String requestURL = Content.baseURL + "docps?check=2";
        try {
            CommonLoadingDialog.showLoadingDialog(UpdatePaymentByAdminActivity.this, "Loading....");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status") == true){
                                    Toast.makeText(UpdatePaymentByAdminActivity.this,"Payment Sucessfully Done!",Toast.LENGTH_SHORT).show();
                                    httpGetItemTobePaid();
                                }else{
                                    Toast.makeText(UpdatePaymentByAdminActivity.this,"Server Error!",Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("project_id", InventoryAppSingleton.getInstance().getProject_id());
                    params.put("user_id", user_id.trim());
                    params.put("iteam_id", InventoryAppSingleton.getInstance().getItem_id());
                    params.put("paid_user", "" + session.getKeyUserId());
                    params.put("qty_done", "" + quantity.trim());
                    params.put("sub_item", "" + subItem.trim());
                    params.put("amount_paid", "" + amount.trim());

                    return params;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UpdatePaymentByAdminActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }


   /* private class paymentUpload extends AsyncTask<String, String, String> {
        StringBuilder response = new StringBuilder();
        String stringData = "";

        @Override
        protected String doInBackground(String... strings) {
            String result = null;
            String charset = "UTF-8";
            String requestURL = Content.baseURL + "docps?check=2";

            System.out.println("URL=========================" + requestURL);
            try {

                MultipartUtility multipart = new MultipartUtility(requestURL, charset);
                multipart.addFormField("project_id", InventoryAppSingleton.getInstance().getProject_id());
                multipart.addFormField("user_id", user_id.trim());
                multipart.addFormField("iteam_id", InventoryAppSingleton.getInstance().getItem_id());
                multipart.addFormField("paid_user", "" + session.getKeyUserId());
                multipart.addFormField("qty_done", "" + quantity.trim());
                multipart.addFormField("sub_item", "" + subItem.trim());
                multipart.addFormField("amount_paid", "" + amount.trim());
//                multipart.addFilePart("pay_img", fileToUpload);

                response = multipart.finish();


                System.out.println("RESPONSE MULTIPART===" + response);

            } catch (Exception ex) {
                CommonLoadingDialog.closeLoadingDialog();
                System.out.println(" " + ex);
            }
            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CommonLoadingDialog.showLoadingDialog(UpdatePaymentByAdminActivity.this, "Uploading....");
        }

        @Override
        protected void onPostExecute(String jsonStr) {
            System.out.println("POST EXECUTION");
            try {
                CommonLoadingDialog.closeLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    System.out.println("PAYMENT RESPONSE========================" + jsonObject.toString());
                    System.out.println("RESPONSE MULTIPART======" + jsonObject);
                    if (jsonObject.getBoolean("status")) {
                        startActivity(new Intent(UpdatePaymentByAdminActivity.this, HistoryActivity.class));
                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        finish();
                    }
                } catch (Exception ex) {
                    System.out.println("ERROR=========1" + ex.toString());
                }

                //finish();
            } catch (Exception ex) {
                System.out.println("ERROR=========2" + ex.toString());
            }
        }


    }*/
   protected void onResume() {
       super.onResume();
       if(inventoryAppSession.getTimeStamp()!=null){

           if(!inventoryAppSession.getTimeStamp().equals("")) {
               Calendar cal = Calendar.getInstance();
               Date currentLocalTime = cal.getTime();
               DateFormat date2 = new SimpleDateFormat("HH:mm a");
               String localTime = date2.format(currentLocalTime);
               if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                   inventoryAppSession.user_logout();
                   startActivity(new Intent(UpdatePaymentByAdminActivity.this,SignInActivity.class));
               }
               else {
                   Calendar cal1 = Calendar.getInstance();
                   Date currentLocalTime1 = cal1.getTime();
                   DateFormat date21 = new SimpleDateFormat("HH:mm a");

                   String localTime1 = date21.format(currentLocalTime1);
                   inventoryAppSession.setCurrentTimeStamp(localTime1);
               }
           }
           else {
               Calendar cal = Calendar.getInstance();
               Date currentLocalTime = cal.getTime();
               DateFormat date2 = new SimpleDateFormat("HH:mm a");

               String localTime = date2.format(currentLocalTime);
               inventoryAppSession.setCurrentTimeStamp(localTime);
           }


       }


   }
    public int difftime(String string, String string2) {
        int hours;
        int min = 0;
        int days;
        long difference;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            Date date1 = simpleDateFormat.parse(string2);
            Date date2 = simpleDateFormat.parse(string);

            difference = date2.getTime() - date1.getTime();
            days = (int) (difference / (1000 * 60 * 60 * 24));
            hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours))
                    / (1000 * 60);
            hours = (hours < 0 ? -hours : hours);
            Log.i("======= Hours", " :: " + hours);
            return min;
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return min;
    }

}
