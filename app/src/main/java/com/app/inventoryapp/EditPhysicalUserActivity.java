package com.app.inventoryapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.PhysicalUserListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.SupervisorWithPhysicalUsers;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.PhysicalSelectedUserInterface;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditPhysicalUserActivity extends AppCompatActivity {
    Activity mActivity = EditPhysicalUserActivity.this;
    String TAG = EditPhysicalUserActivity.this.getClass().getSimpleName();

    @BindView(R.id.imgBackIV)
    ImageView imgBackIV;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.input_search_user)
    EditText input_search_user;
    @BindView(R.id.imageView_clear_text)
    ImageView imageView_clear_text;
    @BindView(R.id.physicalRV)
    RecyclerView physicalRV;
    @BindView(R.id.LL_data_container)
    LinearLayout LLDataContainer;

    @BindView(R.id.btnUpdateUser)
    Button btnUpdateUser;

    AlertDialog.Builder alertDialogBuilder;

    ArrayList<UsersResponse> mSupervisorAL = new ArrayList<>();
    ArrayList<SupervisorWithPhysicalUsers> mSupervisorWithPhysicalAL = new ArrayList<SupervisorWithPhysicalUsers>();
    PhysicalUserListAdapter mPhysicalUserListAdapter;
    String strUserIDs = "";

    String Finalstr_P_users = "";
    String Finalstr_F_users = "";
    String Finalstr_S_users = "";


    ArrayList<UserDataModel> userDataModelArrayList = new ArrayList<UserDataModel>();
    String strAlreadyAddedFinancialUser = "";
    private ArrayList<UsersResponse> finalSupervisorArrayList = new ArrayList<>();
    private ArrayList<UsersResponse> physicalUserArrayList = new ArrayList<>();
    PhysicalSelectedUserInterface mSelectedUserInterface = new PhysicalSelectedUserInterface() {
        @Override
        public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList, boolean isSelectedUnSelect, UsersResponse mResponse) {

            if (isSelectedUnSelect == true) {
                for (int i = 0; i < mArrayList.size(); i++) {
                    UsersResponse mUsersResponse = mArrayList.get(i);
                    if (contains(physicalUserArrayList, mUsersResponse.getId()) == false) {
                        physicalUserArrayList.add(mUsersResponse);
                    }
                }
            } else if (isSelectedUnSelect == false) {
                physicalUserArrayList.remove(mResponse);
            }


            Log.e(TAG, "=====SIZE=====" + physicalUserArrayList.size());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_physical_user);
        ButterKnife.bind(this);
        intit();
        if (getIntent() != null) {
            mSupervisorAL = (ArrayList<UsersResponse>) getIntent().getSerializableExtra("LIST");
            userDataModelArrayList = (ArrayList<UserDataModel>) getIntent().getSerializableExtra("P_SEL_LIST");
            strAlreadyAddedFinancialUser = getIntent().getStringExtra("F_ID");

            for (int i = 0; i < mSupervisorAL.size(); i++) {
                if (strUserIDs.equals("")) {
                    strUserIDs = mSupervisorAL.get(i).getId().trim();
                } else {
                    strUserIDs = strUserIDs + "," + mSupervisorAL.get(i).getId().trim();
                }

            }
            Log.e(TAG, "========IDS=====" + strUserIDs);

            httpGetUsersCall();
        }

    }


    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(mActivity);
    }


    @OnClick({R.id.imgBackIV, R.id.imageView_clear_text, R.id.btnUpdateUser})
    public void onViewClicked(View mView) {
        switch (mView.getId()) {
            case R.id.imgBackIV:
                onBackPressed();
                break;
            case R.id.imageView_clear_text:
                input_search_user.setText("");
                imageView_clear_text.setVisibility(View.GONE);
                break;
            case R.id.btnUpdateUser:
                if (physicalUserArrayList.size() > 0)
                    validateAddUsers();
                else
                    CommonValidationDialog.showLoadingDialog(mActivity, "Please select atleast one physical user");
                break;


        }
    }

    private void validateAddUsers() {
        getAllUsersList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void httpGetUsersCall() {
        try {
            final String url = Content.GET_PHYSICAL_USERS_ACCORING_SUPERVISOR + strUserIDs;
            Log.e(TAG, "==URL==" + url);
            CommonLoadingDialog.showLoadingDialog(mActivity, "Loading...");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                System.out.println("SIZE====" + jsonArray.length());

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                                    SupervisorWithPhysicalUsers mModel = new SupervisorWithPhysicalUsers();
                                    if (!jsonObject1.isNull("id"))
                                        mModel.setId(jsonObject1.getString("id"));
                                    if (!jsonObject1.isNull("username"))
                                        mModel.setUsername(jsonObject1.getString("username"));
                                    if (!jsonObject1.isNull("usertype"))
                                        mModel.setUsertype(jsonObject1.getString("usertype"));
                                    if (!jsonObject1.isNull("name"))
                                        mModel.setName(jsonObject1.getString("name"));
                                    if (!jsonObject1.isNull("password"))
                                        mModel.setPassword(jsonObject1.getString("password"));
                                    if (!jsonObject1.isNull("cellno"))
                                        mModel.setCellno(jsonObject1.getString("cellno"));
                                    if (!jsonObject1.isNull("user_location"))
                                        mModel.setUser_location(jsonObject1.getString("user_location"));
                                    if (!jsonObject1.isNull("role"))
                                        mModel.setRole(jsonObject1.getString("role"));
                                    if (!jsonObject1.isNull("device_token"))
                                        mModel.setDevice_token(jsonObject1.getString("device_token"));
                                    if (!jsonObject1.isNull("supervisor"))
                                        mModel.setSupervisor(jsonObject1.getString("supervisor"));
                                    if (!jsonObject1.isNull("physical"))
                                        mModel.setPhysical(jsonObject1.getString("physical"));
                                    if (!jsonObject1.isNull("status"))
                                        mModel.setStatus(jsonObject1.getString("status"));

                                    if (!jsonObject1.isNull("physicalUser")) {
                                        JSONArray mJsonArray1 = jsonObject1.getJSONArray("physicalUser");
                                        ArrayList<UsersResponse> mSubPhyscialAL = new ArrayList<>();
                                        mSubPhyscialAL.clear();
                                        for (int k = 0; k < mJsonArray1.length(); k++) {
                                            JSONObject mSubObj = mJsonArray1.getJSONObject(k);
                                            UsersResponse mUsersResponse = new UsersResponse();

                                            if (!mSubObj.isNull("id"))
                                                mUsersResponse.setId(mSubObj.getString("id"));
                                            if (!mSubObj.isNull("username"))
                                                mUsersResponse.setUsername(mSubObj.getString("username"));
                                            if (!mSubObj.isNull("usertype"))
                                                mUsersResponse.setUsertype(mSubObj.getString("usertype"));
                                            if (!mSubObj.isNull("name"))
                                                mUsersResponse.setName(mSubObj.getString("name"));
                                            if (!mSubObj.isNull("password"))
                                                mUsersResponse.setPassword(mSubObj.getString("password"));
                                            if (!mSubObj.isNull("cellno"))
                                                mUsersResponse.setCellno(mSubObj.getString("cellno"));
                                            if (!mSubObj.isNull("user_location"))
                                                mUsersResponse.setUser_location(mSubObj.getString("user_location"));
                                            if (!mSubObj.isNull("role"))
                                                mUsersResponse.setRole(mSubObj.getString("role"));
                                            if (!mSubObj.isNull("device_token"))
                                                mUsersResponse.setDevice_token(mSubObj.getString("device_token"));
                                            if (!mSubObj.isNull("supervisor"))
                                                mUsersResponse.setSupervisor(mSubObj.getString("supervisor"));
                                            if (!mSubObj.isNull("physical"))
                                                mUsersResponse.setPhysical(mSubObj.getString("physical"));
                                            if (!mSubObj.isNull("status"))
                                                mUsersResponse.setStatus(mSubObj.getString("status"));

                                            mSubPhyscialAL.add(mUsersResponse);
                                        }

                                        mModel.setmSubPhysicalUserAL(mSubPhyscialAL);
                                    }

                                    mSupervisorWithPhysicalAL.add(mModel);
                                }


                                                             /*Set Adapter*/
                                setmExpandableListAdapter();

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setmExpandableListAdapter() {
        mPhysicalUserListAdapter = new PhysicalUserListAdapter(mActivity, mSupervisorWithPhysicalAL, mSelectedUserInterface);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        physicalRV.setLayoutManager(layoutManager);
        physicalRV.setItemAnimator(new DefaultItemAnimator());
        physicalRV.setAdapter(mPhysicalUserListAdapter);
    }

    boolean contains(ArrayList<UsersResponse> list, String mUserID) {
        for (UsersResponse item : list) {
            if (item.getId().equals(mUserID)) {
                return true;
            }
        }
        return false;
    }


    private void getAllUsersList() {
        for (int i = 0; i < mSupervisorAL.size(); i++) {
            for (int j = 0; j < physicalUserArrayList.size(); j++) {
                if (mSupervisorAL.get(i).getId().equals(physicalUserArrayList.get(j).getSupervisor())) {

                    UsersResponse mUsersResponse = mSupervisorAL.get(i);
                    if (contains(finalSupervisorArrayList, mUsersResponse.getId()) == false) {
                        finalSupervisorArrayList.add(mUsersResponse);
                    }
                }
            }
        }

        Log.e(TAG, "=====Supervisor Size====" + finalSupervisorArrayList.size());

//        EditProjectUserActivity.mSelectedUserArrayListEDIT.addAll(physicalUserArrayList);
//        EditProjectUserActivity.mSelectedUserArrayListEDIT.addAll(finalSupervisorArrayList);


        String str_P_users = "";
        String str_F_users = "";
        String str_S_users = "";

        for (int i = 0; i < physicalUserArrayList.size(); i++) {
            if (str_P_users.equals("")) {
                str_P_users = physicalUserArrayList.get(i).getId().trim();
            } else {
                str_P_users = str_P_users + "," + physicalUserArrayList.get(i).getId().trim();
            }
        }


        for (int i = 0; i < finalSupervisorArrayList.size(); i++) {
            if (str_S_users.equals("")) {
                str_S_users = finalSupervisorArrayList.get(i).getId().trim();
            } else {
                str_S_users = str_S_users + "," + finalSupervisorArrayList.get(i).getId().trim();
            }
        }

        str_F_users = strAlreadyAddedFinancialUser;

        String startingDate[] = InventoryAppSingleton.getInstance().getStarting_date().split("-");
        String endingDate[] = InventoryAppSingleton.getInstance().getEnding_date().split("-");


        Log.e(TAG, "=====FINANCIAL_USER====" + str_F_users);
        Log.e(TAG, "=====PHYSICAL_USER====" + str_P_users);
        Log.e(TAG, "=====SUPERVISOR_USER====" + str_S_users);
        Log.e(TAG, "=====START DATE===" + "" + "starting_date" + startingDate[2] + "-" + startingDate[1] + "-" + startingDate[0]);
        Log.e(TAG, "=====END DATE====" + "ending_date" + endingDate[2] + "-" + endingDate[1] + "-" + endingDate[0]);

        String tempPHY_User = "";
        String tempSUPER_User = "";
        for (int i = 0; i < userDataModelArrayList.size(); i++) {
            if (userDataModelArrayList.get(i).getUsertype().equals("Physical")) {
                if (tempPHY_User.equals("")) {
                    tempPHY_User = userDataModelArrayList.get(i).getId().trim();
                } else {
                    tempPHY_User = tempPHY_User + "," + userDataModelArrayList.get(i).getId().trim();
                }
            } else if (userDataModelArrayList.get(i).getUsertype().equals("Supervisor")) {
                if (tempSUPER_User.equals("")) {
                    tempSUPER_User = userDataModelArrayList.get(i).getId().trim();
                } else {
                    tempSUPER_User = tempSUPER_User + "," + userDataModelArrayList.get(i).getId().trim();
                }
            }
        }


        String[] arrayPHY_Pre = tempPHY_User.split(",");
        String[] arraySUP_Pre = tempSUPER_User.split(",");


        String[] arrayPHY_New = str_P_users.split(",");
        String[] arraySUP_New = str_S_users.split(",");


        Set<String> listPHY = new LinkedHashSet<String>();
        listPHY.addAll(Arrays.asList(arrayPHY_Pre));
        listPHY.addAll(Arrays.asList(arrayPHY_New));
        String finalPHY_array[] = listPHY.toArray(new String[listPHY.size()]);


        Set<String> listSUP = new LinkedHashSet<String>();
        listSUP.addAll(Arrays.asList(arraySUP_Pre));
        listSUP.addAll(Arrays.asList(arraySUP_New));
        String finalSUP_array[] = listSUP.toArray(new String[listSUP.size()]);


        Log.e(TAG, "PHY========= " + Arrays.toString(finalPHY_array));
        Log.e(TAG, "SUP========= " + Arrays.toString(finalSUP_array));
        Log.e(TAG, "FIN========= " + str_F_users);

        //Fianl Physical Array
        for (int i = 0; i < finalPHY_array.length; i++) {
            if (Finalstr_P_users.equals("")) {
                Finalstr_P_users = finalPHY_array[i];
            } else {
                Finalstr_P_users = Finalstr_P_users + "," + finalPHY_array[i];
            }
        }
        //Final Supervisor Array
        for (int i = 0; i < finalSUP_array.length; i++) {
            if (Finalstr_S_users.equals("")) {
                Finalstr_S_users = finalSUP_array[i];
            } else {
                Finalstr_S_users = Finalstr_S_users + "," + finalSUP_array[i];
            }
        }


        //Final Financial Array
        Finalstr_F_users = str_F_users;


        Log.e(TAG, "String PHY========= " + Finalstr_P_users);
        Log.e(TAG, "String SUP========= " + Finalstr_S_users);
        Log.e(TAG, "String FIN========= " + Finalstr_F_users);


        /*eXECUTE ADD uSERS aPI*/
//        if (Finalstr_P_users != null && Finalstr_P_users.length() > 0)
//            showConfirmationDialog();
//        else {
//            CommonValidationDialog.showLoadingDialog(mActivity, "Please select atleast one physical user");
//        }

        showConfirmationDialog();

    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to add user?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                httpAddUSer();
                alertDialog.dismiss();
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });


    }


    private void httpAddUSer() {
        try {
            final String url = Content.baseURL + "projects/" + InventoryAppSingleton.getInstance().getProject_id();

            CommonLoadingDialog.showLoadingDialog(mActivity, "Creating...");


            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();

                            System.out.println("RESPONSE==========" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getBoolean("status")) {
                                    startActivity(new Intent(mActivity, ProjectUsersActivity.class));
                                    finish();
                                }

                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    String startingDate[] = InventoryAppSingleton.getInstance().getStarting_date().split("-");
                    String endingDate[] = InventoryAppSingleton.getInstance().getEnding_date().split("-");


                    params.put("starting_date", startingDate[2] + "-" + startingDate[1] + "-" + startingDate[0]);
                    params.put("ending_date", endingDate[2] + "-" + endingDate[1] + "-" + endingDate[0]);
                    params.put("s_user", Finalstr_S_users);
                    params.put("f_user", Finalstr_F_users);
                    params.put("user_id", Finalstr_P_users);
                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(mActivity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("ERROR====" + ex);
        }

    }

}




