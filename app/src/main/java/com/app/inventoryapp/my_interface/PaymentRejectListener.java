package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.HistoryDataItem;

public interface PaymentRejectListener {
    public void rejectPayment(HistoryDataItem historyDataItem);

    public void approvePayment(HistoryDataItem historyDataItem);

    public void reviewRequest(HistoryDataItem historyDataItem);
}
