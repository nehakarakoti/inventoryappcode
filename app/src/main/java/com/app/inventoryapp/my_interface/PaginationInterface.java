package com.app.inventoryapp.my_interface;

public interface PaginationInterface {
    public void loadMorePage();
}
