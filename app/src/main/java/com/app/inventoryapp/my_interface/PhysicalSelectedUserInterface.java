package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.UsersResponse;

import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/9/2018.
 */

public interface PhysicalSelectedUserInterface {
    public void mSelectedUserInterface(ArrayList<UsersResponse> mArrayList, boolean isSelectUnSelect, UsersResponse mUsersResponse);

}
