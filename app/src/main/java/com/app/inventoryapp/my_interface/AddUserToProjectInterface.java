package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.Response;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.model.UsersResponse;

import java.util.ArrayList;

public interface AddUserToProjectInterface
{
    public void selectUserToProject(ArrayList<UsersResponse> userDataModelArrayList);
}
