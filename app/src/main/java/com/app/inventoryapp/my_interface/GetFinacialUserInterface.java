package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.UsersResponse;

/**
 * Created by android-da on 11/20/18.
 */

public interface GetFinacialUserInterface {
    public void getFinancialUser(UsersResponse mUsersResponse);
}
