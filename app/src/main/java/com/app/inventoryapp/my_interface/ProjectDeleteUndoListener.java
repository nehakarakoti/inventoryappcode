package com.app.inventoryapp.my_interface;

/**
 * Created by android-da on 11/28/18.
 */

public interface ProjectDeleteUndoListener {
    public void undoProject(String project_id, String strStartDate, String strEndDate);

    public void deleteProject(String project_id);
}
