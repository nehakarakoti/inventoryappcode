package com.app.inventoryapp.my_interface;

public interface ProjectRemoveListener {
    public void removeProject(String project_id, String startDate, String endDate);
}
