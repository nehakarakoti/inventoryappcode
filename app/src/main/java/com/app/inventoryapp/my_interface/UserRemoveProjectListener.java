package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.UserDataModel;

public interface UserRemoveProjectListener
{
    public void removeUserFromProject(UserDataModel userDataModel);
}
