package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.DataItem;

public interface PaymentDocUploadInterface
{
    public void showPaymentUploadDialog(String itSelfID,Double amount,String user_id,String quantity, String subItem);
}
