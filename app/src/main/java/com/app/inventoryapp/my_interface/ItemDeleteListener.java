package com.app.inventoryapp.my_interface;

public interface ItemDeleteListener
{
    public void itemDelete(String item_id);
}
