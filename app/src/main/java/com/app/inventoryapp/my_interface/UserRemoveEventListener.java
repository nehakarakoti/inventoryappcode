package com.app.inventoryapp.my_interface;

import com.app.inventoryapp.model.UsersResponse;

public interface UserRemoveEventListener
{
    public void removeUser(UsersResponse usersResponse);
}
