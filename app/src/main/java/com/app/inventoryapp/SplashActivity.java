package com.app.inventoryapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.session.InventoryAppSession;
import org.json.JSONObject;
import java.util.ArrayList;

import me.leolin.shortcutbadger.ShortcutBadger;

public class SplashActivity extends AppCompatActivity {
    final int SPLASH_TIME_OUT = 2000;
    String strData = "";
    private InventoryAppSession inventoryAppSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        inventoryAppSession = new InventoryAppSession(SplashActivity.this);
ShortcutBadger.removeCount(getApplicationContext());
        flusGlobalData();
        getData();


       // setSpalsh();


    }

    private void flusGlobalData() {
        InventoryAppSingleton.getInstance().setItem_id("");
        InventoryAppSingleton.getInstance().setProject_id("");
        InventoryAppSingleton.getInstance().setUsersResponseArrayList(new ArrayList<UsersResponse>());

        InventoryAppSingleton.getInstance().setPhysicalProgress("");
        InventoryAppSingleton.getInstance().setFinancialProgress("");


    }




    private void getData() {
        try {
            if (getIntent().getExtras() != null) {
                for (String key : getIntent().getExtras().keySet()) {
                    Object value = getIntent().getExtras().get(key);
                    if (key.equalsIgnoreCase("data")) {
                        strData = String.valueOf(value);
                    }
                }
            }
            if(!inventoryAppSession.getKeyUserType().equals(""))
            reDirectionsNotifications(strData);
        } catch (Exception ex) {
            System.out.println("ERROR==" + ex);
        }
    }


    private void reDirectionsNotifications(String strDataMM) {
        try {
            JSONObject jsonObject = new JSONObject(strDataMM);

            String project_id = jsonObject.getString("project_id");
            InventoryAppSingleton.getInstance().setProject_id(project_id);
            String type = jsonObject.getString("type");
            String item = "";
            if (!jsonObject.isNull("item")){
                item  = jsonObject.getString("item");
                InventoryAppSingleton.getInstance().setItem_id(item);
            }

            System.out.println("OUT PROJECT==" + type + "====" + item);

            if (type.equals("add_project")) {
//inventoryAppSession.getKeyUserType()
                if(inventoryAppSession.getKeyUserType().equals("Admin"))
                {

                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();
                }
                else
                {
                    startActivity(new Intent(SplashActivity.this, HomeActivityOther.class));
                    finish();
                }

            }
            else if(type.equals("push_custom"))
            {
                startActivity(new Intent(SplashActivity.this, ProjectNotifications.class));
                finish();
            }
            else {
                InventoryAppSingleton.getInstance().setItem_id(item);
                startActivity(new Intent(SplashActivity.this, HistoryActivity.class));
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run()
            {
                startActivity(new Intent(SplashActivity.this,SignInActivity.class));
//                startActivity(new Intent(SplashActivity.this,OtpActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        }, 3000);
    }

}




