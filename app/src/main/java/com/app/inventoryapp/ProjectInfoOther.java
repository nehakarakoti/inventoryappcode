package com.app.inventoryapp;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.adapter.ContractorListAdapter;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.font.CustomTextView;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.model.UserResponseModel;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProjectInfoOther extends BaseActivity {

    ProjectItemModel projectItemModel = new ProjectItemModel();

    //ProjectItemDetails projectItemModel = new ProjectItemDetails();
    @BindView(R.id.textProjectDetails)
    CustomTextView textProjectDetails;
    @BindView(R.id.textQunatity)
    TextView textQunatity;
    @BindView(R.id.textUnits)
    CustomTextView textUnits;
    @BindView(R.id.textContractorName)
    CustomTextView textContractorName;
    @BindView(R.id.textRate)
    CustomTextView textRate;
    @BindView(R.id.textAmount)
    CustomTextView textAmount;
    @BindView(R.id.textInspectionClause)
    CustomTextView textInspectionClause;
    @BindView(R.id.rv_contractor_list)
    RecyclerView rv_contractor_list;
    @BindView(R.id.txtContractorNameTV)
    CustomTextView txtContractorNameTV;
    @BindView(R.id.txtContractorEmailTV)
    CustomTextView txtContractorEmailTV;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.back_btn)
    ImageView back_btn;
    private String project_id;
    private InventoryAppSession inventoryAppSession;
    private ArrayList<UserDataModel> userDataModelArrayList = new ArrayList<>();
    private ArrayList<UserDataModel> userDataModelContractorArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_info_other);
        ButterKnife.bind(this);
        project_id = InventoryAppSingleton.getInstance().getProject_id();
        projectItemModel = InventoryAppSingleton.getInstance().getProjectItemModel();
        inventoryAppSession=new InventoryAppSession(ProjectInfoOther.this);

        System.out.println("RITUPARNA=====" + projectItemModel.toString());

        System.out.println("PROJECT ID=====" + InventoryAppSingleton.getInstance().getProject_id());
        setStatus();

        setData();

        //httpGetContractor();

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }


    private void setStatus() {


        long diff = InventoryAppSingleton.getInstance().getDiff();
        if (diff >= 0) {
            System.out.println("ITEM STATUS============COMPLETED ");
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_chart_green));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.color_red));
        }
    }

    private void setData() {
        textProjectDetails.setText(projectItemModel.getIteam());
        textQunatity.setText(projectItemModel.getQty());
        textInspectionClause.setText(projectItemModel.getRef());
        textAmount.setText(projectItemModel.getC_amount());
        textRate.setText(projectItemModel.getC_rate());
        textUnits.setText(projectItemModel.getUnit());
        txtContractorNameTV.setText(projectItemModel.getContractor_name());
        txtContractorEmailTV.setText(projectItemModel.getContractor_email());
    }


    private void httpGetContractor() {
        try {


            String url = Content.baseURL + "users/history?project_id=" + project_id;


            CommonLoadingDialog.showLoadingDialog(ProjectInfoOther.this, "Loading....");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("RESPONSE=======" + response);

                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    Gson gson = new Gson();

                                    UserResponseModel userResponseModel = gson.fromJson(response, UserResponseModel.class);
                                    userDataModelArrayList = userResponseModel.getData();


                                    parseContractorList(userDataModelArrayList);
                                    setContractorList();

                                    System.out.println("CONTRACVTOR========" + userDataModelContractorArrayList.size());


                                }
                                /*Gson gson = new Gson();
                                HistoryResponse historyResponse = gson.fromJson(response,HistoryResponse.class);
*/


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    System.out.println("HELLO HEDER===" + headers);
                    return headers;
                }
            };
            ;


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProjectInfoOther.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }

    private void setContractorList() {

        ContractorListAdapter contractorListAdapter = new ContractorListAdapter(ProjectInfoOther.this, userDataModelContractorArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        rv_contractor_list.setLayoutManager(mLayoutManager);
        rv_contractor_list.setAdapter(contractorListAdapter);

    }

    private void parseContractorList(ArrayList<UserDataModel> userDataModelArrayList) {
        for (int i = 0; i < userDataModelArrayList.size(); i++) {

            //  System.out.println("USER TYPE====="+userDataModelArrayList.get(i).getUsertype());

            if (userDataModelArrayList.get(i).getUsertype().equals("Physical")) {
                userDataModelContractorArrayList.add(userDataModelArrayList.get(i));
            }
        }
    }
    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(ProjectInfoOther.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }

    }
}
