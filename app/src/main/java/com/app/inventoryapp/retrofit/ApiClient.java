package com.app.inventoryapp.retrofit;

import com.app.inventoryapp.utils.Content;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by RITUPARNA on 5/13/2018.
 */

public class ApiClient
{
    public static final String BASE_URL = Content.baseURL;
    /*public static final String BASE_URL2 = "http://192.168.43.52/slim_api_test/v1/  ";*/
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
