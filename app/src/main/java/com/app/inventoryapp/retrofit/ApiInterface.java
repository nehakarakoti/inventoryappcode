package com.app.inventoryapp.retrofit;



import com.app.inventoryapp.model.UnpaidResponseModel;


import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by RITUPARNA on 5/13/2018.
 */

public interface ApiInterface
{
    /*@FormUrlEncoded
    @POST("login")
    Call<Response> getLoginDetails(@Field("email") String email, @Field("password") String password);*/





    @Multipart
    @POST("docps?check=2")
    Call<UnpaidResponseModel> uploadPaymentDoc(@Part MultipartBody.Part file, @PartMap() Map<String, RequestBody> partMap);



   /* @Multipart
    @POST("project")*/



}
