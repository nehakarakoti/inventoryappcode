package com.app.inventoryapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.ProjectItemModel;
import com.app.inventoryapp.session.InventoryAppSession;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProjectDetailsListAdapterOther extends RecyclerView.Adapter<ProjectDetailsListAdapterOther.MyViewHolder> {
    private Activity activity;
    private InventoryAppSession session;
    private ArrayList<ProjectItemModel> projectItemModelArrayList;


    public ProjectDetailsListAdapterOther(Activity activity, ArrayList<ProjectItemModel> projectItemModelArrayList) {
        this.activity = activity;
        this.projectItemModelArrayList = projectItemModelArrayList;
        this.session = new InventoryAppSession(this.activity);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project_details_other, parent, false);

        return new ProjectDetailsListAdapterOther.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.textView_item.setText(projectItemModelArrayList.get(position).getIteam());

//        if (projectItemModelArrayList.get(position).getQty().length() > 0 || projectItemModelArrayList.get(position).getDone_payment().length() > 0) {
//            holder.mView.setVisibility(View.VISIBLE);
//        } else {
//            holder.mView.setVisibility(View.GONE);
//        }


        String strWorkDoneArray[] = projectItemModelArrayList.get(position).getDone_work().split("\\.");
        String strWorkDone = strWorkDoneArray[0];
        final int workDone = Integer.parseInt(strWorkDone);

        if (workDone == 0) {
            holder.mView.setVisibility(View.GONE);
        } else if (workDone > 0) {
            holder.mView.setVisibility(View.VISIBLE);
        }


        holder.LL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InventoryAppSingleton.getInstance().setItem_id(projectItemModelArrayList.get(position).getId());
                InventoryAppSingleton.getInstance().setItem_rate(projectItemModelArrayList.get(position).getC_rate());
//donework
                UpdateDetailsPhysicalActivity.Ujf = projectItemModelArrayList.get(position).getDone_work();
                /*Setting progress item level*/

                InventoryAppSingleton.getInstance().setFinancialProgressItemlevel(projectItemModelArrayList.get(position).getTotal_payment_done());
                InventoryAppSingleton.getInstance().setPhysicalProgressItemLevel(projectItemModelArrayList.get(position).getTotal_qty_done());
                System.out.println("ITEM LEVEL PROGRESS=======FINANCIAL===" + InventoryAppSingleton.getInstance().getFinancialProgressItemlevel());
                System.out.println("ITEM LEVEL PROGRESS=======PHYSICAL===" + InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel());


                if (session.getKeyUserType().equals("Admin")) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ProjectItemModel", projectItemModelArrayList.get(holder.getAdapterPosition()));
                    activity.startActivity(new Intent(activity, UpdateDetailsActivity.class).putExtras(bundle));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                    System.out.println("OUTPUT  I AM FROM Admin");
                }
                if (session.getKeyUserType().equals("Financial")) {
                    System.out.println("OUTPUT  I AM FROM Financial");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ProjectItemModel", projectItemModelArrayList.get(holder.getAdapterPosition()));
                    activity.startActivity(new Intent(activity, UpdateDetailsFinancialActivity.class).putExtras(bundle));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                }
                if (session.getKeyUserType().equals("Physical")) {
                    System.out.println("OUTPUT  I AM FROM Physical");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ProjectItemModel", projectItemModelArrayList.get(holder.getAdapterPosition()));
                    activity.startActivity(new Intent(activity, UpdateDetailsPhysicalActivity.class).putExtras(bundle));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                }
                if (session.getKeyUserType().equals("Supervisor")) {
                    System.out.println("OUTPUT  I AM FROM Supervisor");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ProjectItemModel", projectItemModelArrayList.get(holder.getAdapterPosition()));
                    activity.startActivity(new Intent(activity, UpdateDetailsSuperActivity.class).putExtras(bundle));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectItemModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.LL_data_container)
        LinearLayout LL_data_container;

        @BindView(R.id.textView_item)
        TextView textView_item;

        @BindView(R.id.LL1)
        RelativeLayout LL1;

        @BindView(R.id.mView)
        TextView mView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
