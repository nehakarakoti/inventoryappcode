package com.app.inventoryapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.font.CustomEditText;
import com.app.inventoryapp.font.CustomFontArialGeoBoldTextView;
import com.app.inventoryapp.model.ItemDataModel;
import com.app.inventoryapp.model.ItemDocModel;
import com.app.inventoryapp.model.ItemEditData;
import com.app.inventoryapp.model.ItemEditResponse;
import com.app.inventoryapp.model.ItemResponse;
import com.app.inventoryapp.model.ProjectItemDetails;
import com.app.inventoryapp.model.ResponseProjectIndividual;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.CommonValidationDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class EditItemActivity extends BaseActivity {


    ProjectItemDetails mProjectItemDetails = new ProjectItemDetails();
    @BindView(R.id.LL_back)
    LinearLayout LLBack;
    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.textSave)
    CustomFontArialGeoBoldTextView textSave;
    @BindView(R.id.LL_edit)
    LinearLayout LLEdit;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.textEditProjectDetails)
    CustomEditText textEditProjectDetails;
    @BindView(R.id.textEditQunatity)
    CustomEditText textEditQunatity;
   /* @BindView(R.id.textEditUnits)
    CustomEditText textEditUnits;*/
    @BindView(R.id.textEditContractorName)
    CustomEditText textEditContractorName;
    @BindView(R.id.textEditRate)
    CustomEditText textEditRate;
    @BindView(R.id.textEditAmount)
    CustomEditText textEditAmount;
    @BindView(R.id.textEditInspectionClause)
    CustomEditText textEditInspectionClause;

    private InventoryAppSession inventoryAppSession;

    AlertDialog.Builder alertDialogBuilder;

    private String unit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        ButterKnife.bind(this);
        inventoryAppSession=new InventoryAppSession(this);

        mProjectItemDetails = (ProjectItemDetails) getIntent().getExtras().getSerializable("ProjectItemDetails");

        System.out.println("DATA========"+mProjectItemDetails.getId());

        intit();

        LLBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(EditItemActivity.this, ProjectDetailsActivity.class));
                finish();
            }
        });

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                enableEditText();
                textSave.setVisibility(View.VISIBLE);
                ivEdit.setVisibility(View.GONE);

            }
        });

        textSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateItemText() && validateQuantity() && validateRate() && validateAmount() && validateInspectionClause())
                {
                    showConfirmationDialog();
                }
            }
        });

        setData();
        disableEditText();


        System.out.println("TESTING=====" + mProjectItemDetails.getIteam());
    }

    private void intit() {
        alertDialogBuilder = new AlertDialog.Builder(EditItemActivity.this);
    }




    private void enableEditText()
    {
        textEditProjectDetails.setEnabled(true);
        textEditQunatity.setEnabled(true);
       // textEditUnits.setEnabled(true);
        textEditContractorName.setEnabled(true);
        textEditRate.setEnabled(true);
        textEditAmount.setEnabled(true);
        textEditInspectionClause.setEnabled(true);
    }

    private void disableEditText()
    {
        textEditProjectDetails.setEnabled(false);
        textEditQunatity.setEnabled(false);
       // textEditUnits.setEnabled(false);
        textEditContractorName.setEnabled(false);
        textEditRate.setEnabled(false);
        textEditAmount.setEnabled(false);
        textEditInspectionClause.setEnabled(false);
    }

    private void setData()
    {
        textEditProjectDetails.setText(""+mProjectItemDetails.getIteam());
        textEditQunatity.setText(""+mProjectItemDetails.getQty());
       // textEditUnits.setText(""+mProjectItemDetails.getUnit());
        textEditContractorName.setText(""+inventoryAppSession.getKeyUserName());
        textEditRate.setText(""+mProjectItemDetails.getC_rate());
        textEditAmount.setText(""+mProjectItemDetails.getC_amount());
        textEditInspectionClause.setText(""+mProjectItemDetails.getRef());
        unit=mProjectItemDetails.getUnit();
    }


    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        messageText.setText("Are you sure to update?");
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                UpdateAPI();
                //new UpdateDetailsPhysicalActivity.UpdateProjectData().execute();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


        /*final android.support.v7.app.AlertDialog.Builder dialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        LayoutInflater LayoutInflater = this.getLayoutInflater();
        final View dialogView = LayoutInflater.inflate(R.layout.dialog_layout, null);


        // ButterKnife.bind(this, dialogView);

        dialogBuilder.setView(dialogView);

        final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();

      // alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();*/
    }


    private void UpdateAPI()
    {
        try
        {

            //http://dharmani.com/railway_api/web/projectds?expand=tblIteams
            final String url = Content.baseURL + "iteams/"+ mProjectItemDetails.getId();

            System.out.println("OUTPUT URL===="+url);
            CommonLoadingDialog.showLoadingDialog(EditItemActivity.this,"Updating....");



            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            //System.out.println("OUTPUT==========="+response);

                            try
                            {
                                JSONObject jsonObject=new JSONObject(response);

                                System.out.println("DATA===="+jsonObject);
                                if(!jsonObject.getBoolean("status"))
                                {

                                    Toast.makeText(EditItemActivity.this, ""+jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    Gson gson=new Gson();
                                    ItemEditResponse itemEditResponse=gson.fromJson(response, ItemEditResponse.class);

                                    if (itemEditResponse.getStatus().equals("true"))
                                    {
                                        Toast.makeText(EditItemActivity.this, "Updated successfully", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(EditItemActivity.this, ProjectDetailsActivity.class));
                                        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                                        finish();
                                    }

                                }

                            }
                            catch (Exception ex)
                            {
                                System.out.println("OUTPUT ======ERROR==" + ex.toString());
                                CommonLoadingDialog.closeLoadingDialog();
                            }



                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("qty", textEditQunatity.getText().toString().trim());
                    params.put("item", textEditProjectDetails.getText().toString());
                    params.put("unit", unit);
                    params.put("c_rate", textEditRate.getText().toString());
                    params.put("c_amount", textEditAmount.getText().toString());
                    params.put("ins_cl", textEditInspectionClause.getText().toString());
                    Log.e("Inventory signin == ", url + params);

                    return params;
                }
            };



            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(EditItemActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {

        }
    }






    /*====================Validation part===============*/

    private boolean validateQuantity()
    {

       if(textEditQunatity.getText().toString().trim().equals(""))
       {
           CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Quantity can't be empty");
            return false;
       }
       else
       {
          Double quantity=Double.parseDouble(textEditQunatity.getText().toString().trim());

           if(quantity==0)
           {
               CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Quantity can't be zero");
               return false;
           }
           return true;
       }



    }

    private boolean validateAmount()
    {
        if(textEditAmount.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Amount can't be empty");
            return false;
        }
        else
        {
           Double amount=Double.parseDouble(textEditAmount.getText().toString().trim());

           if(amount==0.0)
           {
               CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Amount can't be zero");
               return false;
           }
           else
           {

               return true;
           }



        }
        //return true;
    }

    private boolean validateRate()
    {
        if(textEditRate.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Rate can't be empty");
            return false;
        }
        else
        {
            Double amount=Double.parseDouble(textEditRate.getText().toString().trim());

            if(amount==0.0)
            {
                CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Rate can't be zero");
                return false;
            }
            else
            {

                return true;
            }



        }
    }

    private boolean validateInspectionClause()
    {
        if(textEditInspectionClause.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Inspection clause can't be empty");
            return false;
        }
        return true;
    }
    private boolean validateItemText()
    {

        if(textEditProjectDetails.getText().toString().trim().equals(""))
        {
            CommonValidationDialog.showLoadingDialog(EditItemActivity.this,"Item description can't be empty");
            return false;
        }
        return true;
    }


    protected void onResume() {
        super.onResume();
        if(inventoryAppSession.getTimeStamp()!=null){

            if(!inventoryAppSession.getTimeStamp().equals("")) {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");
                String localTime = date2.format(currentLocalTime);
                if(  difftime(localTime,inventoryAppSession.getTimeStamp())>=60){
                    inventoryAppSession.user_logout();
                    loggingOut(EditItemActivity.this);
                }
                else {
                    Calendar cal1 = Calendar.getInstance();
                    Date currentLocalTime1 = cal1.getTime();
                    DateFormat date21 = new SimpleDateFormat("HH:mm a");

                    String localTime1 = date21.format(currentLocalTime1);
                    inventoryAppSession.setCurrentTimeStamp(localTime1);
                }
            }
            else {
                Calendar cal = Calendar.getInstance();
                Date currentLocalTime = cal.getTime();
                DateFormat date2 = new SimpleDateFormat("HH:mm a");

                String localTime = date2.format(currentLocalTime);
                inventoryAppSession.setCurrentTimeStamp(localTime);
            }


        }


    }


}
