package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.inventoryapp.EditUserProject;
import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UserDataModel;
import com.app.inventoryapp.my_interface.UserRemoveProjectListener;
import com.app.inventoryapp.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectUsersListAdapter extends RecyclerView.Adapter<ProjectUsersListAdapter.MyViewHolder> {

    private ArrayList<UserDataModel> userDataModelArrayList = new ArrayList<>();
    private Activity activity;
    private UserRemoveProjectListener listener;

    public ProjectUsersListAdapter(ArrayList<UserDataModel> userDataModelArrayList, Activity activity, UserRemoveProjectListener listener) {
        this.activity = activity;
        this.userDataModelArrayList = userDataModelArrayList;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project_users_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        if (userDataModelArrayList.get(position) != null){
            if (userDataModelArrayList.get(holder.getAdapterPosition()).getName() != null)
                holder.text_name.setText(userDataModelArrayList.get(holder.getAdapterPosition()).getName());
            if (userDataModelArrayList.get(holder.getAdapterPosition()).getUsername() != null)
                holder.text_email.setText(userDataModelArrayList.get(holder.getAdapterPosition()).getUsername());
            if (userDataModelArrayList.get(holder.getAdapterPosition()).getUsertype() != null)
                holder.text_role.setText(Utilities.getUpdatedRoleText(userDataModelArrayList.get(holder.getAdapterPosition()).getUsertype()));

            holder.btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.removeUserFromProject(userDataModelArrayList.get(holder.getAdapterPosition()));
                }
            });

            holder.btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //  System.out.println("OUTPUT USER"+userDataModelArrayList.get(holder.getAdapterPosition()));
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("UserDataModel", userDataModelArrayList.get(holder.getAdapterPosition()));
                    activity.startActivity(new Intent(activity, EditUserProject.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return userDataModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_name)
        TextView text_name;

        @BindView(R.id.text_email)
        TextView text_email;

        @BindView(R.id.text_role)
        TextView text_role;

        @BindView(R.id.btn_remove)
        Button btn_remove;

        @BindView(R.id.btn_edit)
        Button btn_edit;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
