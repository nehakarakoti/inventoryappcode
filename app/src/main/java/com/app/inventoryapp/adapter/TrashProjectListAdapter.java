package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.my_interface.PaginationInterface;
import com.app.inventoryapp.my_interface.ProjectRemoveListener;
import com.app.inventoryapp.my_interface.ProjectDeleteUndoListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.GetStatus;
import com.app.inventoryapp.utils.Utilities;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by android-da on 5/1/18.
 */

public class TrashProjectListAdapter extends RecyclerView.Adapter<TrashProjectListAdapter.MyViewHolder> {
    ProjectDeleteUndoListener mProjectDeleteUndoListener;
    private Activity activity;
    private InventoryAppSession session;
    private ArrayList<ProjectDataModel> projectDataModelArrayList;
    PaginationInterface mPaginationInterface;


    public TrashProjectListAdapter(Activity activity, ArrayList<ProjectDataModel> projectDataModelArrayList,ProjectDeleteUndoListener mProjectDeleteUndoListener,PaginationInterface mPaginationInterface) {
        this.activity = activity;
        this.projectDataModelArrayList = projectDataModelArrayList;
        this.session = new InventoryAppSession(this.activity);
        this.mProjectDeleteUndoListener = mProjectDeleteUndoListener;
        this.mPaginationInterface = mPaginationInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trash_project, parent, false);
        return new MyViewHolder(v);

    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            String starting_date = projectDataModelArrayList.get(position).getStarting_date();
            String ending_date = projectDataModelArrayList.get(position).getEnding_date();

            String financialProgress = projectDataModelArrayList.get(position).getP_total_payment_done();
            String physicalProgress = projectDataModelArrayList.get(position).getP_total_qty_done();


            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(starting_date);
            Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(ending_date);


            //DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            String smonth1 = new DateFormatSymbols().getShortMonths()[date1.getMonth()];
            String smonth2 = new DateFormatSymbols().getShortMonths()[date2.getMonth()];


            Calendar calendar1 = new GregorianCalendar();
            calendar1.setTime(date1);
            int year1 = calendar1.get(Calendar.YEAR);
            int month1 = calendar1.get(Calendar.MONTH) + 1;
            int day1 = calendar1.get(Calendar.DAY_OF_MONTH);


            Calendar calendar2 = new GregorianCalendar();
            calendar2.setTime(date2);
            int year2 = calendar2.get(Calendar.YEAR);
            int month2 = calendar2.get(Calendar.MONTH) + 1;
            int day2 = calendar2.get(Calendar.DAY_OF_MONTH);


            System.out.println("RITUPARNA  " + day2 + "-" + smonth2 + "" + year2);
            System.out.println("RITUPARNA  " + day1 + "-" + smonth1 + "" + year1);


            long diff = GetStatus.dayDiff(ending_date);
            System.out.println("DIFFERENCE======================" + diff);


            if (diff >= 0) {
                holder.viewStatus.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_chart_green));
            } else {
                holder.viewStatus.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_red));
            }


            holder.tv_projectDate.setText(+day1 + "-" + smonth1 + "-" + year1 + " to " + day2 + "-" + smonth2 + "-" + year2);

            //holder.tv_projectDate.setText(Utilities.convertOneToAnotherFormat(starting_date) + " to " + Utilities.convertOneToAnotherFormat(ending_date));


            holder.tv_projectName.setText(projectDataModelArrayList.get(position).getProject_name());

            holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProjectDeleteUndoListener.deleteProject(projectDataModelArrayList.get(position).getId());
                }
            });

            holder.txtUndo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProjectDeleteUndoListener.undoProject(projectDataModelArrayList.get(position).getId(),projectDataModelArrayList.get(position).getStarting_date(),projectDataModelArrayList.get(position).getEnding_date());
                }
            });




            if (holder.getAdapterPosition() == projectDataModelArrayList.size() - 1){
                mPaginationInterface.loadMorePage();
            }

        } catch (Exception ex) {

        }

    }

    @Override
    public int getItemCount() {
        return projectDataModelArrayList.size();
    }


    public void updateAdapter(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        this.projectDataModelArrayList = projectDataModelArrayList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_projectName)
        TextView tv_projectName;

        @BindView(R.id.tv_projectDate)
        TextView tv_projectDate;

        @BindView(R.id.LL_itemContainer)
        LinearLayout LL_itemContainer;

        @BindView(R.id.viewStatus)
        View viewStatus;

        @BindView(R.id.RL_container)
        RelativeLayout RL_container;

        @BindView(R.id.txtDelete)
        TextView txtDelete;


        @BindView(R.id.txtUndo)
        TextView txtUndo;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
