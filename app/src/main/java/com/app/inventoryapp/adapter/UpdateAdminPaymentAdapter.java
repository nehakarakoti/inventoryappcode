package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.UpdatePaymentByAdminActivity;
import com.app.inventoryapp.model.DataItem;
import com.app.inventoryapp.my_interface.PaymentDocUploadInterface;
import com.app.inventoryapp.my_interface.UpdateDetailsInterface;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.Content;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UpdateAdminPaymentAdapter extends RecyclerView.Adapter<UpdateAdminPaymentAdapter.MyViewHolder> {
    Activity activity;
    InventoryAppSession session;
    UpdateDetailsInterface listener;
    PaymentDocUploadInterface listener2;
    String user_id, qnt, subItem, numberAsString;
    private ArrayList<DataItem> dataItemArrayList;
    private AlertDialog.Builder alertDialogBuilder;


    public UpdateAdminPaymentAdapter(Activity activity, ArrayList<DataItem> dataItemArrayList) {
        this.activity = activity;
        this.dataItemArrayList = dataItemArrayList;
        listener = (UpdatePaymentByAdminActivity) activity;
        listener2 = (UpdatePaymentByAdminActivity) activity;
        session = new InventoryAppSession(activity);
        alertDialogBuilder = new AlertDialog.Builder(this.activity);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_by_admin, parent, false);

        return new UpdateAdminPaymentAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        try {
            String date = dataItemArrayList.get(position).getCreatedOn().trim().split(" ")[0];
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            String smonth = new DateFormatSymbols().getShortMonths()[date1.getMonth()];
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            holder.textView_date.setText("Date: " + day + "-" + smonth + "-" + year);
            holder.textView_itemQuantity.setText("Quantity: " + dataItemArrayList.get(position).getQtyDone().trim());
            Double quantity = Double.parseDouble(dataItemArrayList.get(position).getQtyDone().trim());
            System.out.println("RATE OF ITEM====" + UpdatePaymentByAdminActivity.rate_per_unit);
            final Double amount = quantity * UpdatePaymentByAdminActivity.rate_per_unit;
            numberAsString = String.format("%.2f", amount);
            holder.textView_itemAmount.setText("Amount: " + numberAsString);

            user_id = "" + dataItemArrayList.get(position).getUserId();
            qnt = dataItemArrayList.get(position).getQtyDone().trim();

            subItem = "" + dataItemArrayList.get(position).getId();

            /*Set Financial Comment*/
            if (dataItemArrayList.get(position).getFuser_comment().length() > 0) {
                holder.txtItemCommentValueTV.setText("Comment: " + dataItemArrayList.get(position).getFuser_comment());
            } else {
            }


            holder.btn_payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Double quantity = Double.parseDouble(dataItemArrayList.get(position).getQtyDone().trim());
                    final Double amount = quantity * UpdatePaymentByAdminActivity.rate_per_unit;
                    String user_id = "" + dataItemArrayList.get(position).getUserId();
                    String qnt = dataItemArrayList.get(position).getQtyDone().trim();
                    String subItem = "" + dataItemArrayList.get(position).getId();
                    String itSelfID = "" + dataItemArrayList.get(position).getId();

                    listener2.showPaymentUploadDialog(itSelfID, amount, user_id, qnt, subItem);
                }
            });

            String fileExtension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(dataItemArrayList.get(position).getDocName());

            System.out.println("IAMGE EXTENSION======" + fileExtension);

            if (fileExtension.equals("pdf")) {
                holder.itemImage.setBackground(ContextCompat.getDrawable(activity, R.drawable.doc_image));
            } else {
                Picasso.with(activity).load(Content.baseURL + dataItemArrayList.get(position).getDocName())
                        .placeholder(R.drawable.place_holder2)
                        .resize(300, 300)
                        .error(R.drawable.place_holder2)
                        .skipMemoryCache().transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        int size = Math.min(source.getWidth(), source.getHeight());
                        int x = (source.getWidth() - size) / 2;
                        int y = (source.getHeight() - size) / 2;
                        Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
                        if (result != source) {
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "front";
                    }
                }).into(holder.itemImage);
            }

        } catch (Exception ex) {

        }


    }


    @Override
    public int getItemCount() {
        return dataItemArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemImage)
        ImageView itemImage;
        @BindView(R.id.btn_payment)
        Button btn_payment;
        @BindView(R.id.textView_itemAmount)
        TextView textView_itemAmount;
        @BindView(R.id.textView_date)
        TextView textView_date;
        @BindView(R.id.textView_itemQuantity)
        TextView textView_itemQuantity;
        @BindView(R.id.txtItemCommentValueTV)
        TextView txtItemCommentValueTV;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }


}


