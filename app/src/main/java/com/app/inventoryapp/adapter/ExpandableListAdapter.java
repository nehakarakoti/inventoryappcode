package com.app.inventoryapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inventoryapp.AddUserProjectActivity;
import com.app.inventoryapp.EditUser;
import com.app.inventoryapp.R;
import com.app.inventoryapp.model.SupervisorWithPhysicalUsers;
import com.app.inventoryapp.model.UsersResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Dharmani Apps on 9/15/2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    String TAG = "ExpandableListAdapter";
    boolean isDetails;
    private Context _context;
    private ArrayList<SupervisorWithPhysicalUsers> modelArrayList; // header titles

    public ExpandableListAdapter(Context context, ArrayList<SupervisorWithPhysicalUsers> modelArrayList, boolean isDetails) {
        this._context = context;
        this.modelArrayList = modelArrayList;
        this.isDetails = isDetails;
    }

    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<UsersResponse> chList = modelArrayList.get(groupPosition).getmSubPhysicalUserAL();
        return chList.get(childPosition);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_user_list, null);
            holder = new ChildViewHolder();
            convertView.setTag(holder);
        }
        holder = (ChildViewHolder) convertView.getTag();
        UsersResponse mChildModel = (UsersResponse) getChild(groupPosition, childPosition);

        holder.roleTextView = (TextView) convertView.findViewById(R.id.roleTextView);
        holder.emailTextView = (TextView) convertView.findViewById(R.id.emailTextView);
        holder.nameTextView = (TextView) convertView.findViewById(R.id.nameTextView);
        holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
        holder.LL_user_item = (LinearLayout) convertView.findViewById(R.id.LL_user_item);

        holder.emailTextView.setText(mChildModel.getUsername());
        holder.roleTextView.setText(mChildModel.getUsertype());
        holder.nameTextView.setText(mChildModel.getName());


        if (mChildModel.isChecked()) {
            holder.checkbox.setChecked(true);
            //usersResponseArrayList.get(position).setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
            //usersResponseArrayList.get(position).setChecked(false);
        }


    /*    holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkbox);
                if (checkBox.isChecked()) {
                    *//*Here U need to check whether user has identical user is add or not and should not more than 3*//*
                    modelArrayList.get(groupPosition).getmSubPhysicalUserAL().get(childPosition).setChecked(true);
                    AddUserProjectActivity.finalList.add(usersResponseArrayList.get(finalHolder.getAdapterPosition()));
                    usersResponseArrayListCarrier.add(usersResponseArrayList.get(finalHolder.getAdapterPosition()));
                    mSelectedUserInterface.mSelectedUserInterface(usersResponseArrayListCarrier);
                } else {
                    usersResponseArrayList.get(finalHolder.getAdapterPosition()).setChecked(false);
                    AddUserProjectActivity.finalList.remove(usersResponseArrayList.get(finalHolder.getAdapterPosition()));
                    usersResponseArrayListCarrier.remove(usersResponseArrayList.get(finalHolder.getAdapterPosition()));
                    mSelectedUserInterface.mSelectedUserInterface(usersResponseArrayListCarrier);
                }
            }
        });*/


        holder.LL_user_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("UsersResponse", usersResponseArrayList.get(holder.getAdapterPosition()));
//                if (!usersResponseArrayList.get(holder.getAdapterPosition()).getPassword().equals("")) {
//                    //    bundle.putSerializable("UserHasPassword", usersResponseArrayList.get(holder.getAdapterPosition()).getPassword());
//                    bundle.putString("password", usersResponseArrayList.get(holder.getAdapterPosition()).getPassword());
//                }
//                activity.startActivity(new Intent(activity, EditUser.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));

            }
        });


        return convertView;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.modelArrayList.get(groupPosition).getmSubPhysicalUserAL().size();
    }

    @Override
    public SupervisorWithPhysicalUsers getGroup(int groupPosition) {
        return this.modelArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.modelArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, final ViewGroup parent) {
        ParentViewHolder holder = null;
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_supervisor, null);
            holder = new ParentViewHolder();
            convertView.setTag(holder);
        }
        holder = (ParentViewHolder) convertView.getTag();

        final SupervisorWithPhysicalUsers mSupervisorWithPhysicalUsers = modelArrayList.get(groupPosition);
        holder.itemTxtSupervisorTV = (TextView) convertView.findViewById(R.id.itemTxtSupervisorTV);
        holder.itemTxtSupervisorTV.setText(mSupervisorWithPhysicalUsers.getName());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public class ParentViewHolder {
        public TextView itemTxtSupervisorTV;
    }

    public class ChildViewHolder {
        public  TextView roleTextView;
        public  TextView emailTextView;
        public  TextView nameTextView;
        public  LinearLayout LL_user_item;
        public  CheckBox checkbox;
    }

}