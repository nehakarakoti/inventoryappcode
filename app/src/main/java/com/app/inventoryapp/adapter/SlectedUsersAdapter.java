package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.ClipData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UsersResponse;

import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/16/2018.
 */

public class SlectedUsersAdapter extends BaseAdapter
{
    private Activity activity;
    private ArrayList<UsersResponse> usersResponseArrayList;

    public SlectedUsersAdapter(ArrayList<UsersResponse> usersResponseArrayList,Activity activity)
    {
            this.activity=activity;
            this.usersResponseArrayList=usersResponseArrayList;

            System.out.println("SIZE OF SELECTED USER===="+usersResponseArrayList.size());

    }

    @Override
    public int getCount() {
        return usersResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return usersResponseArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       // nameTextView
        if (convertView == null)
        {
            convertView = LayoutInflater.from(activity).
                    inflate(R.layout.item_user_list_selected, parent, false);
        }


        TextView nameTextView,roleTextView;

        nameTextView=(TextView)convertView.findViewById(R.id.nameTextView) ;
        roleTextView=(TextView)convertView.findViewById(R.id.nameTextView);

        nameTextView.setText(usersResponseArrayList.get(position).getName());
        roleTextView.setText(usersResponseArrayList.get(position).getRole());

      //  System.out.println("OUT===========NAME==="+usersResponseArrayList.get(position).getRole());
        System.out.println("OUT===========ROLE===XYZ"+usersResponseArrayList.get(position).getName());

        // returns the view for the current row
        return convertView;

    }
}
