package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.EditItemActivity;
import com.app.inventoryapp.R;
import com.app.inventoryapp.UpdateDetailsActivity;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.ProjectItemDetails;
import com.app.inventoryapp.my_interface.ItemDeleteListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.ProjectData;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import volley.PhysicalUser;

/**
 * Created by android-da on 5/2/18.
 */

public class ProjectDetailsListAdapter extends RecyclerView.Adapter<ProjectDetailsListAdapter.MyViewHoilder> {

    private Activity activity;
    private InventoryAppSession session;
    private ArrayList<ProjectItemDetails> projectItemModelArrayList;
    private ItemDeleteListener mItemDeleteListener;

    public ProjectDetailsListAdapter(Activity activity, ArrayList<ProjectItemDetails> projectItemModelArrayList, ItemDeleteListener mItemDeleteListener) {
        this.activity = activity;
        this.projectItemModelArrayList = projectItemModelArrayList;
        this.session = new InventoryAppSession(this.activity);
        this.mItemDeleteListener = mItemDeleteListener;
    }

    @Override
    public MyViewHoilder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_project_details, parent, false);

        return new MyViewHoilder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHoilder holder, final int position) {
        System.out.println("OUTPUT====== WORK DONE===" + projectItemModelArrayList.get(position).getTotal_qty_done());
        holder.textView_item.setText(projectItemModelArrayList.get(position).getIteam());


        if (session.getKeyUserType().equals("Admin")) {
            holder.llEditDeleteLL.setVisibility(View.VISIBLE);
        } else {
            holder.llEditDeleteLL.setVisibility(View.GONE);
        }

        //final Float workDone = Float.parseFloat(projectItemModelArrayList.get(position).getDone_work());

        String strWorkDoneArray[] = projectItemModelArrayList.get(position).getDone_work().split("\\.");
        String strWorkDone = strWorkDoneArray[0];
        final int workDone = Integer.parseInt(strWorkDone);

        if (workDone == 0) {
            holder.mView.setVisibility(View.GONE);
        } else if (workDone > 0) {
            holder.mView.setVisibility(View.VISIBLE);
        }


        holder.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workDone == 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ProjectItemDetails", projectItemModelArrayList.get(position));
                    activity.startActivity(new Intent(activity, EditItemActivity.class).putExtras(bundle).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    activity.finish();
                } else {
                    Toast.makeText(activity, "Already Updated Quantity, You can not update the quantity!", Toast.LENGTH_LONG).show();
                }


            }
        });

        holder.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (workDone == 0) {
                    mItemDeleteListener.itemDelete(projectItemModelArrayList.get(position).getId());
                } else {
                    Toast.makeText(activity, "Already Updated Quantity, You can not Delete the project item!", Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.LL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InventoryAppSingleton.getInstance().setItem_id(projectItemModelArrayList.get(position).getId());

                InventoryAppSingleton.getInstance().setItem_rate(projectItemModelArrayList.get(position).getC_rate());
                InventoryAppSingleton.getInstance().setFinancialProgressItemlevel(projectItemModelArrayList.get(position).getTotal_payment_done());
                InventoryAppSingleton.getInstance().setPhysicalProgressItemLevel(projectItemModelArrayList.get(position).getTotal_qty_done());

                System.out.println("ITEM LEVEL PROGRESS=======FINANCIAL===" + InventoryAppSingleton.getInstance().getFinancialProgressItemlevel());
                System.out.println("ITEM LEVEL PROGRESS=======PHYSICAL===" + InventoryAppSingleton.getInstance().getPhysicalProgressItemLevel());

                if (session.getKeyUserType().equals("Admin") || session.getKeyUserType().equals("Supervisor")) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("ProjectItemModel", projectItemModelArrayList.get(holder.getAdapterPosition()));
                    activity.startActivity(new Intent(activity, UpdateDetailsActivity.class).putExtras(bundle));
                    activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    System.out.println("OUTPUT  I AM FROM Admin");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return projectItemModelArrayList.size();
    }

    public class MyViewHoilder extends RecyclerView.ViewHolder {
        @BindView(R.id.LL_data_container)
        LinearLayout LL_data_container;
        @BindView(R.id.llTittle)
        LinearLayout llTittle;
        @BindView(R.id.textView_item)
        TextView textView_item;

        @BindView(R.id.recyclerviewInner)
        RecyclerView recyclerviewInner;
        @BindView(R.id.ivProgress)
        ProgressBar ivProgress;

        @BindView(R.id.txtEdit)
        TextView txtEdit;

        @BindView(R.id.txtDelete)
        TextView txtDelete;

        @BindView(R.id.LL1)
        RelativeLayout LL1;

        @BindView(R.id.btndetails)
        Button btndetails;
        @BindView(R.id.llEditDeleteLL)
        LinearLayout llEditDeleteLL;

        @BindView(R.id.mView)
        TextView mView;
        String url = "http://dharmani.com/railway_api/web/doc/getitemupdate?id=";
        ArrayList<PhysicalUser> mlist = new ArrayList<>();

        public MyViewHoilder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            btndetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerviewInner.getVisibility() == View.GONE) {
                        ivProgress.setVisibility(View.VISIBLE);
                        String id = projectItemModelArrayList.get(getAdapterPosition()).getId();

                        try {


                            StringRequest stringRequest = new StringRequest(Request.Method.GET, url + id,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if (ivProgress.getVisibility() == View.VISIBLE)
                                                ivProgress.setVisibility(View.GONE);
                                            try {
                                                JSONObject jsonObject = new JSONObject(response);

                                                if (jsonObject.getBoolean("status")) {

                                                    Gson gson = new Gson();
                                                    mlist.clear();
                                                    ProjectData projectResponse = gson.fromJson(response, ProjectData.class);


                                                    mlist.clear();

                                                    ArrayList<PhysicalUser> mTempAL = new ArrayList();
                                                    for (int i = 0; i < projectResponse.getData().size(); i++) {
                                                        PhysicalUser mModel = projectResponse.getData().get(i);
                                                        mlist.add(mModel);
                                                    }
                                                    SummaryListAdapter summaryListAdapter = new SummaryListAdapter(activity, mlist);
                                                    recyclerviewInner.setLayoutManager(new LinearLayoutManager(activity));
                                                    recyclerviewInner.setAdapter(summaryListAdapter);
                                                    llTittle.setVisibility(View.VISIBLE);
                                                    recyclerviewInner.setVisibility(View.VISIBLE);


                                                    if (mlist.size() <= 0) {
                                                        llTittle.setVisibility(View.GONE);
                                                        recyclerviewInner.setVisibility(View.GONE);

                                                        Toast.makeText(activity, " Item not Updated ", Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                Toast.makeText(activity, " Item not Updated ", Toast.LENGTH_LONG).show();

                                                if (ivProgress.getVisibility() == View.VISIBLE)
                                                    ivProgress.setVisibility(View.GONE);

                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            System.out.println("OUTPUT ======ERROR==" + error.toString());
//                                        if (CommonLoadingDialog.alertDialog != null)
//                                            CommonLoadingDialog.closeLoadingDialog();
                                        }
                                    }) {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    HashMap<String, String> headers = new HashMap<String, String>();
                                    System.out.println("HELLO HEDER===" + headers);
                                    return headers;
                                }
                            };


                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            RequestQueue requestQueue = Volley.newRequestQueue(activity);
                            stringRequest.setShouldCache(false);
                            requestQueue.add(stringRequest);
                        } catch (Exception ex) {


                        }

////                    llTittle.setVisibility(View.VISIBLE);
                    } else if (recyclerviewInner.getVisibility() == View.VISIBLE) {
                        ivProgress.setVisibility(View.GONE);
                        recyclerviewInner.setVisibility(View.GONE);
                        llTittle.setVisibility(View.GONE);
                    }
//
//else{
//                    ivProgress.setVisibility(View.GONE);
//                  //  llTittle.setVisibility(View.GONE);
//                }
//         url= url + projectItemModelArrayList.get(getAdapterPosition()).getId();
//                try {
//
//
//                    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
//                            new Response.Listener<String>() {
//                                @Override
//                                public void onResponse(String response) {
//if(ivProgress.getVisibility()==View.VISIBLE)
//                                    ivProgress.setVisibility(View.GONE);
//                                    try {
//                                        JSONObject jsonObject = new JSONObject(response);
//
//                                        if (jsonObject.getBoolean("status")) {
//
//                                            Gson gson = new Gson();
//                                            mlist.clear();
//                                            ProjectData projectResponse = gson.fromJson(response, ProjectData.class);
//
//
//                                            mlist.clear();
//
//                                            ArrayList<PhysicalUser> mTempAL = new ArrayList();
//                                            for (int i = 0; i < projectResponse.getData().size(); i++) {
//                                                PhysicalUser mModel = projectResponse.getData().get(i);
//                                                mlist.add(mModel);
//                                            }
//                                            SummaryListAdapter summaryListAdapter=new SummaryListAdapter(activity,mlist);
//                                            recyclerviewInner.setLayoutManager(new LinearLayoutManager(activity));
//                                            recyclerviewInner.setAdapter(summaryListAdapter);
//                                            llTittle.setVisibility(View.VISIBLE);
//                                            recyclerviewInner.setVisibility(View.VISIBLE);
//
//
//                                            if(mlist.size()<=0) {
//                                                llTittle.setVisibility(View.GONE);
//                                                recyclerviewInner.setVisibility(View.GONE);
//                                                Toast.makeText(activity,"sorry!no Data available",Toast.LENGTH_LONG).show();
//                                            }
//                                        }
//                                    } catch (Exception ex) {
//                                        if(ivProgress.getVisibility()==View.VISIBLE)
//                                        ivProgress.setVisibility(View.GONE);
//                                        llTittle.setVisibility(View.GONE);
//                                        recyclerviewInner.setVisibility(View.GONE);
//                                    }
//                                }
//                            },
//                            new Response.ErrorListener() {
//                                @Override
//                                public void onErrorResponse(VolleyError error) {
//                                    System.out.println("OUTPUT ======ERROR==" + error.toString());
//                                    if (CommonLoadingDialog.alertDialog != null)
//                                        CommonLoadingDialog.closeLoadingDialog();
//                                }
//                            }) {
//                        @Override
//                        public Map<String, String> getHeaders() throws AuthFailureError {
//                            HashMap<String, String> headers = new HashMap<String, String>();
//                            System.out.println("HELLO HEDER===" + headers);
//                            return headers;
//                        }
//                    };
//
//
//                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
//                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                    RequestQueue requestQueue = Volley.newRequestQueue(activity);
//                    stringRequest.setShouldCache(false);
//                    requestQueue.add(stringRequest);
//                } catch (Exception ex) {
//
//
//                }

                }
            });
        }
    }

}
