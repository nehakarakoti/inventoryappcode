package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inventoryapp.AddUserProjectActivity;
import com.app.inventoryapp.EditUser;
import com.app.inventoryapp.PhysicalUserActivity;
import com.app.inventoryapp.R;
import com.app.inventoryapp.SelectedUserInterface;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.PhysicalSelectedUserInterface;
import com.app.inventoryapp.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dharmaniz on 2/5/18.
 */

public class InnerPhysicalListAdapter extends RecyclerView.Adapter<InnerPhysicalListAdapter.MyViewHolder> implements Filterable {


    public ArrayList<UsersResponse> usersResponseArrayListCarrier = new ArrayList<>();
    PhysicalSelectedUserInterface mSelectedUserInterface;
    private Activity activity;
    private ArrayList<UsersResponse> usersResponseArrayList;
    private ArrayList<UsersResponse> usersResponseArrayRestoreList = new ArrayList<>();

    public InnerPhysicalListAdapter(Activity activity, ArrayList<UsersResponse> usersResponseArrayList, PhysicalSelectedUserInterface mSelectedUserInterface) {
        this.activity = activity;
        this.usersResponseArrayList = usersResponseArrayList;
        this.usersResponseArrayRestoreList = usersResponseArrayList;
        this.mSelectedUserInterface = mSelectedUserInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        for (int i = 0; i < InventoryAppSingleton.getInstance().getUsersResponseArrayList().size(); i++) {
//            if (usersResponseArrayList.get(position).getId() == InventoryAppSingleton.getInstance().getUsersResponseArrayList().get(i).getId()) {
//                usersResponseArrayList.get(position).setChecked(true);
//
//            }
//        }

        final UsersResponse usersResponse = usersResponseArrayList.get(position);


        // holder.emailTextView.setText(usersResponse.getUsername()+usersResponse.getId());
        holder.emailTextView.setText(usersResponse.getUsername());
        holder.roleTextView.setText(Utilities.getUpdatedRoleText(usersResponse.getUsertype()));
        holder.nameTextView.setText(usersResponse.getName());


        if (usersResponse.isChecked()) {
            holder.checkbox.setChecked(true);
            //usersResponseArrayList.get(position).setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
            //usersResponseArrayList.get(position).setChecked(false);
        }


        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkbox);
                if (checkBox.isChecked()) {
                    /*Here U need to check whether user has identical user is add or not and should not more than 3*/
                    usersResponseArrayList.get(holder.getAdapterPosition()).setChecked(true);
                    AddUserProjectActivity.finalList.add(usersResponseArrayList.get(holder.getAdapterPosition()));
                    usersResponseArrayListCarrier.add(usersResponseArrayList.get(holder.getAdapterPosition()));
                    mSelectedUserInterface.mSelectedUserInterface(usersResponseArrayListCarrier,true,usersResponse);
                } else {
                    usersResponseArrayList.get(holder.getAdapterPosition()).setChecked(false);
                    AddUserProjectActivity.finalList.remove(usersResponseArrayList.get(holder.getAdapterPosition()));
                    usersResponseArrayListCarrier.remove(usersResponseArrayList.get(holder.getAdapterPosition()));
                    mSelectedUserInterface.mSelectedUserInterface(usersResponseArrayListCarrier,false,usersResponse);
                }
            }
        });




        holder.LL_user_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("UsersResponse", usersResponseArrayList.get(holder.getAdapterPosition()));
                if (!usersResponseArrayList.get(holder.getAdapterPosition()).getPassword().equals("")) {
                    //    bundle.putSerializable("UserHasPassword", usersResponseArrayList.get(holder.getAdapterPosition()).getPassword());
                    bundle.putString("password", usersResponseArrayList.get(holder.getAdapterPosition()).getPassword());
                }
                activity.startActivity(new Intent(activity, EditUser.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));

            }
        });


    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    usersResponseArrayList = usersResponseArrayRestoreList;
                } else {
                    ArrayList<UsersResponse> filteredList = new ArrayList<>();
                    for (UsersResponse row : usersResponseArrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    usersResponseArrayList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = usersResponseArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                usersResponseArrayList = (ArrayList<UsersResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return usersResponseArrayList.size();
    }

    private boolean checkedIdenticalUser(UsersResponse usersResponse) {

        System.out.println("USER TYPE=======================================");
        for (int i = 0; i < usersResponseArrayListCarrier.size(); i++) {
            if (usersResponseArrayListCarrier.get(i).getUsertype().equals(usersResponse.getUsertype())) {
                // usersResponseArrayListCarrier.remove(usersResponse);
                System.out.println("OUTPUT OF================= REMOVE==" + usersResponse.getUsertype());
                return true;
            }
        }
        return false;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.roleTextView)
        TextView roleTextView;

        @BindView(R.id.emailTextView)
        TextView emailTextView;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.LL_user_item)
        LinearLayout LL_user_item;


        @BindView(R.id.checkbox)
        CheckBox checkbox;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            // this.setIsRecyclable(false);
        }
    }
}
