package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.inventoryapp.ProjectDetailsActivity;
import com.app.inventoryapp.ProjectDetailsOtherActivity;
import com.app.inventoryapp.R;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.ProjectDataModel;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.GetStatus;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectListAdapterOther extends RecyclerView.Adapter<ProjectListAdapterOther.MyViewHolder> {

    private Activity activity;
    private InventoryAppSession session;
    private ArrayList<ProjectDataModel> projectDataModelArrayList;


    public ProjectListAdapterOther(Activity activity, ArrayList<ProjectDataModel> projectDataModelArrayList) {
        this.activity = activity;
        this.projectDataModelArrayList = projectDataModelArrayList;
        this.session = new InventoryAppSession(this.activity);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project_list_other, parent, false);
        return new ProjectListAdapterOther.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        try {

            String ending_date = projectDataModelArrayList.get(position).getEnding_date();

            String extendeddate = projectDataModelArrayList.get(position).getEcd();


//            Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(ending_date);
//
//            //DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
//            String smonth1 = new DateFormatSymbols().getShortMonths()[date1.getMonth()];
//            String smonth2 = new DateFormatSymbols().getShortMonths()[date2.getMonth()];
//
//            Calendar calendar1 = new GregorianCalendar();
//            calendar1.setTime(date1);
//            int year1 = calendar1.get(Calendar.YEAR);
//            int month1 = calendar1.get(Calendar.MONTH) + 1;
//            int day1 = calendar1.get(Calendar.DAY_OF_MONTH);
//
//            Calendar calendar2 = new GregorianCalendar();
//            calendar2.setTime(date2);
//            int year2 = calendar2.get(Calendar.YEAR);
//            int month2 = calendar2.get(Calendar.MONTH) + 1;
//            int day2 = calendar2.get(Calendar.DAY_OF_MONTH);

//
//            System.out.println("RITUPARNA  " + day2 + "-" + smonth2 + "" + year2);
//            System.out.println("RITUPARNA  " + day1 + "-" + smonth1 + "" + year1);
            if(!projectDataModelArrayList.get(position).getEcd().equals("")){
                Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(extendeddate);

                String smonth1 = new DateFormatSymbols().getShortMonths()[date1.getMonth()];

                Calendar calendar1 = new GregorianCalendar();
                   calendar1.setTime(date1);
                int year1 = calendar1.get(Calendar.YEAR);
                int month1 = calendar1.get(Calendar.MONTH) + 1;
                int day1 = calendar1.get(Calendar.DAY_OF_MONTH);
                Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(ending_date);



                String smonth2 = new DateFormatSymbols().getShortMonths()[date2.getMonth()];



//+day1 + "-" + smonth1 + "-" + year1 + " to " +

                Calendar calendar2 = new GregorianCalendar();
                calendar2.setTime(date2);
                int year2 = calendar2.get(Calendar.YEAR);
                int month2 = calendar2.get(Calendar.MONTH) + 1;
                int day2 = calendar2.get(Calendar.DAY_OF_MONTH);
                holder.tv_projectDate.setText( day2 + "-" + smonth2 + "-" + year2+ " to " +day1 + "-" + smonth1 + "-" + year1  );
            }

            else {
                Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(ending_date);
                String smonth2 = new DateFormatSymbols().getShortMonths()[date2.getMonth()];

                Calendar calendar2 = new GregorianCalendar();
                calendar2.setTime(date2);
                int year2 = calendar2.get(Calendar.YEAR);
                int month2 = calendar2.get(Calendar.MONTH) + 1;
                int day2 = calendar2.get(Calendar.DAY_OF_MONTH);
                holder.tv_projectDate.setText( day2 + "-" + smonth2 + "-" + year2);

            }


            long diff = GetStatus.dayDiff(ending_date);
            if (diff >= 0) {
                holder.viewStatus.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_chart_green));
            } else {
                holder.viewStatus.setBackgroundColor(ContextCompat.getColor(activity, R.color.color_red));
            }

           // holder.tv_projectDate.setText(+day1 + "-" + smonth1 + "-" + year1 + " to " + day2 + "-" + smonth2 + "-" + year2);
            holder.tv_projectName.setText(projectDataModelArrayList.get(position).getProject_name());

            holder.RL_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    /*Setting project id in singletone for future use*/
                    InventoryAppSingleton.getInstance().setProject_id(projectDataModelArrayList.get(position).getId());
                    InventoryAppSingleton.getInstance().setFinancialProgress(projectDataModelArrayList.get(position).getP_total_payment_done());
                    InventoryAppSingleton.getInstance().setPhysicalProgress(projectDataModelArrayList.get(position).getP_total_qty_done());


                    String starting_date = projectDataModelArrayList.get(position).getStarting_date();
                    String ending_date = projectDataModelArrayList.get(position).getEnding_date();

                    InventoryAppSingleton.getInstance().setStarting_date(starting_date);
                    InventoryAppSingleton.getInstance().setEnding_date(ending_date);
                    long diff = GetStatus.dayDiff(ending_date);

                    InventoryAppSingleton.getInstance().setDiff(diff);


                    if (session.getKeyUserType().equals("Admin")) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("ProjectDataModel", projectDataModelArrayList.get(position));

                        activity.startActivity(new Intent(activity, ProjectDetailsActivity.class).putExtras(bundle));
                        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);

                    }
                    if (session.getKeyUserType().equals("Physical")) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("ProjectDataModel", projectDataModelArrayList.get(position));
                        activity.startActivity(new Intent(activity, ProjectDetailsOtherActivity.class).putExtras(bundle));
                        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    }
                    if (session.getKeyUserType().equals("Financial")) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("ProjectDataModel", projectDataModelArrayList.get(position));
                        activity.startActivity(new Intent(activity, ProjectDetailsOtherActivity.class).putExtras(bundle));
                        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    }
                    if (session.getKeyUserType().equals("Supervisor")) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("ProjectDataModel", projectDataModelArrayList.get(position));

                        activity.startActivity(new Intent(activity, ProjectDetailsActivity.class).putExtras(bundle));
                        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    }

                }
            });
        } catch (Exception ex) {
            System.out.println("ERROR=====" + ex);
        }
    }

    @Override
    public int getItemCount() {
        return projectDataModelArrayList.size();
    }

    public void updateAdapter(ArrayList<ProjectDataModel> projectDataModelArrayList) {
        this.projectDataModelArrayList = projectDataModelArrayList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_projectName)
        TextView tv_projectName;

        @BindView(R.id.tv_projectDate)
        TextView tv_projectDate;

        @BindView(R.id.LL_itemContainer)
        LinearLayout LL_itemContainer;

        @BindView(R.id.viewStatus)
        View viewStatus;

        @BindView(R.id.RL_container)
        RelativeLayout RL_container;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
