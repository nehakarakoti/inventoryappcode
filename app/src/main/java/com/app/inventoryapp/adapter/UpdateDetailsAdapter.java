package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.inventoryapp.R;
import com.app.inventoryapp.model.DummyUpdateDetailsModel;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by RITUPARNA on 5/11/2018.
 */

public class UpdateDetailsAdapter extends RecyclerView.Adapter<UpdateDetailsAdapter.MyViewHolder>
{
    private Activity activity;
    private ArrayList<DummyUpdateDetailsModel> dummyUpdateDetailsModelArrayList;

    public UpdateDetailsAdapter(Activity activity, ArrayList<DummyUpdateDetailsModel> dummyUpdateDetailsModelArrayList)
    {
        this.activity=activity;
        this.dummyUpdateDetailsModelArrayList=dummyUpdateDetailsModelArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_update_details,parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {

    }

    @Override
    public int getItemCount() {
        return dummyUpdateDetailsModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);
        }
    }
}
