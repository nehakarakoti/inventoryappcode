package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.DocumentViewActivity;
import com.app.inventoryapp.R;
import com.app.inventoryapp.UpdatePaymentByAdminActivity;
import com.app.inventoryapp.font.CustomTextView;
import com.app.inventoryapp.model.HistoryDataItem;
import com.app.inventoryapp.model.HistoryModel;
import com.app.inventoryapp.model.HistoryPayment;
import com.app.inventoryapp.my_interface.PaymentRejectListener;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import volley.ReviewStatus;

import static android.view.View.GONE;

/**
 * Created by android-da on 5/2/18.
 */

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.MyViewHolder> {


    private Activity activity;
    private ArrayList<HistoryModel> historyModelArrayList;
    private InventoryAppSession session;
    private List<HistoryDataItem> historyDataItemArrayList;
    private String paymentImage = "";
    private PaymentRejectListener listener;
    private InventoryAppSession inventoryAppSession;

    public HistoryListAdapter(Activity activity, List<HistoryDataItem> historyDataItemArrayList, PaymentRejectListener listener) {
        this.activity = activity;
        this.historyDataItemArrayList = historyDataItemArrayList;
        session = new InventoryAppSession(activity);
        this.listener = listener;
        inventoryAppSession = new InventoryAppSession(activity);

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history_list2, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        try {
            System.out.println("USER TYPE   " + session.getKeyUserType());//getKeyUserType()
            String sDate1 = historyDataItemArrayList.get(holder.getAdapterPosition()).getCreatedOn();
            String updateTime = "";
            String timeSectingString = sDate1.split(" ")[1];
            String hourSection = timeSectingString.split(":")[0];
            String minuteSection = timeSectingString.split(":")[1];


            if (Integer.parseInt(hourSection) > 12) {
                updateTime = "" + (Integer.parseInt(hourSection) - 12) + ":" + minuteSection + " PM";
            } else {
                updateTime = "" + (Integer.parseInt(hourSection)) + ":" + minuteSection + " AM";
            }

            //System.out.println("OUTPUT====RITU=== UPDATE DATE=====" + sDate1);
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate1);
            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            String smonth = new DateFormatSymbols().getShortMonths()[date1.getMonth()];


            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);


            String dateTextValue = "<font><b>Date: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + day + "-" + smonth + "-" + year + "<b></font>";
            String qntyTextValue = "<font><b>Quantity: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + historyDataItemArrayList.get(holder.getAdapterPosition()).getQtyDone() + "<b></font>";
            String updatedByTextValue = "<font><b>Quantity updated By: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + historyDataItemArrayList.get(holder.getAdapterPosition()).getUser().getName() + "<b></font>";
            String updatedTimeByTextValue = "<font><b>Time updated: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + updateTime + "<b></font>";
            //Comment
            if (historyDataItemArrayList.get(holder.getAdapterPosition()).getFuserComment() != null && historyDataItemArrayList.get(holder.getAdapterPosition()).getFuserComment().length() > 0) {
                String strComment = "<font><b>Comment: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + historyDataItemArrayList.get(holder.getAdapterPosition()).getFuserComment() + "<b></font>";
                holder.textCommentTV.setText(Html.fromHtml(strComment));
            }

            holder.textDate.setText(Html.fromHtml(dateTextValue));
            holder.textQuantity.setText(Html.fromHtml(qntyTextValue));
            holder.textUpdateBy.setText(Html.fromHtml(updatedByTextValue));
            holder.textTime.setText(Html.fromHtml(updatedTimeByTextValue));

                if (historyDataItemArrayList.get(position).getWork_type().equals("1")) {
                    holder.tvWorkType.setText("Supply");
                    holder.llCb.setVisibility(View.VISIBLE);
                    holder.tvRemark.setVisibility(View.VISIBLE);
                    holder.reviewshow.setVisibility(View.VISIBLE);
                    holder.reviewshow.setText(historyDataItemArrayList.get(position).getWork_remark());
                    if (historyDataItemArrayList.get(position).getWork_remark().length() > 28) {
                        holder.tvRemark.setVisibility(View.VISIBLE);
                    } else {
                        holder.tvRemark.setVisibility(View.GONE);
                    }


                } else if (historyDataItemArrayList.get(position).getWork_type().equals("2")) {
                    holder.tvWorkType.setText("Installation");

                    holder.reviewshow.setVisibility(View.VISIBLE);
                    holder.llCb.setVisibility(View.VISIBLE);
                    holder.reviewshow.setText(historyDataItemArrayList.get(position).getWork_remark());
                    if (historyDataItemArrayList.get(position).getWork_remark().length() > 28)
                        holder.tvRemark.setVisibility(View.VISIBLE);
                    else
                        holder.tvRemark.setVisibility(View.GONE);

                } else  {
                    holder.tvWorkType.setText("Both supply and installation");
                    holder.llCb.setVisibility(View.VISIBLE);
                    if (historyDataItemArrayList.get(position).getWork_remark().length() > 28)
                        holder.tvRemark.setVisibility(View.VISIBLE);
                    else
                        holder.tvRemark.setVisibility(View.GONE);

                    holder.reviewshow.setVisibility(View.VISIBLE);
                    holder.reviewshow.setText(historyDataItemArrayList.get(position).getWork_remark());
                }
// else {
//                    holder.tvRemark.setVisibility(View.VISIBLE);
//                    holder.llCb.setVisibility(View.VISIBLE);
//                    holder.reviewshow.setVisibility(View.VISIBLE);
//                    holder.reviewshow.setText(historyDataItemArrayList.get(position).getWork_remark());
//
//                }
            String fileExtension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(historyDataItemArrayList.get(holder.getAdapterPosition()).getDocName());


            System.out.println("FILE EXTENSION UPDATE========" + position + fileExtension);

            Picasso.with(activity).load(Content.baseURL + historyDataItemArrayList.get(holder.getAdapterPosition()).getDocName())
                    .placeholder(R.drawable.place_holder2)
                    .error(R.drawable.doc_image)
                    .resize(300, 300)
                    .into(holder.imagView);

            /*This block of cosde  is used show the payment info*/


            if (historyDataItemArrayList.get(position).getPayStatus() == 1) {
                List<HistoryPayment> historyPaymentArrayList = new ArrayList<HistoryPayment>();
                historyPaymentArrayList = historyDataItemArrayList.get(position).getPayment();
                // holder.imageView_rejected.setVisibility(View.GONE);
                System.out.println("PAYMENT SIZE=====" + historyPaymentArrayList.size());
                if (historyPaymentArrayList.size() > 0) {
                    holder.btnPaymentPay.setVisibility(GONE);
                    String statusTextValue = "<font><b>Status: <b><font>" + "<font COLOR=\'GREEN\'> <b>paid<b></font>";
                    holder.textStatus.setText(Html.fromHtml(statusTextValue));
                    holder.viewDivider.setVisibility(View.VISIBLE);
                    holder.LL_paymentContainer.setVisibility(View.VISIBLE);
                    holder.btn_payment_reject.setVisibility(View.GONE);
                    holder.btn_payment_approve.setVisibility(GONE);
                    HistoryPayment historyPayment = historyPaymentArrayList.get(0);

                    System.out.println("PAYMENT IMAGE     " + paymentImage);
                    String fileExtension2 = android.webkit.MimeTypeMap.getFileExtensionFromUrl(historyPayment.getPay_img());
                    // System.out.println("PAYMENT IMAGE EXTENSION=============" + Content.baseURL + historyPayment.getPay_img());

                    try {

                        String paymentDateString = historyPayment.getPaidDate();
                        String paymentTime = "";
                        String p_timeSectingString = paymentDateString.split(" ")[1];
                        String p_hourSection = p_timeSectingString.split(":")[0];
                        String p_minuteSection = p_timeSectingString.split(":")[1];
                        if (Integer.parseInt(p_hourSection) > 12) {
                            paymentTime = "" + (Integer.parseInt(p_hourSection) - 12) + ":" + p_minuteSection + " PM";
                        } else {
                            paymentTime = "" + (Integer.parseInt(p_hourSection)) + ":" + p_minuteSection + " AM";
                        }

                        // System.out.println("OUTPUT====RITU=== PAY DATE=====" + paymentDateString);

                        Date paymentDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(paymentDateString);
                        //DateFormatSymbols dateFormatSymbols2 = new DateFormatSymbols();
                        String paymentDateMonth = new DateFormatSymbols().getShortMonths()[paymentDate.getMonth()];
                        Calendar paymentCalendar = new GregorianCalendar();
                        paymentCalendar.setTime(paymentDate);
                        int pyear = calendar.get(Calendar.YEAR);
                        int pmonth = calendar.get(Calendar.MONTH) + 1;
                        int pday = calendar.get(Calendar.DAY_OF_MONTH);
                        int payTime = calendar.get(Calendar.HOUR_OF_DAY);
                        String paymentdateTextValue = "<font><b>Payment Date: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + pday + "-" + paymentDateMonth + "-" + pyear + "<b></font>";
                        String payamountTextValue = "<font><b>Paid Amount: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + historyPayment.getAmountPaid() + "<b></font>";
                        String paymentupdatedByTextValue = "<font><b>Payment By: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + historyPayment.getPaidName() + "<b></font>";
                        String paymentupdatedTimeByTextValue = "<font><b>Time updated: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + paymentTime + "<b></font>";

                        System.out.println("PAYMENT====================DATE" + paymentdateTextValue);

                        holder.textPaymentAmount.setText(Html.fromHtml(payamountTextValue));
                        holder.textPaymentDate.setText(Html.fromHtml(paymentdateTextValue));
                        holder.textPaymentBy.setText(Html.fromHtml(paymentupdatedByTextValue));
                        holder.paymentTime.setText(Html.fromHtml(paymentupdatedTimeByTextValue));
                        paymentImage = historyPayment.getPay_img();

                        Picasso.with(activity).load(Content.baseURL + historyPayment.getPay_img())
                                .placeholder(R.drawable.place_holder2)
                                .error(R.drawable.doc_image)
                                .resize(300, 300)
                                .into(holder.imagView2);


                    } catch (Exception ex) {
                        System.out.println("ERROR===================" + ex);
                    }


                }

            } else {
                holder.btnPaymentPay.setVisibility(GONE);
                String statusTextValue = "";

                if (historyDataItemArrayList.get(holder.getAdapterPosition()).getPayStatus() == 0) {
                    statusTextValue = "<font><b>Status: <b><font>" + "<font COLOR=\'BLUE\'> <b>Under review<b></font>";
                }
                if (historyDataItemArrayList.get(holder.getAdapterPosition()).getPayStatus() == 2) {
                    statusTextValue = "<font><b>Status: <b><font>" + "<font COLOR=\'RED\'> <b>Rejected<b></font>";
                }
                if (historyDataItemArrayList.get(holder.getAdapterPosition()).getPayStatus() == 3) {
                    statusTextValue = "<font><b>Status: <b><font>" + "<font COLOR=\'GREEN\'> <b>Approved<b></font>";
                }


                //String statusTextValue = "<font><b>Status: <b><font>" + "<font COLOR=\'BLACK\'> <b>not paid<b></font>";
                holder.textStatus.setText(Html.fromHtml(statusTextValue));
                holder.viewDivider.setVisibility(GONE);
                holder.LL_paymentContainer.setVisibility(GONE);

                //if (session.getKeyUserType().equals("Supervisor") || session.getKeyUserType().equals("Admin")) {

                String strLoggedIDSupervisorID = session.getKeyUserId();
                String strPhysicalSupervisorID = "" + historyDataItemArrayList.get(holder.getAdapterPosition()).getUser().getSupervisor();
                Log.e("HistoryAdapter", "====Logged SuperVisor ID===" + strLoggedIDSupervisorID);
                Log.e("HistoryAdapter", "====Physical SuperVisor ID===" + strPhysicalSupervisorID);
                if (session.getKeyUserType().equals("Supervisor") && strLoggedIDSupervisorID.equals(strPhysicalSupervisorID)) {
                    holder.btn_payment_reject.setVisibility(View.VISIBLE);
                    holder.btn_payment_approve.setVisibility(View.VISIBLE);
                } else {
                    holder.btn_payment_reject.setVisibility(View.GONE);
                }

                if (historyDataItemArrayList.get(holder.getAdapterPosition()).getPayStatus() == 2) {
                    holder.btn_payment_reject.setVisibility(View.GONE);
                    // holder.imageView_rejected.setVisibility(View.VISIBLE);
                    holder.btn_payment_approve.setVisibility(GONE);
                } else {
                    //holder.imageView_rejected.setVisibility(View.GONE);
                }

                if (historyDataItemArrayList.get(holder.getAdapterPosition()).getPayStatus() == 3) {
                    holder.btn_payment_reject.setVisibility(View.GONE);
                    // holder.imageView_rejected.setVisibility(View.GONE);
                    holder.btn_payment_approve.setVisibility(GONE);
                    //holder.imageView_approve.setVisibility(View.VISIBLE);
                }

                if (inventoryAppSession.getKeyUserType().equals("Physical")) {
                    if (Integer.valueOf(historyDataItemArrayList.get(position).getEdit_count()) > 1) {
                        holder.ivEdit.setVisibility(View.INVISIBLE);
                        holder.btn_editQuantity.setVisibility(View.GONE);
                    } else {
                        holder.ivEdit.setVisibility(View.INVISIBLE);
                        holder.btn_editQuantity.setVisibility(View.GONE);
                    }

                } else {
                    holder.btn_editQuantity.setVisibility(View.GONE);
                }
                if (inventoryAppSession.getKeyUserType().equals("Admin")) {
                    if (historyDataItemArrayList.get(holder.getAdapterPosition()).getFuserComment().length() > 1)
                        holder.btnPaymentPay.setVisibility(View.VISIBLE);
                    else {
                        holder.btnPaymentPay.setVisibility(GONE);
                    }
                    if (historyDataItemArrayList.get(holder.getAdapterPosition()).getEditable() > 0)
                        holder.ivEdit.setVisibility(View.INVISIBLE);
                    else {
                        holder.ivEdit.setVisibility(View.INVISIBLE);
                    }
                } else {
                    holder.btnPaymentPay.setVisibility(GONE);
                }


            }


            holder.btnPaymentPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //InventoryAppSingleton.getInstance().getItem_id();
                    activity.startActivity(new Intent(activity, UpdatePaymentByAdminActivity.class));


                }
            });

            /*NEW UPDATE================*/

            if (session.getKeyUserType().equals("Physical") && historyDataItemArrayList.get(holder.getAdapterPosition()).getPayStatus() == 0) {
                /*holder.btn_review_request.setVisibility(View.VISIBLE);*/
            }

            /*=========================================================*/


            holder.LL_update_image_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("DOC============" + historyDataItemArrayList.get(holder.getAdapterPosition()).getDocName());
                    Bundle bundle = new Bundle();
                    bundle.putString("doc_url", historyDataItemArrayList.get(holder.getAdapterPosition()).getDocName());
                    activity.startActivity(new Intent(activity, DocumentViewActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));
                }
            });

            holder.imagView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    System.out.println("DOC============" + paymentImage);
                    bundle.putString("doc_url", historyDataItemArrayList.get(holder.getAdapterPosition()).getPayment().get(0).getPay_img());
                    activity.startActivity(new Intent(activity, DocumentViewActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));
                }
            });

            holder.btn_payment_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.rejectPayment(historyDataItemArrayList.get(holder.getAdapterPosition()));
                }
            });

            holder.btn_payment_approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.approvePayment(historyDataItemArrayList.get(holder.getAdapterPosition()));
                }
            });



            /*If Admin Updated the Quantity*/
            if (historyDataItemArrayList.get(holder.getAdapterPosition()).getDocName().equals("NA") && historyDataItemArrayList.get(holder.getAdapterPosition()).getMessage().length() > 3) {
                holder.textQuantity.setVisibility(View.GONE);
                holder.textStatus.setVisibility(View.GONE);
                holder.textAdminUpdateMessageTV.setVisibility(View.VISIBLE);
                String adminUpdateMessage = "<font><b>Message: <b><font>" + "<font COLOR=\'BLACK\'> <b>" + historyDataItemArrayList.get(holder.getAdapterPosition()).getMessage() + "<b></font>";
                holder.textAdminUpdateMessageTV.setText(Html.fromHtml(adminUpdateMessage));
            } else {
                holder.textQuantity.setVisibility(View.VISIBLE);
                holder.textStatus.setVisibility(View.VISIBLE);
                holder.textAdminUpdateMessageTV.setVisibility(View.GONE);
            }

        } catch (Exception Ex) {
            System.out.println("OUTPUT ERROR====" + Ex.toString());
        }


        System.out.println("PAYMENT STATAUS====" + historyDataItemArrayList.get(position).getPayStatus());


    }

    @Override
    public int getItemCount() {
        return historyDataItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvWorkType)
        TextView tvWorkType;
        @BindView(R.id.textDate)
        CustomTextView textDate;
        @BindView(R.id.textQuantity)
        CustomTextView textQuantity;
        @BindView(R.id.textTime)
        TextView textTime;
        @BindView(R.id.textUpdateBy)
        TextView textUpdateBy;
        @BindView(R.id.textAdminUpdateMessageTV)
        TextView textAdminUpdateMessageTV;
        @BindView(R.id.imagView)
        ImageView imagView;
        @BindView(R.id.btn_editQuantity)
        Button btn_editQuantity;

        @BindView(R.id.textStatus)
        TextView textStatus;

        @BindView(R.id.viewDivider)
        View viewDivider;


        @BindView(R.id.textPaymentDate)
        TextView textPaymentDate;

        @BindView(R.id.textPaymentBy)
        TextView textPaymentBy;

        @BindView(R.id.paymentTime)
        TextView paymentTime;

        @BindView(R.id.textCommentTV)
        TextView textCommentTV;

        @BindView(R.id.btnPaymentPay)
        Button btnPaymentPay;


        @BindView(R.id.imagView2)
        ImageView imagView2;

        @BindView(R.id.LL_paymentContainer)
        LinearLayout LL_paymentContainer;

        @BindView(R.id.textPaymentAmount)
        TextView textPaymentAmount;


        @BindView(R.id.progress_update)
        ProgressBar progress_update;

        @BindView(R.id.progress_payment)
        ProgressBar progress_payment;

        @BindView(R.id.btn_payment_reject)
        Button btn_payment_reject;
        AlertDialog.Builder alertDialogBuilder;

       /* @BindView(R.id.imageView_rejected)
        ImageView imageView_rejected;*/

        @BindView(R.id.LL_payment_image_container)
        LinearLayout LL_payment_image_container;

        @BindView(R.id.LL_update_image_container)
        LinearLayout LL_update_image_container;
        @BindView(R.id.ivEdit)
        ImageView ivEdit;
        @BindView(R.id.btn_payment_approve)
        Button btn_payment_approve;
        @BindView(R.id.pbEdit)
        ProgressBar pbEdit;
        @BindView(R.id.llCb)
        LinearLayout llCb;
        @BindView(R.id.tvRemark)
        TextView tvRemark;
        @BindView(R.id.reviewshow)
        TextView reviewshow;
        /* @BindView(R.id.imageView_approve)
         ImageView imageView_approve;*/

        /* @BindView(R.id.btn_review_request)
         Button btn_review_request;*/


        public MyViewHolder(View itemView) {
            super(itemView);


            ButterKnife.bind(this, itemView);

            btn_editQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    alertDialogBuilder = new AlertDialog.Builder(activity);

                    LayoutInflater LayoutInflater = activity.getLayoutInflater();
                    View dialogView = LayoutInflater.inflate(R.layout.dialog_review_request_layout, null);


                    alertDialogBuilder.setView(dialogView);
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    Window window = alertDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    alertDialog.show();

                    Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
                    Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

                    //final LinearLayout LL_review_text_container=(LinearLayout)dialogView.findViewById(R.id.LL_review_text_container);
                    final EditText inputReview = (EditText) dialogView.findViewById(R.id.inputReview);
                    // final TextView reviewText=(TextView) dialogView.findViewById(R.id.reviewText);
                    noBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                    yesBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            System.out.println("DATA======" + inputReview.getText());
                            if (!inputReview.getText().toString().equals("")) {
                                //   Toast.makeText(HistoryActivity.this, "Please enter review text", Toast.LENGTH_SHORT).show();
                                try {
                                    final String url = Content.baseURL + "doc/reviewupdate";
                                    /*SimpleProgressBar.showProgress(SignInActivity.this);*/

                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                    try {
                                                        Toast.makeText(activity, "your request has submitted", Toast.LENGTH_SHORT).show();


                                                        alertDialog.dismiss();
                                                        JSONObject jsonObject = new JSONObject(response);
                                                        Object object = jsonObject.get("status");
                                                        Object object1 = jsonObject.get("data");

                                                        if (object instanceof Boolean) {
                                                            Gson gson = new Gson();
                                                            ReviewStatus reviewStatus = gson.fromJson(response, ReviewStatus.class);

                                                            System.out.println("OUTPUT======success==" + reviewStatus.getStatus());
                                                            System.out.println("OUTPUT======success==" + reviewStatus.getData().getEdit_count());
                                                            if (Integer.valueOf(reviewStatus.getData().getEdit_count()) < 2) {
                                                                btn_editQuantity.setVisibility(View.VISIBLE);
                                                            } else {
                                                                btn_editQuantity.setVisibility(View.GONE);
                                                            }


                                                        } else {
                                                            alertDialog.dismiss();
                                    /*LL_errorSection.setVisibility(View.VISIBLE);
                                    text_error.setText(""+jsonObject.getString("data"));*/


                                                        }


                                                    } catch (Exception ex) {
                                                        alertDialog.dismiss();
                                                        CommonLoadingDialog.closeLoadingDialog();
                                                        System.out.println("OUTPUT ======ERROR==" + ex);
                                                    }


                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    alertDialog.dismiss();
                                                    System.out.println("OUTPUT ======ERROR==" + error.toString());
                                                }
                                            }) {
                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("id", String.valueOf(historyDataItemArrayList.get(getAdapterPosition()).getId()));
                                            params.put("message", inputReview.getText().toString());
                                            params.put("user_id", inventoryAppSession.getKeyUserId());


                                            return params;
                                        }

                                    };

                                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    RequestQueue requestQueue = Volley.newRequestQueue(activity);
                                    stringRequest.setShouldCache(false);
                                    requestQueue.add(stringRequest);
                                } catch (Exception ex) {
                                    System.out.println("OUTPUT ======ERROR==" + ex);
                                }
                            } else {
                                alertDialog.dismiss();
                                //reviewRequestApiCall(inputReview.getText().toString().trim());
                            }

                        }
                    });


                    //trial
                    // pbEdit.setVisibility(View.VISIBLE);
//InventoryAppSingleton.getInstance().getItem_id()

                }

            });
            tvRemark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    alertDialogBuilder = new AlertDialog.Builder(activity);

                    LayoutInflater LayoutInflater = activity.getLayoutInflater();
                    View dialogView = LayoutInflater.inflate(R.layout.dialog_remark_details, null);


                    alertDialogBuilder.setView(dialogView);
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    Window window = alertDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

                    window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    alertDialog.show();

                    Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
                    Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);

                    //final LinearLayout LL_review_text_container=(LinearLayout)dialogView.findViewById(R.id.LL_review_text_container);
                    final TextView inputReview = (TextView) dialogView.findViewById(R.id.inputReview);
                    // final TextView reviewText=(TextView) dialogView.findViewById(R.id.reviewText);
                    inputReview.setText(historyDataItemArrayList.get(getAdapterPosition()).getWork_remark());
                    final Button LL_button_container = (Button) dialogView.findViewById(R.id.LL_button_container);
                    //trial
                    // pbEdit.setVisibility(View.VISIBLE);
//InventoryAppSingleton.getInstance().getItem_id()
                    LL_button_container.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                }

            });
        }


    }


}
