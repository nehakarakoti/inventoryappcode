package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.inventoryapp.DocumentViewActivity;
import com.app.inventoryapp.R;
import com.app.inventoryapp.utils.Content;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdateDocListAdapter extends RecyclerView.Adapter<UpdateDocListAdapter.MyViewHolder> {
    Activity activity;
    ArrayList<String> stringArrayListUpdateDoc;

    public UpdateDocListAdapter(Activity activity, ArrayList<String> stringArrayListUpdateDoc) {
        this.activity = activity;
        this.stringArrayListUpdateDoc = stringArrayListUpdateDoc;
        System.out.println("DOC IMAGE============" + stringArrayListUpdateDoc.size());

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_doc, parent, false);

        return new UpdateDocListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (stringArrayListUpdateDoc.get(position).toString() != null && stringArrayListUpdateDoc.get(position).toString().contains("/")) {
            holder.RL_doc_container.setVisibility(View.VISIBLE);
            holder.doc_name.setText(stringArrayListUpdateDoc.get(position).split("/")[1].split("_")[1].split("\\.")[0]);

            Picasso.with(activity)
                    .load(Content.baseURL + stringArrayListUpdateDoc.get(position))
                    .placeholder(R.drawable.place_holder2)
                    .error(R.drawable.doc_image)
                    .resize(300, 300)
                    .into(holder.image_doc);
        } else {
            holder.RL_doc_container.setVisibility(View.GONE);
        }

        holder.RL_doc_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("doc_url", stringArrayListUpdateDoc.get(position));

                activity.startActivity(new Intent(activity, DocumentViewActivity.class).putExtras(bundle));
                activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                //Toast.makeText(activity, "M CLICK", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringArrayListUpdateDoc.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.RL_doc_container)
        RelativeLayout RL_doc_container;

        @BindView(R.id.image_doc)
        ImageView image_doc;

        @BindView(R.id.doc_name)
        TextView doc_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
