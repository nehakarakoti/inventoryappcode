package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UserDataModel;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ContractorListAdapter extends RecyclerView.Adapter<ContractorListAdapter.MyViewHolder>
{
    Activity activity;
    private ArrayList<UserDataModel> userDataModelContractorArrayList=new ArrayList<>();

    public ContractorListAdapter(Activity activity,ArrayList<UserDataModel> userDataModelContractorArrayList)
    {
        this.activity=activity;
        this.userDataModelContractorArrayList=userDataModelContractorArrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contractor_name_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
            holder.textContractorName.setText(userDataModelContractorArrayList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return userDataModelContractorArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        @BindView(R.id.textContractorName)
        TextView textContractorName;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
