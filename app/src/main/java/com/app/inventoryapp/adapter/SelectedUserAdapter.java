package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.UserRemoveEventListener;
import com.app.inventoryapp.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RITUPARNA on 5/20/2018.
 */

public class SelectedUserAdapter extends RecyclerView.Adapter<SelectedUserAdapter.MyViewHolder> {
    Activity activity;
    UserRemoveEventListener userRemoveEventListener;
    private ArrayList<UsersResponse> usersResponseArrayList;


    public SelectedUserAdapter(ArrayList<UsersResponse> usersResponseArrayList, Activity activity, UserRemoveEventListener userRemoveEventListener) {
        this.activity = activity;
        this.usersResponseArrayList = usersResponseArrayList;
        this.userRemoveEventListener = userRemoveEventListener;


        for (int i = 0; i < usersResponseArrayList.size(); i++) {
            System.out.println("USER NAME====" + usersResponseArrayList.get(i).getName());
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_list_selected, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.nameTextView.setText(usersResponseArrayList.get(position).getName());
        holder.roleTextView.setText(Utilities.getUpdatedRoleText(usersResponseArrayList.get(position).getUsertype()));
        holder.LL_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(activity, "Hi", Toast.LENGTH_SHORT).show();
                //userRemoveEventListener.removeUser(usersResponseArrayList.get(position));
                // usersResponseArrayList.remove(position);
                usersResponseArrayList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersResponseArrayList.size();
    }

    public void updateList(ArrayList<UsersResponse> usersResponseArrayList) {
        this.usersResponseArrayList = usersResponseArrayList;
        notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.roleTextView)
        TextView roleTextView;

        @BindView(R.id.LL_close)
        LinearLayout LL_close;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
