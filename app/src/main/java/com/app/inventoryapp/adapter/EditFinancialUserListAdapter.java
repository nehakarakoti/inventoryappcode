package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.app.inventoryapp.EditUser;
import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.GetFinacialUserInterface;
import com.app.inventoryapp.utils.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dharmaniz on 2/5/18.
 */

public class EditFinancialUserListAdapter extends RecyclerView.Adapter<EditFinancialUserListAdapter.MyViewHolder> {
    GetFinacialUserInterface mGetFinacialUserInterface;
    private Activity activity;
    private ArrayList<UsersResponse> usersResponseArrayList;
    private ArrayList<UsersResponse> usersResponseArrayRestoreList = new ArrayList<>();
    private int lastSelectedPosition = -1;

    String strAlreadyAddedFinancialUser;

    public EditFinancialUserListAdapter(Activity activity, ArrayList<UsersResponse> usersResponseArrayList, GetFinacialUserInterface mGetFinacialUserInterface,String strAlreadyAddedFinancialUser) {
        this.activity = activity;
        this.usersResponseArrayList = usersResponseArrayList;
        this.usersResponseArrayRestoreList = usersResponseArrayList;
        this.mGetFinacialUserInterface = mGetFinacialUserInterface;
        this.strAlreadyAddedFinancialUser = strAlreadyAddedFinancialUser;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_financial_user, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final UsersResponse usersResponse = usersResponseArrayList.get(position);
        holder.emailTextView.setText(usersResponse.getUsername());
        holder.roleTextView.setText(Utilities.getUpdatedRoleText(usersResponse.getUsertype()));
        holder.nameTextView.setText(usersResponse.getName());
        System.out.println("USER ID===" + usersResponseArrayList.get(position).getUsername() + usersResponseArrayList.get(position).getId());



//        if (strAlreadyAddedFinancialUser != null && strAlreadyAddedFinancialUser.length() > 0){
//            holder.mRadioButton.setVisibility(View.GONE);
//        }else{
//            holder.mRadioButton.setVisibility(View.VISIBLE);
//        }

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        holder.mRadioButton.setChecked(lastSelectedPosition == position);

        holder.mRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = holder.getAdapterPosition();
                mGetFinacialUserInterface.getFinancialUser(usersResponse);
                notifyDataSetChanged();
            }
        });


        holder.LL_user_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("UsersResponse", usersResponseArrayList.get(holder.getAdapterPosition()));
                if (!usersResponseArrayList.get(holder.getAdapterPosition()).getPassword().equals("")) {
                    bundle.putString("password", usersResponseArrayList.get(holder.getAdapterPosition()).getPassword());
                }
                activity.startActivity(new Intent(activity, EditUser.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtras(bundle));

            }
        });


    }

    @Override
    public int getItemCount() {
        return usersResponseArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.roleTextView)
        TextView roleTextView;

        @BindView(R.id.emailTextView)
        TextView emailTextView;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.LL_user_item)
        LinearLayout LL_user_item;

        @BindView(R.id.mRadioButton)
        RadioButton mRadioButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
