package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.inventoryapp.HistoryActivity;
import com.app.inventoryapp.HomeActivity;
import com.app.inventoryapp.HomeActivityOther;
import com.app.inventoryapp.R;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.NotificationData;
import com.app.inventoryapp.session.InventoryAppSession;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{

    Activity activity;
    ArrayList<NotificationData> notificationDataArrayList=new ArrayList<>();
    InventoryAppSession session;

    public NotificationAdapter(Activity activity, ArrayList<NotificationData> notificationDataArrayList)
    {
        this.notificationDataArrayList=notificationDataArrayList;
        this.activity=activity;
        session=new InventoryAppSession(activity);

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);

        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        try {
            String sDate1 = notificationDataArrayList.get(holder.getAdapterPosition()).getCreated_on();

            String updateTime = "";
            String timeSectingString = sDate1.split(" ")[1];
            String hourSection = timeSectingString.split(":")[0];
            String minuteSection = timeSectingString.split(":")[1];


            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            String smonth = new DateFormatSymbols().getShortMonths()[date1.getMonth()];

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            if (Integer.parseInt(hourSection) > 12) {
                updateTime = "" + (Integer.parseInt(hourSection) - 12) + ":" + minuteSection + " PM";
            } else {
                updateTime = "" + (Integer.parseInt(hourSection)) + ":" + minuteSection + " AM";
            }


            holder.notification_message.setText(notificationDataArrayList.get(position).getMassage());
            holder.notification_date.setText(day+"-"+smonth+"-"+year+"     "+updateTime);


            holder.RL_notification_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                   // Toast.makeText(activity, ""+notificationDataArrayList.get(position).getActivity_name(), Toast.LENGTH_SHORT).show();
                    if(notificationDataArrayList.get(position).getActivity_name().equals("add_project"))
                    {
                        InventoryAppSingleton.getInstance().setProject_id(notificationDataArrayList.get(position).getProject_id());
                        if(session.getKeyUserType().equals("Supervisor") && session.getKeyUserType().equals("Admin"))
                        {
                            InventoryAppSingleton.getInstance().setItem_id(notificationDataArrayList.get(position).getItem_id());
                            activity.startActivity(new Intent(activity, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            activity.finish();
                        }
                        else
                        {
                            InventoryAppSingleton.getInstance().setItem_id(notificationDataArrayList.get(position).getItem_id());
                            activity.startActivity(new Intent(activity, HomeActivityOther.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                        }
                    }
                    else
                    {
                        InventoryAppSingleton.getInstance().setItem_id(notificationDataArrayList.get(position).getItem_id());
                        activity.startActivity(new Intent(activity, HistoryActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        activity.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                    }
                }
            });
        } catch (Exception ex) {

        }

    }

    @Override
    public int getItemCount() {
        return notificationDataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.notification_date)
        TextView notification_date;

        @BindView(R.id.notification_message)
        TextView notification_message;

        @BindView(R.id.RL_notification_holder)
        RelativeLayout RL_notification_holder;



        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateList(ArrayList<NotificationData> notificationDataArrayList)
    {
        this.notificationDataArrayList=notificationDataArrayList;
        notifyDataSetChanged();
    }

}
