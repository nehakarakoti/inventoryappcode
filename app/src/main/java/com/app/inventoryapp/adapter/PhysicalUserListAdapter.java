package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.SelectedUserInterface;
import com.app.inventoryapp.model.SupervisorWithPhysicalUsers;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.PhysicalSelectedUserInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dharmaniz on 2/5/18.
 */

public class PhysicalUserListAdapter extends RecyclerView.Adapter<PhysicalUserListAdapter.MyViewHolder> {

    String TAG = "PhysicalUserListAdapter";
    private Activity activity;
    private ArrayList<SupervisorWithPhysicalUsers> usersResponseArrayList;
    private PhysicalSelectedUserInterface mSelectedUserInterface;


    public PhysicalUserListAdapter(Activity activity, ArrayList<SupervisorWithPhysicalUsers> usersResponseArrayList, PhysicalSelectedUserInterface mSelectedUserInterface) {
        this.activity = activity;
        this.usersResponseArrayList = usersResponseArrayList;
        this.mSelectedUserInterface = mSelectedUserInterface;

    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_supervisor, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        SupervisorWithPhysicalUsers mModel = usersResponseArrayList.get(position);
        holder.itemTxtSupervisorTV.setText(mModel.getName());


        /*Set Adapter*/
        InnerPhysicalListAdapter mAdapter = new InnerPhysicalListAdapter(activity, mModel.getmSubPhysicalUserAL(), mSelectedUserInterface);
        holder.itemPhysicalRV.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        holder.itemPhysicalRV.setLayoutManager(layoutManager);
        holder.itemPhysicalRV.setItemAnimator(new DefaultItemAnimator());
        holder.itemPhysicalRV.setAdapter(mAdapter);
    }


    @Override
    public int getItemCount() {
        return usersResponseArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemTxtSupervisorTV)
        TextView itemTxtSupervisorTV;

        @BindView(R.id.itemPhysicalRV)
        RecyclerView itemPhysicalRV;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
