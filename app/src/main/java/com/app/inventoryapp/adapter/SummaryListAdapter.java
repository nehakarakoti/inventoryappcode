package com.app.inventoryapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.utils.Utilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import volley.PhysicalUser;

/**
 * Created by SHALINI on 7/17/2018.
 */

public class SummaryListAdapter extends RecyclerView.Adapter {
    Context mContext;
    List<PhysicalUser> memeberList;


    public SummaryListAdapter(Context mContext, List<PhysicalUser> memeberList) {
        this.mContext = mContext;
        this.memeberList = memeberList;
    }

    @NonNull
    @Override

    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyView(LayoutInflater.from(mContext).inflate(R.layout.recyclerview_dataproject, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyView view1 = (MyView) holder;
        PhysicalUser physicalUser = memeberList.get(position);

        if (physicalUser.getName() != null && physicalUser.getUser_type() != null)
            view1.tvName.setText(physicalUser.getName() + "\n" + Utilities.getUpdatedRoleText(physicalUser.getUser_type()));

        if (physicalUser.getQty() != null)
        view1.tvQuantityUpdated.setText(physicalUser.getQty().toString());
    }


    @Override
    public int getItemCount() {
        return memeberList.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvQuantityUpdated)
        TextView tvQuantityUpdated;

        public MyView(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
