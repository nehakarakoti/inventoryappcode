package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UsersResponse;
import com.app.inventoryapp.my_interface.AddUserToProjectInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectParticularUsersListAdapter extends RecyclerView.Adapter<ProjectParticularUsersListAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<UsersResponse> usersResponseArrayListFilter=new ArrayList<>();
    private ArrayList<UsersResponse> usersResponseArrayListCarrier=new ArrayList<>();
    private AddUserToProjectInterface listener;

    public ProjectParticularUsersListAdapter( Activity activity,ArrayList<UsersResponse> usersResponseArrayListFilter, AddUserToProjectInterface listener)
    {
        this.activity=activity;
        this.usersResponseArrayListFilter=usersResponseArrayListFilter;
        this.listener=listener;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_list, parent, false);

        return new ProjectParticularUsersListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {

        UsersResponse usersResponse=usersResponseArrayListFilter.get(position);


        holder.emailTextView.setText(usersResponseArrayListFilter.get(holder.getAdapterPosition()).getUsername());
        holder.nameTextView.setText(usersResponseArrayListFilter.get(holder.getAdapterPosition()).getName());
        holder.roleTextView.setText(usersResponseArrayListFilter.get(holder.getAdapterPosition()).getUsertype());

        if(usersResponse.isChecked())
        {

            holder.checkbox.setChecked(true);
            //usersResponseArrayList.get(position).setChecked(true);
        }
        else
        {
            holder.checkbox.setChecked(false);
            //usersResponseArrayList.get(position).setChecked(false);

        }

        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CheckBox checkBox=(CheckBox)v.findViewById(R.id.checkbox);
                if(checkBox.isChecked())
                {

                    usersResponseArrayListFilter.get(holder.getAdapterPosition()).setChecked(true);
                    usersResponseArrayListCarrier.add(usersResponseArrayListFilter.get(holder.getAdapterPosition()));
                    listener.selectUserToProject(usersResponseArrayListCarrier);
                }
                else
                {
                    usersResponseArrayListFilter.get(holder.getAdapterPosition()).setChecked(false);
                    usersResponseArrayListCarrier.remove(usersResponseArrayListFilter.get(holder.getAdapterPosition()));
                    listener.selectUserToProject(usersResponseArrayListCarrier);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersResponseArrayListFilter.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.roleTextView)
        TextView roleTextView;

        @BindView(R.id.emailTextView)
        TextView emailTextView;

        @BindView(R.id.nameTextView)
        TextView nameTextView;

        @BindView(R.id.LL_user_item)
        LinearLayout LL_user_item;


        @BindView(R.id.checkbox)
        CheckBox checkbox;

        public MyViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
