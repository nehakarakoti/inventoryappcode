package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.inventoryapp.R;
import com.app.inventoryapp.model.UserTypeModel;
import com.app.inventoryapp.utils.Utilities;

import java.util.ArrayList;

/**
 * Created by RITUPARNA on 5/4/2018.
 */

public class CustomSpinnerAdapter extends BaseAdapter {

    Activity activity;
    ArrayList<UserTypeModel> userTypeModelArrayList;

    public CustomSpinnerAdapter(Activity activity, ArrayList<UserTypeModel> userTypeModelArrayList) {
        this.activity = activity;
        this.userTypeModelArrayList = userTypeModelArrayList;
    }

    @Override
    public int getCount() {
        return userTypeModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return userTypeModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (activity).getLayoutInflater();
        View row = inflater.inflate(R.layout.item_spinner_data, parent, false);

        TextView textspinner = (TextView) row.findViewById(R.id.textspinner);
        if (position == 0){
            textspinner.setText(userTypeModelArrayList.get(position).getUser_name());
        }else{
            textspinner.setText(Utilities.getUpdatedRoleText(userTypeModelArrayList.get(position).getUser_name()));
        }



        return row;
    }


}
