package com.app.inventoryapp.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.inventoryapp.R;
import com.app.inventoryapp.UpdateDetailsFinancialActivity;
import com.app.inventoryapp.app.InventoryAppSingleton;
import com.app.inventoryapp.model.DataItem;
import com.app.inventoryapp.model.UnpaidResponseModel;
import com.app.inventoryapp.my_interface.PaymentDocUploadInterface;
import com.app.inventoryapp.my_interface.UpdateDetailsInterface;
import com.app.inventoryapp.session.InventoryAppSession;
import com.app.inventoryapp.utils.CommonLoadingDialog;
import com.app.inventoryapp.utils.Content;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UpdateDetailsPaymentAdapter extends RecyclerView.Adapter<UpdateDetailsPaymentAdapter.MyViewHolder> {
    // ArrayList<ItemDocModel> itemDocModelArrayList;
    Activity activity;
    InventoryAppSession session;
    UpdateDetailsInterface listener;
    PaymentDocUploadInterface listener2;
    String user_id, qnt, subItem, numberAsString;
    private ArrayList<DataItem> dataItemArrayList;
    private AlertDialog.Builder alertDialogBuilder;


    public UpdateDetailsPaymentAdapter(Activity activity, ArrayList<DataItem> dataItemArrayList) {
        this.activity = activity;
        this.dataItemArrayList = dataItemArrayList;
        listener = (UpdateDetailsFinancialActivity) activity;
        listener2 = (UpdateDetailsFinancialActivity) activity;
        session = new InventoryAppSession(activity);
        alertDialogBuilder = new AlertDialog.Builder(this.activity);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_update_details, parent, false);

        return new UpdateDetailsPaymentAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        try {
            String date = dataItemArrayList.get(position).getCreatedOn().trim().split(" ")[0];
            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            String smonth = new DateFormatSymbols().getShortMonths()[date1.getMonth()];
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            holder.textView_date.setText("Date: " + day + "-" + smonth + "-" + year);
            holder.textView_itemQuantity.setText("Quantity: " + dataItemArrayList.get(position).getQtyDone().trim());
            Double quantity = Double.parseDouble(dataItemArrayList.get(position).getQtyDone().trim());
            System.out.println("RATE OF ITEM====" + UpdateDetailsFinancialActivity.rate_per_unit);
            final Double amount = quantity * UpdateDetailsFinancialActivity.rate_per_unit;
            numberAsString = String.format("%.2f", amount);
            holder.textView_itemAmount.setText("Amount: " + numberAsString);

            user_id = "" + dataItemArrayList.get(position).getUserId();
            qnt = dataItemArrayList.get(position).getQtyDone().trim();

            subItem = "" + dataItemArrayList.get(position).getId();

            /*Set Financial Comment*/
            if (dataItemArrayList.get(position).getFuser_comment().length() > 2){
                holder.btn_payment.setVisibility(View.GONE);
                holder.txtItemCommentValueTV.setText("Comment: " + dataItemArrayList.get(position).getFuser_comment());
            }else{
                holder.btn_payment.setVisibility(View.VISIBLE);
            }



            holder.btn_payment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Double quantity = Double.parseDouble(dataItemArrayList.get(position).getQtyDone().trim());
                    final Double amount = quantity * UpdateDetailsFinancialActivity.rate_per_unit;
                    String user_id = "" + dataItemArrayList.get(position).getUserId();
                    String qnt = dataItemArrayList.get(position).getQtyDone().trim();
                    String subItem = "" + dataItemArrayList.get(position).getId();
                    String itSelfID = "" + dataItemArrayList.get(position).getId();

                    listener2.showPaymentUploadDialog(itSelfID,amount, user_id, qnt, subItem);
                }
            });

            String fileExtension = android.webkit.MimeTypeMap.getFileExtensionFromUrl(dataItemArrayList.get(position).getDocName());

            System.out.println("IAMGE EXTENSION======" + fileExtension);

            if (fileExtension.equals("pdf")) {
                holder.itemImage.setBackground(ContextCompat.getDrawable(activity, R.drawable.doc_image));
            } else {
                Picasso.with(activity).load(Content.baseURL + dataItemArrayList.get(position).getDocName())
                        .placeholder(R.drawable.place_holder2)
                        .resize(300, 300)
                        .error(R.drawable.place_holder2)
                        .skipMemoryCache().transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        int size = Math.min(source.getWidth(), source.getHeight());
                        int x = (source.getWidth() - size) / 2;
                        int y = (source.getHeight() - size) / 2;
                        Bitmap result = Bitmap.createBitmap(source, x, y, size, size);
                        if (result != source) {
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "front";
                    }
                }).into(holder.itemImage);
            }

        } catch (Exception ex) {

        }


    }

    private void httpPayment(final String amount, final String quantity, final String user_id, final String subItem) {


        System.out.println("INVENTORY==========AMOUNT TO BE PAID==" + amount);
        System.out.println("INVENTORY==========QUANTITY==" + quantity);
        System.out.println("INVENTORY==========USER ID==" + user_id);
        System.out.println("INVENTORY==========SUB ITEM==" + subItem);


        try {


            final String url = Content.baseURL + "docps?check=2";

            System.out.println("OUTPUT URL====" + url);
            CommonLoadingDialog.showLoadingDialog(activity, "Paying....");


            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            try {


                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getBoolean("status")) {
                                    System.out.println("RESPONSE======" + response);
                                    Gson gson = new Gson();

                                    UnpaidResponseModel unpaidResponseModel = gson.fromJson(response, UnpaidResponseModel.class);
                                    dataItemArrayList = (ArrayList<DataItem>) unpaidResponseModel.getData();
                                    //Log.d("xxx", dataItemArrayList);

                                    System.out.println("SIZE AFTER PAYMENT=====" + dataItemArrayList.size() + dataItemArrayList);


                                    //notifyItemChanged(0);
                                    notifyDataSetChanged();

                                    if (dataItemArrayList.size() == 0) {
                                        listener.UpdateUI();
                                    }


                                } else {
                                    Toast.makeText(activity, "Payment not success", Toast.LENGTH_SHORT).show();
                                }


                                //LoginResponse loginResponse=gson.fromJson(response, LoginResponse.class);


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                            CommonLoadingDialog.closeLoadingDialog();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();


                    System.out.println("OUTPUT=======PARAM   project_id==" + InventoryAppSingleton.getInstance().getProject_id());
                    System.out.println("OUTPUT=======PARAM  user_id==" + session.getKeyUserId());
                    System.out.println("OUTPUT=======PARAM iteam_id ==" + InventoryAppSingleton.getInstance().getItem_id());
                    System.out.println("OUTPUT=======PARAM paid_user==" + user_id.trim());
                    System.out.println("OUTPUT=======PARAM qty_done==" + quantity.trim());
                    System.out.println("OUTPUT=======PARAM sub_item==" + subItem.trim());
                    System.out.println("OUTPUT=======PARAM amount_paid==" + amount.trim());


                    params.put("project_id", InventoryAppSingleton.getInstance().getProject_id());
                    params.put("user_id", user_id.trim());
                    params.put("iteam_id", InventoryAppSingleton.getInstance().getItem_id());
                    params.put("paid_user", session.getKeyUserId());
                    params.put("qty_done", quantity.trim());
                    params.put("sub_item", subItem.trim());
                    params.put("amount_paid", amount.trim());
                    Log.e("Inventory signin == ", url + params);

                    return params;
                }

            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(activity);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("ERROR======" + ex);
        }
    }

    @Override
    public int getItemCount() {
        return dataItemArrayList.size();
    }

    private void showConfirmationDialog() {
        LayoutInflater LayoutInflater = activity.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_confirmation_layout, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);


        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //httpPayment("" + numberAsString, qnt, user_id, subItem);
                alertDialog.dismiss();
                //new UpdateDetailsPhysicalActivity.UpdateProjectData().execute();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }


    /*private void docUploadDialog() {
        LayoutInflater LayoutInflater = activity.getLayoutInflater();
        View dialogView = LayoutInflater.inflate(R.layout.dialog_payment_certificate_upload, null);
        TextView messageText = (TextView) dialogView.findViewById(R.id.textDialog);
        Button yesBtn = (Button) dialogView.findViewById(R.id.yesBtn);
        Button noBtn = (Button) dialogView.findViewById(R.id.noBtn);
        ImageView docImage=(ImageView)dialogView.findViewById(R.id.docImage);

        alertDialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();



        docImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               egefgegeg
            }
        });


        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // httpPayment(""+numberAsString,qnt, user_id, subItem );
                alertDialog.dismiss();
            }
        });


        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });


    }*/

    public void updateList(ArrayList<DataItem> dataItemArrayList) {
        this.dataItemArrayList = dataItemArrayList;

        notifyDataSetChanged();
        if (dataItemArrayList.size() == 0) {
            listener.UpdateUI();
        }


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemImage)
        ImageView itemImage;
        @BindView(R.id.btn_payment)
        Button btn_payment;
        @BindView(R.id.textView_itemAmount)
        TextView textView_itemAmount;
        @BindView(R.id.textView_date)
        TextView textView_date;
        @BindView(R.id.textView_itemQuantity)
        TextView textView_itemQuantity;
        @BindView(R.id.txtItemCommentValueTV)
        TextView txtItemCommentValueTV;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


    }


}


