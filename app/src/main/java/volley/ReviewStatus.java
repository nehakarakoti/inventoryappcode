package volley;

public class ReviewStatus {
    private String status="";
private CountObject data;

    public CountObject getData() {
        return data;
    }

    public void setData(CountObject data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
