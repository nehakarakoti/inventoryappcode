package volley;

public class CountObject {
    private int cancelled_by,approved_by,edit_count,editable;

    public int getCancelled_by() {
        return cancelled_by;
    }

    public void setCancelled_by(int cancelled_by) {
        this.cancelled_by = cancelled_by;
    }

    public int getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(int approved_by) {
        this.approved_by = approved_by;
    }

    public int getEdit_count() {
        return edit_count;
    }

    public void setEdit_count(int edit_count) {
        this.edit_count = edit_count;
    }

    public int getEditable() {
        return editable;
    }

    public void setEditable(int editable) {
        this.editable = editable;
    }
}
